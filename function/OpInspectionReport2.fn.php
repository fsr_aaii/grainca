<?php

    $client = OpInspectionReport::getClient();
    $drive = new Google_Service_Drive($client); 
    //OpInspectionReport::sync($sys,$drive,$client,'1llssrd48gOZ_pKYAX0xlvcggK9m3fq_R');
    $sys->checkTrans();



    


/*$sheetId = "0";  # Please set sheet ID.

$service = new Google_Service_Sheets($client);

// The ID of the spreadsheet to update.
$spreadsheetId = '1zXb_-AHYmQpUHlLIQAl9w2D40Oh1FgX_V0OLpL8NgQU';  // TODO: Update placeholder value.

// TODO: Assign values to desired properties of `requestBody`:
$requestBody = new Google_Service_Sheets_BatchUpdateValuesRequest();

$requestBody = '{
    "requests": [
        {
            "copyPaste": {
                "source": {
                    "sheetId": $sheetId,
                    "startRowIndex": 7,
                    "endRowIndex":8,
                    "startColumnIndex": 0,
                    "endColumnIndex": 1
                },
                "destination": {
                    "sheetId": $sheetId,
                    "startRowIndex": 7,
                    "endRowIndex": 9,
                    "startColumnIndex": 1,
                    "endColumnIndex": 2
                },
                "pasteType": "PASTE_FORMULA"
            }
        }
    ]
}';

$response = $service->spreadsheets_values->batchUpdate($spreadsheetId, $requestBody);

// TODO: Change code below to process the `response` object:
echo '<pre>', var_export($response, true), '</pre>', "\n";*/

/*
$service = new Google_Service_Sheets($client);

$spreadsheetId = '1zXb_-AHYmQpUHlLIQAl9w2D40Oh1FgX_V0OLpL8NgQU';  // TODO: Update placeholder value.

    $cell = [
        'userEnteredFormat' => [
            'numberFormat' => [
                'type' => 'NUMBER',
                'pattern' => '[Color22]0.00', // '[Blue]#.##'
            ],
            "backgroundColor" => [
                "red" => 0.2,
                "green" => 1.0,
                "blue" => 0.5,
            ],
        ],
        'userEnteredValue' => [
            'numberValue' => 23
        ],
        'note' => 'previous value: 44'
    ];
    $cells = [ $cell, $cell ];
    $row = [
        'values' => $cells
    ];
    $rows[] = $row;

    $appendCellsRequest = [
        'fields' => '*',
        'rows' => $rows
    ];

    $request = [
        'appendCells' => $appendCellsRequest
    ];

$requests = new Google_Service_Sheets_Request(array(
    'insertDimension' => array(
        'range' => array(
            'sheetId' => 0,
            'dimension' => "ROWS",
            'startIndex' => 3,
            'endIndex' => 4
        )
    )
));


$dimensionRange = new Google_Service_Sheets_DimensionRange([
        'sheetId' => $sheetId,
        'dimension' => 'COLUMNS',
        'startIndex' => 1,
        'endIndex' => 2        
    ]);

    $dimensionProperties = new Google_Service_Sheets_DimensionProperties([
        'pixelSize' => 200                   
    ]);


   /* $requestBody = new Google_Service_Sheets_Request(array(
    'updateDimensionProperties' => array(
        'range' => $dimensionRange,
                'properties' => $dimensionProperties,
                'fields' => 'pixelSize'
        )
    )
);*/

     /*

   $requestBody =  new Google_Service_Sheets_Request(array(
        'repeatCell' => array(
            'cell' => array(
                'userEnteredFormat' => array(
                    'backgroundColor' => array(
                        'red' => 0,
                        'green' => 0,
                        'blue' => 1
                    )
                )
            ),
            'range' => array(
                'sheetId' => $sheetId,  // <--- Please set the sheet ID.
                'startRowIndex' => 10,
                'endRowIndex' => 11,
                'startColumnIndex' => 1,
                'endColumnIndex' => 12
            ),
            'fields' => 'userEnteredFormat'
        )
    ));


    $batchUpdate = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
        'requests' => [ $requestBody ]
    ]);

    $service->spreadsheets->batchUpdate($spreadsheetId, $batchUpdate);
*/


   $service = new Google_Service_Sheets($client); // <--- This is from your script.

$copiedRowNumber = 10; // Please set the row number of "MAIN SHEET" that you want to copy.
$sourceSpreadsheetId = "$spreadsheetId"; // Please set the source Spreadsheet ID.
$sourceSheetId = "0"; // Please set the source sheet ID.
$destinationSpreadsheetId = "$spreadsheetId"; // Please set the destination Spreadsheet ID.
$destinationSheetName = "base de datos"; // Please set the destination sheet name.
$destinationSheetId = "0"; // Please set the destination sheet ID.

// 1. Copy the source sheet in the source Spreadsheet to the destination Spreadsheet.
$requestBody = new Google_Service_Sheets_CopySheetToAnotherSpreadsheetRequest(
    array("destinationSpreadsheetId" => $destinationSpreadsheetId)
);
$res1 = $service->spreadsheets_sheets->copyTo($sourceSpreadsheetId, $sourceSheetId, $requestBody);
$copiedSheetId = $res1["sheetId"];

// 2. Retrieve 1st empty row number of the destination sheet.
$res2 = $service->spreadsheets_values->get($destinationSpreadsheetId, $destinationSheetName);
$row = count($res2["values"]);

// 3. Copy the values with the format from the copied sheet to the destination sheet. And, delete the copied sheet.
$requests = [
    new \Google_Service_Sheets_Request([
        'copyPaste' => [
            'source' => [
                'sheetId' => $copiedSheetId,
                'startRowIndex' => 10,
                'endRowIndex' => 11,
            ],
            'destination' => [
                'sheetId' => $destinationSheetId,
                'startRowIndex' => 11,
                'endRowIndex' => 12
            ],
            'pasteType' => 'PASTE_NORMAL',
        ],
    ]),
    new \Google_Service_Sheets_Request([
        'deleteSheet' => ['sheetId' => $copiedSheetId]
    ])
];
$batchUpdateRequest = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest(['requests' => $requests]);
$res3 = $service->spreadsheets->batchUpdate($destinationSpreadsheetId, $batchUpdateRequest);