<?php 
session_start(); 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once("include/system.inc.php");  

$router = new AltoRouter();
$router->setBasePath(TF_BASE_PATH);


$router->map( 'GET', '/', function() {
  TfResponse::call(__DIR__ . '/login.html');
});

$router->map( 'GET', '/forgot', function() {
  TfResponse::call(__DIR__ . '/forgot.html');
  //6LeGzMgaAAAAAPtKMdG7k-bsiFDQ0SWecteQsIRO
});


$router->map( 'GET', '/JS/01', function() {
   try{
     
      
      $sys = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $sys->setFunctionalAreaCode('ve');
      $sys->setAuthenticationType("Sys");
      $sys->start();      
      
       require(TF_REQUIRE_PATH.'/function/OpInspectionReport.fn.php');

  
      $sys->close();
            
  }catch(Throwable $t){
      echo "string".$t;
  }
  
});


$router->map( 'POST', '/forgot', function() {
  
  $tfResponse = new TfResponse();
  try{
    $recaptcha = $_POST["g-recaptcha-response"];
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data = array(
      'secret' => '6LeGzMgaAAAAAPtKMdG7k-bsiFDQ0SWecteQsIRO',
      'response' => $recaptcha
    );
    $options = array('http' => array ('method' => 'POST',
                                      'content' => http_build_query($data)));
    
    $context  = stream_context_create($options);
    $verify = file_get_contents($url, false, $context);
    $captcha_success = json_decode($verify);

    if ($captcha_success->success) {


      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      
      $tfs->setFunctionalAreaCode('ve');
      $tfs->setUserLogin($_POST["t_user"]["login"]);
      $tfs->setSessionType("A");
      $tfs->setAuthenticationType("Login");
      $tfs->start();      
      $tfs->forgotToken();
    
      $body = file_get_contents("template/forgot.html");
      $body = str_replace('{%JWT%}',TF_ROOT_PATH.'/forgot/'.$tfs->getForgot(),$body);
      $body = str_replace('{%NAME%}',$tfs->getUserName(),$body);
      $body = str_replace('{%LOGIN%}',$tfs->getUserLogin(),$body);
    

       $mail = new PHPMailer(true);

       try {
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;

        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->IsSMTP();
        $mail->SMTPDebug  = 0;
        $mail->Host       = 'smtp.gmail.com';
        $mail->Port       = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth   = true;
        $mail->Username = TF_MAIL_ACCOUNT;
        $mail->Password = TF_MAIL_PASSWORD;
        $mail->AddAddress($tfs->getUserEmailAccount(),$tfs->getUserName());
        $mail->From = TF_MAIL_ACCOUNT;
        $mail->SetFrom(TF_MAIL_ACCOUNT, TF_MAIL_FROM);
        $mail->Subject = utf8_decode("Nueva contraseña");
        $mail->MsgHTML($body);
        $mail->Body = $body;

        $mail->send();
        $tfResponse->set("tfStatus","OK",FALSE);
      } catch (Exception $e) {
        $tfResponse->set("tfStatus","Failed",FALSE); 
      }

      $tfs->close();    
      $tfResponse->return(); 
    } else {
      $tfs->close(); 
      $tfResponse->set("tfStatus","NOT OK",FALSE);
      $tfResponse->return(); 
    }
     
  }catch(Throwable $t){      
     $tfResponse->exception($t);
  }  
 
   
});


$router->map( 'GET', '/forgot/[:tst]', function($tfForgotToken) {
  
  try{
    $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
        
    $tfs->setFunctionalAreaCode('ve');

    $tfs->setForgot($tfForgotToken);

    $tfs->setAuthenticationType("Forgot");
    $tfs->start(); 
     require __DIR__ .'/reset.php';

    $tfs->close(); 


  }catch(Throwable $t){      
     TfResponse::call(__DIR__ . '/token.html');
  } 

});


$router->map( 'POST', '/[a:tfa]/reset', function($tfFunctionalArea) {
  
  $tfResponse = new TfResponse();
  try{

      $tfRequest = new TfRequest($_POST['request-data']);
      unset($_POST);

      $t_user =  $tfRequest->get("t_user");

      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode('ve');
      $tfs->setUserId($tfRequest->get("t_user_id"));
      $tfs->setUserPassword(md5($t_user["password"]));
      $tfs->setAuthenticationType("Reset");
      $tfs->setTrailId($tfRequest->get("tfTrailId"));
      $tfs->start();      
      $tfs->setRandomPublicKey($tfRequest->get("tfRandomPublicKey"));
      $tfs->setNewTrail($tfRequest->get("tfRandomPublicKey"));
      $tfResponse->set("tfSessionToken",$tfs->getId(),FALSE);
      $tfs->close(); 
      $tfResponse->return(); 
            
  }catch(Throwable $t){
      $tfResponse->exception($t);
    }
 
   
});
$router->map( 'GET', '/admin', function() {
  TfResponse::call(__DIR__ . '/admin.html');
});


$router->map( 'GET', '/register', function() {
  TfResponse::call(__DIR__ . '/register.html');
});

$router->map( 'POST', '/admin/[a:tfa]/login', function($tfFunctionalArea) {
  $tfResponse = new TfResponse();
  try{
      $tfRequest = new TfRequest($_POST['request-data']);
      unset($_POST);
      
      $t_user =  $tfRequest->get("t_user");

      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setUserLogin($t_user["login"]);
      $tfs->setUserPassword(md5($t_user["password"]));
      $tfs->setSessionType("A");
      $tfs->setAuthenticationType("Password");
      $tfs->setTrailId($tfRequest->get("tfTrailId"));
      $tfs->start();      
      $tfs->setRandomPublicKey($tfRequest->get("tfRandomPublicKey"));
      $tfs->setNewTrail($tfRequest->get("tfRandomPublicKey"));
      $tfResponse->set("tfSessionToken",$tfs->getId(),FALSE);
      $tfs->close(); 
      $tfResponse->return(); 
            
  }catch(Throwable $t){
      $tfResponse->exception($t);
    }
  
});

$router->map( 'POST', '/[a:tfa]/login', function($tfFunctionalArea) {
  $tfResponse = new TfResponse();
  try{
      $tfRequest = new TfRequest($_POST['request-data']);
      unset($_POST);
      
      $t_user =  $tfRequest->get("t_user");

      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setUserLogin($t_user["login"]);
      $tfs->setUserPassword(md5($t_user["password"]));
      $tfs->setAuthenticationType("Password");
      $tfs->setTrailId($tfRequest->get("tfTrailId"));
      $tfs->start();      
      $tfs->setRandomPublicKey($tfRequest->get("tfRandomPublicKey"));
      $tfs->setNewTrail($tfRequest->get("tfRandomPublicKey"));
      $tfResponse->set("tfSessionToken",$tfs->getId(),FALSE);
      $tfs->close(); 
      $tfResponse->return(); 
            
  }catch(Throwable $t){
      $tfResponse->exception($t);
    }
  
});

$router->map( 'POST', '/[a:tfa]/register', function($tfFunctionalArea) {
  $tfResponse = new TfResponse();
  try{
      $tfRequest = new TfRequest($_POST['request-data']);
      unset($_POST);
      
      $t_user =  $tfRequest->get("t_user");
      
      $sys = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $sys->setFunctionalAreaCode($tfFunctionalArea);
      $sys->setAuthenticationType("Sys");
      $sys->start();      
      
      if (TUser::exist($sys,$t_user["login"])){
        $tfResponse->set("tfStatus","Duplicate",FALSE);  
      }else{

        $tfser = new TUser($sys);
        $tfser->setLogin($t_user["login"]);  
        $tfser->setActive("Y");  
        $tfser->setPassword(md5("g4l4x14g4l1l30"));
        $tfser->setPasswordDate(date("Y-m-d"));
        $tfser->setEmailAccount($t_user["login"]); 
        $tfser->setSource($t_user["institution"]);
        $tfser->setName($t_user["name"].", ".$t_user["lastname"]); 
        $tfser->setCreatedBy($sys->getUserId());
        $tfser->setCreatedDate(date("Y-m-d H:i:s"));
        $tfser->register(3);
        if ($tfser->isValid()){ 
          $sys->checkTrans();
          $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
          $tfs->setFunctionalAreaCode($tfFunctionalArea);
          $tfs->setUserLogin($tfser->getLogin());
          $tfs->setUserPassword($tfser->getPassword());
          $tfs->setAuthenticationType("Password");
          $tfs->setTrailId($tfRequest->get("tfTrailId"));
          $tfs->start();      
          $tfs->setRandomPublicKey($tfRequest->get("tfRandomPublicKey"));
          $tfs->setNewTrail($tfRequest->get("tfRandomPublicKey"));
          $tfResponse->set("tfStatus","Ok",FALSE);
          $tfResponse->set("tfSessionToken",$tfs->getId(),FALSE);
          $tfs->close();
        }else{
          $tfResponse->set("tfStatus","Failed",FALSE); 
        }  
      }

      $sys->close();
      $tfResponse->return();
            
  }catch(Throwable $t){
      $tfResponse->exception($t);
    }
  
});

$router->map( 'GET', '/[a:tli]/[a:tst]', function($tfFunctionalArea,$tfSessionToken) {
   try{
      
      $tfTrailId = TfWidget::strRandom(8);
      $tfRandomPublicKey = TfWidget::strRandom(8);

      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setAuthenticationType("Token");
      $tfs->setTrailId($tfTrailId);
      $tfs->start();      
      $tfs->setRandomPublicKey($tfRandomPublicKey);
      $tfs->setNewTrail($tfRandomPublicKey);
      
      echo '<script type="text/javascript">
         sessionStorage.setItem("RANDOM_PUBLIC_KEY","'.$tfRandomPublicKey.'");
         sessionStorage.setItem("TF_TRAIL_ID","'.$tfTrailId.'");
         sessionStorage.setItem("TF_SESSION_TOKEN",'.$tfSessionToken.');
         sessionStorage.setItem("TF_APP_ROOT",window.location.href);   
         sessionStorage.setItem("TF_URL_ROOT",window.location.href+"/"+sessionStorage.getItem("TF_TRAIL_ID"));
         history.pushState({ step: 0 },null,sessionStorage.getItem("TF_URL_ROOT")+"/0");
         window.location.href=sessionStorage.getItem("TF_URL_ROOT")+"/0";
      </script>';

      $tfs->close(); 
            
  }catch(Throwable $t){
      if (is_file(__DIR__ .'/'.$t->getCode().'.html')){
       require __DIR__ .'/'.$t->getCode().'.html';
     }else{
       print_r($t);
     } 
  }
});

$router->map( 'GET', '/[a:tli]/[a:tst]/[a:tsb]/[a:tss]', function($tfFunctionalArea,$tfSessionToken,$tfTrailId,$tfStepId) {
  try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setAuthenticationType("Token");       
      $tfs->start();
      TfResponse::call(__DIR__ . '/home.php');
      $tfs->close(); 
            
  }catch(Throwable $t){
      if (is_file(__DIR__ .'/'.$t->getCode().'.html')){
       require __DIR__ .'/'.$t->getCode().'.html';
     }else{
       require __DIR__ .'/911.php';
     } 
  }

});

$router->map( 'GET', '/[a:tli]/[a:tst]/[a:tsb]/[a:tss]/403', function($tfFunctionalArea,$tfSessionToken,$tfTrailId,$tfStepId) {  
  require __DIR__ .'/403.php';
  
});


$router->map( 'POST', '/[a:tli]/[a:tst]/[a:tsb]/nav', function($tfFunctionalArea,$tfSessionToken,$tfTrailId) {
  $tfResponse = new TfResponse();
  try{
      $tfRequest = new TfRequest($_POST['request-data']);
      unset($_POST);
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");
      $tfs->start();
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      $tfResponse->set("tfRandomPublicKey",$tfs->getRandomPublicKey(),false);
      //$tfResponse->set("tfMenu",$tfs->menu(),true);
      //$tfResponse->set("tfUserPhoto",$tfs->getUserPhoto(),true);
      $tfResponse->set("tfMenu",$tfs->menu(),false);
      $tfResponse->set("tfUserPhoto",$tfs->getUserPhoto(),false);
      $tfs->close(); 
      $tfResponse->return(); 
            
  }catch(Throwable $t){
      $tfResponse->exception($t);
  }

});

$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/default/[a:tss]', function($tfFunctionalArea,$tfSessionToken,$tfTrailId,$tfSessionStep) { 
  $tfResponse = new TfResponse();
  ob_start(); 
   try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfRequest = new TfRequest($_POST['request-data'],$tfs->getRandomPublicKey());
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());

      if ($tfs->isEmptyTrail()){
        $path=$tfs->default();
        if ($path!=''){
          $tfRequestAction = $tfs->getAction();

          require($path);
        }else{
          require __DIR__ .'/default.html';
        }
      }else{ 
         $tfs->refresh($tfSessionStep);
      }  
      $tfs->close();
      $content = ob_get_contents();
  ob_end_clean();
  $tfResponse->set("tfContent",$content,true);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
    print_r($t);
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfContent",$content,true);
     $tfResponse->return(); 

     exit();
  }
});

$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/previous/[a:tss]', function($tfFunctionalArea,$tfSessionToken,$tfTrailId,$tfSessionStep) {
  $tfResponse = new TfResponse();
  ob_start();
   try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      $tfs->previous($tfSessionStep);
      $tfs->close();
      $content = ob_get_contents();
  ob_end_clean();
  //$tfResponse->set("tfTrail",json_encode($tfs->getBreadcrumb(),true),false);
  $tfResponse->set("tfContent",$content,true);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfContent",$content,true);
     $tfResponse->return(); 

     exit();
  }
});


$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/view/[a:trt]/[:trc]/[a:tra]', function($tfFunctionalArea,$tfSessionToken,$tfTrailId,$tfRequestTaskId,$tfRequestController,$tfRequestAction) {
  $tfResponse = new TfResponse();
  ob_start();
  try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTaskId($tfRequestTaskId);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfRequest = new TfRequest($_POST['request-data'],$tfs->getRandomPublicKey());
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      $path=$tfs->access($tfRequestController);
      $tfs->toBeTrail($tfRequestController,$tfRequestAction,$_POST['request-data']);
      $tfs->setSessionHistoryState(1);
      require($path);
      $tfs->close();
      $content = ob_get_contents();
  ob_end_clean();
  $tfResponse->set("tfSessionHistoryState",$tfs->getSessionHistoryState(),false);
  $tfResponse->set("tfSessionPushState",$tfs->maxTrail(),false);
  $tfResponse->set("tfContent",$content,false);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfSessionHistoryState",0,false);
     $tfResponse->set("tfContent",TfException::format($t),false);
     $tfResponse->return(); 
    
     //exit();
  }
});

$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/redirect/[a:trt]/[:trc]/[a:tra]', function($tfFunctionalArea,$tfSessionToken,$tfTrailId,$tfRequestTaskId,$tfRequestController,$tfRequestAction) {
  $tfResponse = new TfResponse();
  ob_start();
  try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTaskId($tfRequestTaskId);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfRequest = new TfRequest($_POST['request-data'],$tfs->getRandomPublicKey());
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      $path=$tfs->access($tfRequestController);
      //$tfs->toBeTrail($tfRequestController,$tfRequestAction,$_POST['request-data']);
      $tfs->setSessionHistoryState(1);
      require($path);
      $tfs->close();
      $content = ob_get_contents();
  ob_end_clean();
  $tfResponse->set("tfSessionHistoryState",$tfs->getSessionHistoryState(),false);
  $tfResponse->set("tfSessionPushState",$tfs->maxTrail(),false);
  $tfResponse->set("tfContent",$content,true);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfSessionHistoryState",0,false);
     $tfResponse->set("tfContent",$t,true);
     $tfResponse->return(); 

     exit();
  }
});

$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/fn/[a:trt]/[:trc]/[a:tra]', function($tfFunctionalArea,$tfSessionToken,$tfTrailId,$tfRequestTaskId,$tfRequestController,$tfFnName) {
  $tfResponse = new TfResponse();
  ob_start();
  try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTaskId($tfRequestTaskId);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();     
      $tfRequest = new TfRequest($_POST['request-data'],$tfs->getRandomPublicKey());
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      $path=$tfs->access($tfRequestController,false);
      require($path);
$tfs->close();
      $content = ob_get_contents();
  ob_end_clean();
  $tfResponse->set("tfContent",$content,true);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
     print_r($t);
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfContent",$content,true);
     $tfResponse->return(); 

     exit();
  }
});



$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/home', function($tfFunctionalArea,$tfSessionToken,$tfTrailId) {
  
  $tfResponse = new TfResponse();
 ob_start();
   try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfRequest = new TfRequest($_POST['request-data'],$tfs->getRandomPublicKey());
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());

      $path=$tfs->default();
      if ($path!=''){
        $tfRequestAction = $tfs->getAction();
        require($path);
      }else{
        require __DIR__ .'/default.html';
      }
      $tfs->close();
      $content = ob_get_contents();
  ob_end_clean();
  $tfResponse->set("tfContent",$content,true);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
    print_r($t);
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfContent",$content,true);
     $tfResponse->return(); 

     exit();
  }
});

$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/logout', function($tfFunctionalArea,$tfSessionToken,$tfTrailId) {

  $tfResponse = new TfResponse();
   try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      $tfs->logout();
      $tfs->close();
  $tfResponse->set("tfContent","",false);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      

     $tfResponse->set("tfContent",$t,true);
     $tfResponse->return(); 

     exit();
  }
});



$match = $router->match();
if( $match && is_callable( $match['target'] ) ) {
	call_user_func_array( $match['target'], $match['params']); 
} else {
	require __DIR__ . '/404.php';
}
?>



