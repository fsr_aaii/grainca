<style type="text/css">
  #main-window[chromehidden*="toolbar"] #nav-bar {
  visibility: collapse;
}
</style>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="shortcut icon" type="image/x-icon" href="../../../asset/images/favicon.ico">
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Grainca</title>

  <link rel="stylesheet" type="text/css" href="../../../vendor/bootstrap-4.5.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../../../font/fontawesome-free-5.12.1-web/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="../../../font/fontawesome/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="../../../font/font-lato/latofonts.css">
  <link rel="stylesheet" type="text/css" href="../../../font/font-lato/latostyle.css">
  <link rel="stylesheet" type="text/css" href="../../../vendor/jquery-nice-select-1.1.0/css/nice-select.css">
  <link rel="stylesheet" type="text/css" href="../../../vendor/boxicons/css/boxicons.min.css">
  <link rel="stylesheet" type="text/css" href="../../../vendor/gijgo-1.9.10/css/gijgo.min.css">
  <link rel="stylesheet" type="text/css" href="../../../vendor/flexdatalist/jquery.flexdatalist.min.css">

  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/datatables.min.css"/>
 
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/AutoFill-2.3.3/css/autoFill.bootstrap4.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/Buttons-1.5.6/css/buttons.bootstrap4.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/ColReorder-1.5.0/css/colReorder.bootstrap4.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/FixedColumns-3.2.5/css/fixedColumns.bootstrap4.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/FixedHeader-3.1.4/css/fixedHeader.bootstrap4.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/KeyTable-2.5.0/css/keyTable.bootstrap4.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/Responsive-2.2.2/css/responsive.bootstrap4.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/RowGroup-1.1.0/css/rowGroup.bootstrap4.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/RowReorder-1.2.4/css/rowReorder.bootstrap4.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/Scroller-2.0.0/css/scroller.bootstrap4.min.css"/>
  <link rel="stylesheet" type="text/css" href="../../../vendor/datatables/Select-1.3.0/css/select.bootstrap4.min.css"/>

  <link rel="stylesheet" type="text/css" href="../../../asset/css/main.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/nav.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/form.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/datatable.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/progress.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/select.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/modal.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/webcam.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/gallery.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/stopwatch.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/timeline.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../../asset/css/guaramo.css?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/css/main.css');?>" rel="stylesheet">

  

</head>

<body id="page-top" class="h-100" >
      <div id="loading">
        <div class="d-flex align-content-center flex-wrap">
          <div  class="col-lg-4 mx-auto">
            <div class="row">
            <div  class="col-12 text-center mx-auto">
              <img src="../../../asset/images/logologin.png" class="img-fluid loading-logo">
            </div>
             <div class="col-12 text-center mx-auto">
              <img src="../../../asset/images/loading.gif" class="img-fluid loading-bar">
            </div>
             </div>
          </div>
        </div>
      </div>
      <div id="screen" style="display: none">
       <div class="d-flex toggled" id="wrapper" >

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
      <div class="sidebar-heading">
         <a class="navbar-brand" href="#" onclick="history.go(0)">   
            <img src="../../../asset/images/logo.png" class="logo">
         </a>
      </div>
     
<nav class="navbar left-menu">
        

          <ul id="main-menu-ul" name="main-menu-ul" class="navbar-nav ml-2">
            
           
          </ul>
      </nav>



       
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper" class="bg-light">

      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="nav-link-toggle" href="#" id="menu-toggle"><i class="bx bx-menu"></i></a>



        <div id="navbarSupportedContent" class="text-right">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item"> 
              <a class="nav-link"  onclick="TfRequest.home();" ><i class="bx bx-home"></i></a> 
            </li>
            <li class="nav-item window-open"> 
              <a class="nav-link"  onclick="TfRequest.tab();" ><i class="bx bx-window-open"></i></a> 
            </li>
            <li class="nav-item"> 
              <a class="nav-link"  onclick="TfRequest.logout();" ><i class="bx bx-power-off"></i></a> 
            </li>
          </ul>
        </div>
      </nav>
      
      <div id="tf-container" name="tf-container" class="container-fluid">
  

      </div>
      <nav class="nav-btn">
      <div class="row">
        <a class="btn btn-guaramo xl rounded-circle"  onclick="history.back()">
           <i class="bx bx-left-arrow"></i>
        </a>
        <a class="btn btn-guaramo xl rounded-circle go-to"  href="#page-top">
           <i class="bx bx-up-arrow"></i>
        </a>
        <a class="btn btn-guaramo xl rounded-circle go-to"  href="#page-bottom">
           <i class="bx bx-down-arrow"></i>
        </a>
        </div>
      </nav>
    </div>
    <!-- /#page-content-wrapper -->
     
  </div>
  <!-- /#wrapper -->
    <!-- Bootstrap core JavaScript-->
  <script src="../../../vendor/jquery-3.5.1/jquery.min.js"></script>
  <script type="text/javascript" src="../../../vendor/popper-1.16.0/popper.min.js"></script>
  <script src="../../../vendor/bootstrap-4.5.0/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../../vendor/jquery-easing/jquery.easing.min.js"></script>


  <script type="text/javascript" src="../../../vendor/jquery-base64/jquery.base64.js"></script>  
  <script type="text/javascript" src="../../../vendor/jquery-form/jquery.form.js"></script>
  <script type="text/javascript" src="../../../vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
  <script type="text/javascript" src="../../../vendor/jquery-touchSwipe/jquery.touchSwipe.min.js"></script>
  <script type="text/javascript" src="../../../vendor/validate/jquery.validate.js"></script>
  <script type="text/javascript" src="../../../vendor/jquery-additional-methods/additional-methods.min.js"></script>


  <!-- Custom scripts for all pages-->
  <script type="text/javascript" src="../../../vendor/gijgo-1.9.10/js/gijgo.min.js"></script>
  <script type="text/javascript" src="../../../vendor/flexdatalist/jquery.flexdatalist.min.js"></script>
  <script type="text/javascript" src="../../../vendor/chartjs/dist/Chart.min.js"></script>
  <script type="text/javascript" src="../../../vendor/chartjs/dist/Chart.bundle.min.js"></script> 
  <script type="text/javascript" src="../../../vendor/chartjs-plugin-datalabels/dist/chartjs-plugin-datalabels.js"></script> 
  <script type="text/javascript" src="../../../vendor/jsencrypt/jsencrypt.min.js"></script>
  <script type="text/javascript" src="../../../vendor/serializeObject/jquery.serializeObject.min.js"></script>
  <script type="text/javascript" src="../../../vendor/serializeJSON/jquery.serializeJSON.min.js"></script>
  <script type="text/javascript" src="../../../vendor/Alert/jquery.alerts.js"></script>
  <script type="text/javascript" src="../../../vendor/crypto-js/crypto-js.js"></script>
  <script type="text/javascript" src="../../../vendor/Encryption/js/Encryption.js"></script> 
  <script type="text/javascript" src="../../../vendor/tableToExcel/dist/tableToExcel.js"></script> 
  <script type="text/javascript" src="../../../vendor/jquery-nice-select-1.1.0/js/jquery.nice-select.min.js"></script>
  <script type="text/javascript" src="../../../vendor/jquery-nicescroll/jquery.nicescroll.min.js"></script>
  
  <script type="text/javascript" src="../../../vendor/datatables/datatables.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/JSZip-2.5.0/jszip.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/AutoFill-2.3.3/js/dataTables.autoFill.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/AutoFill-2.3.3/js/autoFill.bootstrap4.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/Buttons-1.5.6/js/buttons.bootstrap4.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/Buttons-1.5.6/js/buttons.colVis.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/Buttons-1.5.6/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/Buttons-1.5.6/js/buttons.print.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/ColReorder-1.5.0/js/dataTables.colReorder.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/FixedColumns-3.2.5/js/dataTables.fixedColumns.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/FixedHeader-3.1.4/js/dataTables.fixedHeader.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/KeyTable-2.5.0/js/dataTables.keyTable.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/Responsive-2.2.2/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/Responsive-2.2.2/js/responsive.bootstrap4.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/RowGroup-1.1.0/js/dataTables.rowGroup.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/RowReorder-1.2.4/js/dataTables.rowReorder.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/Scroller-2.0.0/js/dataTables.scroller.min.js"></script>
  <script type="text/javascript" src="../../../vendor/datatables/Select-1.3.0/js/dataTables.select.min.js"></script>
  <script type="text/javascript" src="../../../vendor/simplePagination/jquery.simplePagination.js"></script>
  <script type="text/javascript" src="../../../vendor/webcam-easy/webcam-easy.js"></script>
  <script type="text/javascript" src="../../../vendor/CircleType/circletype.min.js"></script>


  <script type="text/javascript" src="../../../asset/js/main.js?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/js/main.js');?>"></script>
  <script type="text/javascript" src="../../../asset/js/seeds.js?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/js/seeds.js');?>"></script>
  <script type="text/javascript" src="../../../asset/js/messages_es.js?v=<?=filemtime(TF_REQUIRE_PATH.'/asset/js/messages_es.js');?>"></script>
  <script type="text/javascript" src="../../../tuich/core/js/TfCryptography.js?v=<?=filemtime(TF_REQUIRE_PATH.'/tuich/core/js/TfCryptography.js');?>"></script>
  <script type="text/javascript" src="../../../tuich/core/js/TfRequest.js?v=<?=filemtime(TF_REQUIRE_PATH.'//tuich/core/js/TfRequest.js');?>"></script>
  <script type="text/javascript" src="../../../tuich/core/js/TfExport.js?v=<?=filemtime(TF_REQUIRE_PATH.'/tuich/core/js/TfExport.js');?>"></script>
  <script type="text/javascript" src="../../../tuich/core/js/TfValidate.js?v=<?=filemtime(TF_REQUIRE_PATH.'//tuich/core/js/TfValidate.js');?>"></script>
  <script type="text/javascript" src="../../../localization/datatables_es.js?v=<?=filemtime(TF_REQUIRE_PATH.'/localization/datatables_es.js');?>"></script>
  <script type="text/javascript" src="../../../localization/messages_es.js?v=<?=filemtime(TF_REQUIRE_PATH.'/localization/messages_es.js');?>"></script>
  
  <div id="page-bottom" class="footer"></div>
</div>
</body>

    
</html>

<script type="text/javascript">

  $(document).ready(function() {
      

    sessionStorage.setItem("TF_POPSTATE","FALSE");  

    if ($(window).width() < 768) {
           $("#wrapper").removeClass("toggled");
    }
        
    if (sessionStorage.getItem("TF_SESSION_TOKEN") === null) {
      window.location.href+='/404';
    }else{

      TfRequest.nav(); 

      $(".dropdown-item").click(function(e) {
        if ($(window).width() < 768) {
           $("#wrapper").removeClass("toggled");
        }  
      });
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      }); 

      $(".nav-link").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });

       $(window).on("popstate", function(e) {
         sessionStorage.setItem("TF_POPSTATE","TRUE"); 
         var step = e.originalEvent.state || 'NULL';
         if (step=='NULL'){
          console.log("step:NULL");
           history.go(0);
         }else{
          TfRequest.previous(e.originalEvent.state.step);
         } 
      });

      if (sessionStorage.getItem("TF_POPSTATE")=="FALSE"){
        TfRequest.default();
      }
           
    } 
    $("#loading").hide();
    $("#screen").show();
  });



  

</script>
 