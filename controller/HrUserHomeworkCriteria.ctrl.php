<?php
  if ($tfRequestAction=="AL"){
     $hrUserHomeworkCriteriaList=HrUserHomeworkCriteria::dataList($tfs);
  }else{
     $hrUserHomeworkCriteria = new HrUserHomeworkCriteria($tfs);
     $hrUserHomeworkCriteria->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrUserHomeworkCriteria->setCreatedBy($tfs->getUserId());
      $hrUserHomeworkCriteria->setCreatedDate(date("Y-m-d H:i:s"));
      $hrUserHomeworkCriteria->setValidations();
      $hrUserHomeworkCriteria->create();
      if ($hrUserHomeworkCriteria->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $hrUserHomeworkCriteria->update();
      if ($hrUserHomeworkCriteria->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrUserHomeworkCriteria","AE",$tfResponse->encrypt(array("hr_user_homework_criteria_id" => $hrUserHomeworkCriteria->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrUserHomeworkCriteria->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrUserHomeworkCriteria.rvw.php");
    }else{
      require("view/HrUserHomeworkCriteria.vw.php");
    } 
  }
?>
