<?php
  if ($tfRequestAction=="AL"){
     $opPortPierList=OpPortPier::dataList($tfs);
  }else{
     $opPortPier = new OpPortPier($tfs);
     $opPortPier->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $opPortPier->setCreatedBy($tfs->getUserId());
      $opPortPier->setCreatedDate(date("Y-m-d H:i:s"));
      $opPortPier->setValidations();
      $opPortPier->create();
      if ($opPortPier->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $opPortPier->update();
      if ($opPortPier->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("OpPortPier","AE",$tfResponse->encrypt(array("op_port_pier_id" => $opPortPier->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $opPortPier->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/OpPortPier.rvw.php");
    }else{
      require("view/OpPortPier.vw.php");
    } 
  }
?>
