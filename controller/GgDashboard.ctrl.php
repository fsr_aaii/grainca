<?php

  if ($tfRequestAction=="AR"){
     $opInspectionReportList = OpInspectionReport::dataList($tfs);
  }else if ($tfRequestAction=="AU"){
     $opInspectionReportList = OpInspectionReport::dLByUser($tfs);
     //print_r($opInspectionReportList);
  }else if ($tfRequestAction=="AVC"){
     $opInspectionReportList = OpInspectionReport::dLByUser($tfs);
  }else if ($tfRequestAction=="AVP"){
     $opInspectionReportList = OpInspectionReport::dLByUser($tfs);
  }else if ($tfRequestAction=="AD"){
    $opInspectionReport = OpInspectionReport::info($tfs,$tfRequest->op_inspection_report_id);
    $opInspectionReportDetail = OpInspectionReport::DetailByReport($tfs,$tfRequest->op_inspection_report_id);
     $opInspectionReportDetailTransport2 = OpInspectionReport::detailTransport3($tfs,$tfRequest->op_inspection_report_id);
    
    $cargado = OpInspectionReport::detailDestinationCargado($tfs,$tfRequest->op_inspection_report_id);
    $despachado = OpInspectionReport::detailDestinationEmitida($tfs,$tfRequest->op_inspection_report_id);
  }else if ($tfRequestAction=="DVC"){
     $cargado = OpDestination::detailTransportCargado($tfs,$tfRequest->op_destination_id);
  }else if ($tfRequestAction=="DVP"){
     $despachado = OpDestination::detailTransportEmitida($tfs,$tfRequest->op_destination_id);
  }else if ($tfRequestAction=="GPDT"){
     $porDespachar = OpInspectionReport::detailPorDespachar($tfs,$tfRequest->op_destination_id,$tfRequest->op_transport);
  }else if ($tfRequestAction=="GPDT2"){
     $porDespachar = OpInspectionReport::allPorDespachar($tfs,$tfRequest->op_inspection_report_id);
  }else if ($tfRequestAction=="GPDD"){
     $porDespachar = OpDestination::detailPorDespachar($tfs,$tfRequest->op_destination_id);
  }else if ($tfRequestAction=="TPC1"){
     $opInspectionReportDetailTransport4 = OpInspectionReport::detailTransport4($tfs,$tfRequest->op_inspection_report_id,$tfRequest->op_inspection_report_detail_transport);
  }else if ($tfRequestAction=="TPC2"){
     $opInspectionReportDetailTransport5 = OpInspectionReport::detailTransport5($tfs,$tfRequest->op_destination_id,$tfRequest->op_inspection_report_detail_transport);
  }else{
     $opInspectionReport = OpInspectionReport::info($tfs,$tfRequest->op_inspection_report_id);
     $opInspectionReportDetail = OpInspectionReport::DetailByReport($tfs,$tfRequest->op_inspection_report_id);
     $opInspectionReportDetailTransport = OpInspectionReport::detailTransport($tfs,$tfRequest->op_inspection_report_id);

     $cargado = OpInspectionReport::detailDestinationCargado($tfs,$tfRequest->op_inspection_report_id);
    $despachado = OpInspectionReport::detailDestinationEmitida($tfs,$tfRequest->op_inspection_report_id);
      
     $generateDate =date("d/m/Y H:i:s"); 
     $chart_pr_label = json_encode(array_unique(array_column($opInspectionReportDetailTransport, 'transport')), TRUE);
     $chart_pr_data1 = json_encode(array_column($opInspectionReportDetailTransport, 'VEH_SICA'), TRUE);
     $chart_pr_data2 = json_encode(array_column($opInspectionReportDetailTransport, 'VEH_PESADO'), TRUE);

     //$opInspectionReportDetailTransport2 = OpInspectionReport::detailTransport2($tfs,$tfRequest->op_inspection_report_id);
     $opInspectionReportDetailTransport2 = OpInspectionReport::detailTransport3($tfs,$tfRequest->op_inspection_report_id);
     $opDestinationList = OpDestination::DetailByReport($tfs,$tfRequest->op_inspection_report_id);
  }
  switch ($tfRequestAction){
    
    case "AN":
      break; 
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
  }
  if($tfRequestAction=='AR'){
      require("view/GgDashboardAR.rvw.php");
  }elseif($tfRequestAction=='AD'){
      require("view/GgDashboardAD.rvw.php");
  }elseif($tfRequestAction=='AU'){
      require("view/GgDashboardAU.rvw.php");
  }elseif($tfRequestAction=='DVC'){
      require("view/GgDashboardDVC.rvw.php");
  }elseif($tfRequestAction=='DVP'){
      require("view/GgDashboardDVP.rvw.php");
  }else if ($tfRequestAction=="GPDT"){
     require("view/GgDashboardGPDT.rvw.php");
  }else if ($tfRequestAction=="GPDT2"){
     require("view/GgDashboardGPDT2.rvw.php");
  }else if ($tfRequestAction=="GPDD"){
     require("view/GgDashboardGPDD.rvw.php");
  }else if ($tfRequestAction=="GPDD2"){
     require("view/GgDashboardGPDD2.rvw.php");
  }else if ($tfRequestAction=="TPC1"){
     require("view/GgDashboardTPC1.rvw.php");
  }else if ($tfRequestAction=="TPC2"){
     require("view/GgDashboardTPC2.rvw.php");
  }else{
      require("view/GgDashboard.rvw.php");
  } 
 
 ?>
