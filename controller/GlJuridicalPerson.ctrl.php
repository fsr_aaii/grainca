<?php
  if ($tfRequestAction=="AL"){
     $glJuridicalPersonList=GlJuridicalPerson::dataList($tfs);
  }else{
     $glJuridicalPerson = new GlJuridicalPerson($tfs);
     $glJuridicalPerson->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $glJuridicalPerson->setCreatedBy($tfs->getUserId());
      $glJuridicalPerson->setCreatedDate(date("Y-m-d H:i:s"));
      $glJuridicalPerson->setValidations();
      $glJuridicalPerson->create();
      if ($glJuridicalPerson->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("GlJuridicalPerson","AE",$tfResponse->encrypt(array("gl_juridical_person_id" => $glJuridicalPerson->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $glJuridicalPerson->update();
      if ($glJuridicalPerson->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("GlJuridicalPerson","AE",$tfResponse->encrypt(array("gl_juridical_person_id" => $glJuridicalPerson->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $glJuridicalPerson->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/GlJuridicalPerson.rvw.php");
    }else{
      require("view/GlJuridicalPerson.vw.php");
    } 
  }
?>
