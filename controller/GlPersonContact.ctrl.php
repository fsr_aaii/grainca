<?php
  
      use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


  if ($tfRequestAction=="AL"){
     $glPersonContactList=GlPersonContact::dataList($tfs);
  }else{
     $glPersonContact = new GlPersonContact($tfs);
     $glPersonContact->populate($tfRequest,in_array($tfRequestAction,array("AC","AE","DI","EN"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AU":
      if ($glPersonContact->getUser()=='checked'){
        $glPersonContact->otorgar();
      }else{
        $glPersonContact->revocar();
      }
      $tfs->checkTrans();
      $tfs->checkTrans();
        $tfs->swapTrail("GlPersonContact","AE",$tfResponse->encrypt(array("gl_person_contact_id" => $glPersonContact->getId())),2);
        $tfRequestAction="AE"; 
      break;  
    case "DI":
      $glPersonContact->setActive("N");
      $glPersonContact->update();
      if ($glPersonContact->isValid()){ 
        $glPersonContact->revocar();
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);  
      }  
      break; 
    case "EN":
      $glPersonContact->setActive("Y");
      $glPersonContact->update();
      if ($glPersonContact->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail();  
      }  
      break;      
    case "AI":
      $glPersonContact->setActive("Y");
      $glPersonContact->setCreatedBy($tfs->getUserId());
      $glPersonContact->setCreatedDate(date("Y-m-d H:i:s"));
      $glPersonContact->setValidations();
      $glPersonContact->create();
      if ($glPersonContact->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("GlPersonContact","AE",$tfResponse->encrypt(array("gl_person_contact_id" => $glPersonContact->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $glPersonContact->update();
      if ($glPersonContact->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("GlPersonContact","AE",$tfResponse->encrypt(array("gl_person_contact_id" => $glPersonContact->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $glPersonContact->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
    case "AS":
       
       $tUser = new TUser($tfs);
       $tUser->populateLogin($glPersonContact->getAccount()); 
        
       $token = array('tsul'=>$tUser->getLogin(),
                   'tsui'=>$tUser->getId(),
                   'tsup'=>$tUser->getPassword(),
                   'tsupd'=>$tUser->getPasswordDate(),
                   'tsum'=>$tUser->getEmailAccount());

      
       $encryption = new Encryption();  
    
      $jwt = $encryption->encrypt(json_encode($token,TRUE),TUICH_SECRET);


      $body = file_get_contents("template/forgot.html");
      $body = str_replace('{%JWT%}',TF_ROOT_PATH.'/forgot/'.$jwt,$body);
      $body = str_replace('{%NAME%}',$glPersonContact->getName(),$body);
      $body = str_replace('{%LOGIN%}',$glPersonContact->getAccount(),$body);
    

       $mail = new PHPMailer(true);

       try {
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;

        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->IsSMTP();
        $mail->SMTPDebug  = 0;
        $mail->Host       = 'smtp.gmail.com';
        $mail->Port       = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth   = true;
        $mail->Username = TF_MAIL_ACCOUNT;
        $mail->Password = TF_MAIL_PASSWORD;
        $mail->AddAddress($glPersonContact->getAccount(),$glPersonContact->getName());
        $mail->From = TF_MAIL_ACCOUNT;
        $mail->SetFrom(TF_MAIL_ACCOUNT, TF_MAIL_FROM);
        $mail->Subject = utf8_decode("Bienvenido a Grainca App crea tu contraseña");
        $mail->MsgHTML($body);
        $mail->Body = $body;

        $mail->send();
        $glPersonContact->setObjMsg("Notificaci&oacute;n enviada satisfactoriamente");
      } catch (Exception $e) {
        $glPersonContact->setObjError("Error al enviar notificaci&oacute;n");
      } 
       break;  
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/GlPersonContact.rvw.php");
    }else{
      require("view/GlPersonContact.vw.php");
    } 
  }
?>
