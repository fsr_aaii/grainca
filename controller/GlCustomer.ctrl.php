<?php  


  if ($tfRequestAction=="AL"){
     $glPersonRelationshipList=GlPersonRelationship::customerList($tfs);
  }elseif ($tfRequestAction=="AN"){

     if ($tfRequest->gl_person_id!=''){
       $glPersonId = $tfRequest->gl_person_id;
     }else{ 
       $ninType = $tfRequest->gl_customer_nin_type;
       $nin = $tfRequest->gl_customer_nin;
       $ninControlDigit = $tfRequest->gl_customer_nin_control_digit;
       $glPersonId = GlPerson::find($tfs,$ninType,$nin,$ninControlDigit);
     }

  }else{  
     $glPerson = new GlPerson($tfs);
     $glPerson->populate($tfRequest,in_array($tfRequestAction,array("AC","AE","DI","EN"))); 
     
     $glJuridicalPerson = new GlJuridicalPerson($tfs);
     $glJuridicalPerson->populate($tfRequest,in_array($tfRequestAction,array("AC","AE","DI","EN"))); 

     $glNaturalPerson = new GlNaturalPerson($tfs);
     $glNaturalPerson->populate($tfRequest,in_array($tfRequestAction,array("AC","AE","DI","EN"))); 

     $glPersonRelationship = new GlPersonRelationship($tfs);
     $glPersonRelationship->populate($tfRequest,in_array($tfRequestAction,array("AC","AE","DI","EN"))); 


     $glPersonContactList=GlPersonContact::dLByPerson($tfs,$glPerson->getId());

  }
  switch ($tfRequestAction){
    case "AN":
      
      if ($glPersonId!=""){
        $glPerson = new GlPerson($tfs);
        $glPerson->populatePK($glPersonId); 
        
        $glPersonRelationshipId = $glPerson->getClientId();
        
        $glJuridicalPerson = new GlJuridicalPerson($tfs);
        $glJuridicalPerson->populatePK($glPersonId); 

        $glNaturalPerson = new GlNaturalPerson($tfs);
        $glNaturalPerson->populatePK($glPersonId); 
 
         echo $glPersonRelationshipId;

        $glPersonRelationship = new GlPersonRelationship($tfs);
        $glPersonRelationship->populatePK($glPersonRelationshipId); 


         $glPersonContactList=GlPersonContact::dLByPerson($tfs,$glPerson->getId());

        //$tfs->swapTrail("GlCustomer","AE",$tfResponse->encrypt(array("gl_person_id" => $glPersonId)),2);
        $tfRequestAction="AE";
      }else{
        $glPerson = new GlPerson($tfs);
        $glPersonRelationship = new GlPersonRelationship($tfs);
        if (in_array($ninType,array("V","E"))){
          $glPerson->setPersonType("N");
          $glNaturalPerson = new GlNaturalPerson($tfs);
          $glNaturalPerson->setNinType($ninType);
          $glNaturalPerson->setNin($nin);
          $glNaturalPerson->setNinControlDigit($ninControlDigit);
        }else{
          $glPerson->setPersonType("J");
          $glJuridicalPerson = new GlJuridicalPerson($tfs);
          $glJuridicalPerson->setNinType($ninType);
          $glJuridicalPerson->setNin($nin);
          $glJuridicalPerson->setNinControlDigit($ninControlDigit);
        } 
      
      }      
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(true);
      break; 
    case "AE":
      break; 
    case "AI":
      $user = $tfs->getUserId(); 
      $date = date("Y-m-d H:i:s");
      
      $glPerson->setCreatedBy($user);
      $glPerson->setCreatedDate($date );
      $glPerson->setValidations();
      $glPerson->create();

      if ($glPerson->isValid()){ 
        if ($glPerson->getPersonType()=="N"){
            $glNaturalPerson->setId($glPerson->getId());
            $glNaturalPerson->setActive('Y');
            $glNaturalPerson->setCreatedBy($user);
            $glNaturalPerson->setCreatedDate($date);
            $glNaturalPerson->setValidations();
            $glNaturalPerson->create();
            if ($glNaturalPerson->isValid()){               
              $glPersonRelationship->setIdPerson($glPerson->getId());
              $glPersonRelationship->setIdRelationship(1);
              $glPersonRelationship->setDateFrom(date("Y-m-d"));
              $glPersonRelationship->setActive('Y');
              $glPersonRelationship->setCreatedBy($user);
              $glPersonRelationship->setCreatedDate($date);
              $glPersonRelationship->setValidations();
              $glPersonRelationship->create();
              if ($glPersonRelationship->isValid()){ 
                $tfs->checkTrans();
                $tfs->backTrail();
              }
            }

        }else{
            $glJuridicalPerson->setId($glPerson->getId());
            $glJuridicalPerson->setActive('Y');
            $glJuridicalPerson->setCreatedBy($user);
            $glJuridicalPerson->setCreatedDate($date);
            $glJuridicalPerson->setValidations();
            $glJuridicalPerson->create();
            if ($glJuridicalPerson->isValid()){ 
              $glPersonRelationship->setIdPerson($glPerson->getId());
              $glPersonRelationship->setIdRelationship(1);
              $glPersonRelationship->setDateFrom(date("Y-m-d"));
              $glPersonRelationship->setActive('Y');
              $glPersonRelationship->setCreatedBy($user);
              $glPersonRelationship->setCreatedDate($date);
              $glPersonRelationship->setValidations();
              $glPersonRelationship->create();
              if ($glPersonRelationship->isValid()){ 
                $tfs->checkTrans();
                $tfs->backTrail();
              }
            }
          
        }

      }  
      
      break;
    case "AA":
      if ($glPerson->getPersonType()=="N"){
          $glNaturalPerson->update();
          if ($glNaturalPerson->isValid()){ 
            $tfs->checkTrans();
            $tfs->backTrail(TRUE);
          }  
      }else{
          $glJuridicalPerson->update();
          if ($glJuridicalPerson->isValid()){ 
            $tfs->checkTrans();
            $tfs->backTrail(TRUE);
          }  
      } 
      
      break;
     case "DI":
      $go=true;

      $glPersonRelationship->setActive('N');
      $glPersonRelationship->update();
      if ($glPersonRelationship->isValid()){ 
        foreach ($glPersonContactList as $row){
          $glPersonContact = new GlPersonContact($tfs);
          $glPersonContact->populatePK($row["id"]); 
          $glPersonContact->setActive("N");
          $glPersonContact->update();
          if ($glPersonContact->isValid()){ 
            $glPersonContact->revocar();            
          }else{
            $go=false;
          }  
        }
        if ($go){ 
          $tfs->checkTrans();
          $tfs->backTrail(TRUE);  
        } 
       } 
         
  
      break; 
    case "EN":
      $go=true;
      $glPersonRelationship->setActive('Y');
      $glPersonRelationship->update();

     if ($glPersonRelationship->isValid()){ 
        /*foreach ($glPersonContactList as $row){
          $glPersonContact = new GlPersonContact($tfs);
          $glPersonContact->populatePK($row["id"]); 
          $glPersonContact->setActive("Y");
          $glPersonContact->update();
          if (!$glPersonContact->isValid()){ 
            $go=false;
          }  
        }*/
       if ($go){ 
          $tfs->checkTrans();
          $tfs->backTrail();  
        }  
      } 
      
      break;   
    case "AB":
      $glPersonRelationship->delete();
      if ($glPersonRelationship->isValid()){ 
        if ($glPerson->getPersonType()=="N"){
            $glNaturalPerson->delete();
            if ($glNaturalPerson->isValid()){ 
              $glPerson->delete();
              if ($glPerson->isValid()){
                $tfs->checkTrans();
                $tfs->backTrail(TRUE);
              }

            }
        }else{
            $glJuridicalPerson->delete();
            if ($glJuridicalPerson->isValid()){
              $glPerson->delete();
              if ($glPerson->isValid()){
                $tfs->checkTrans();
                $tfs->popTrail();
                $tfs->backTrail(TRUE);
              } 
              
            }  
        } 

      }  

      break;

   
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/GlCustomer.rvw.php");
    }elseif ($tfRequestAction=="SH"){
      require("view/GlCustomerSh.vw.php");
    }else{

     if ($glPerson->getPersonType()=='N'){
        require("view/GlCustomerN.vw.php");
      }else{
        require("view/GlCustomerJ.vw.php");
      }
    } 
  }
?>
