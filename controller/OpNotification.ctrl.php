<?php
        use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
  if ($tfRequestAction=="AL"){
     $opNotificationList=OpNotification::dataList($tfs);
  }else{
     $opNotification = new OpNotification($tfs);
     $opNotification->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $opNotification->setCreatedBy($tfs->getUserId());
      $opNotification->setCreatedDate(date("Y-m-d H:i:s"));
      $opNotification->setValidations();
      $opNotification->create();
      if ($opNotification->isValid()){ 
        $tfs->checkTrans();
        
        $body = file_get_contents("template/notification.html");
        $body = str_replace('{%BODY%}',$opNotification->getBody(),$body);
    

       $mail = new PHPMailer(true);

       try {
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;

        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->IsSMTP();
        $mail->SMTPDebug  = 0;
        $mail->Host       = 'smtp.gmail.com';
        $mail->Port       = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth   = true;
        $mail->Username = TF_MAIL_ACCOUNT;
        $mail->Password = TF_MAIL_PASSWORD;
        

        $address = GlPersonContact::listByReport($tfs,$opNotification->getIdInspectionReport());

        foreach ($address as $row){
         $mail->AddAddress($row["account"],$row["name"]);
        }  
        
        
        $mail->From = TF_MAIL_ACCOUNT;
        $mail->SetFrom(TF_MAIL_ACCOUNT, TF_MAIL_FROM);
        $mail->Subject = 'GRAINCA - '.utf8_decode($opNotification->getSubject());
        $mail->MsgHTML($body);
        $mail->Body = $body;
         
        if (count($address)>0){
          $mail->send();
          $opNotification->setObjMsg("Notificaci&oacute;n enviada satisfactoriamente");
          $tfs->backTrail(TRUE);
        } else{
          $opNotification->setObjError("Este cliente no tiene contactos registrados");
        }
        
      } catch (Exception $e) {
        $opNotification->setObjError("Error al enviar notificaci&oacute;n");
      } 
      }
      break;
    case "AA":
      $opNotification->update();
      if ($opNotification->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("OpNotification","AE",$tfResponse->encrypt(array("op_notification_id" => $opNotification->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $opNotification->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/OpNotification.rvw.php");
    }else{
      require("view/OpNotification.vw.php");
    } 
  }
?>
