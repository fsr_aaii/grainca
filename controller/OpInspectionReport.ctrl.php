<?php
  //use PhpOffice\PhpSpreadsheet\Spreadsheet;
  //use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  //use PhpOffice\PhpSpreadsheet\IOFactory;


  if ($tfRequestAction=="AL"){
     $opInspectionReportList=OpInspectionReport::dataList($tfs);
  }else{
     
     $opInspectionReport = new OpInspectionReport($tfs);
     $opInspectionReport->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 


     $opDestinationList=OpDestination::dLByReport($tfs,$opInspectionReport->getId());

     $opNotificationList=OpNotification::dLByReport($tfs,$opInspectionReport->getId());


  }


  switch ($tfRequestAction){
    
    case "AN":
       break;

    case "ANSSS":
     
    $client = OpInspectionReport::getClient();

    $drive = new Google_Service_Drive($client); 
    
    OpInspectionReport::sync($tfs,$drive,$client,'1llssrd48gOZ_pKYAX0xlvcggK9m3fq_R');

    $tfs->checkTrans();


      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":

     $pos = strpos(OpProduct::description($tfs,$opInspectionReport->getIdProduct1()),' ');

     if ($pos === false) {
        $opInspectionReport->setName($opInspectionReport->getVessel().'-'.OpProduct::description($tfs,$opInspectionReport->getIdProduct1()).'-'.OpPortPier::description($tfs,$opInspectionReport->getIdPortPier()).'-'.date("YmdHis"));
    } else {
        $opInspectionReport->setName($opInspectionReport->getVessel().'-'.strstr(OpProduct::description($tfs,$opInspectionReport->getIdProduct1()), ' ', true).'-'.OpPortPier::description($tfs,$opInspectionReport->getIdPortPier()).'-'.date("YmdHis"));
    }
      $opInspectionReport->setActive('Y');
      $opInspectionReport->setProcess('N');
      $opInspectionReport->setRobDate(date("Y-m-d H:i:s"));
      $opInspectionReport->setLastProcessing(date("Y-m-d H:i:s"));      
      $opInspectionReport->setCreatedDate(date("Y-m-d H:i:s"));
      $opInspectionReport->setCreatedBy($tfs->getUserId());
      $opInspectionReport->setCreatedDate(date("Y-m-d H:i:s"));
      $opInspectionReport->setValidations();
      $opInspectionReport->create();
      if ($opInspectionReport->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }else{
         print_r($opInspectionReport->getAttrErrors());
      }

      break;
    case "AA":

      $opInspectionReport->update();
      if ($opInspectionReport->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("OpInspectionReport","AE",$tfResponse->encrypt(array("op_inspection_report_id" => $opInspectionReport->getId())),2);
        $tfRequestAction="AE";
      }else{
        print_r($opInspectionReport->getAttrErrors());
      }

               
      break;
    case "AX":
      $opInspectionReport->setActive('N');
      $opInspectionReport->setValidations();
      $opInspectionReport->update();
      if ($opInspectionReport->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }else{
         print_r($opInspectionReport->getAttrErrors());
      }
      break;  
    case "AB":
      $opInspectionReport->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }

  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
   if  ($tfRequestAction=="AL"){
     require("view/OpInspectionReport.rvw.php");
   }else{
      require("view/OpInspectionReport.vw.php");
    
  } 
    
  }

?>
