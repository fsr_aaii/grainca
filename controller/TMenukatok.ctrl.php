<?php 
    
  	$entities=TComponent::allEntities($tfs);

  	$entity=explode(".",$tfRequest->mt_entity);

	$gAllCheck = "";
	$gModelBaseCheck = "";
	$gModelCheck = "";
	$gControllerCheck = "";
	$gViewCheck = "";
	//$gROViewCheck = "";
	$gDTViewCheck = "";
	//$gJSViewCheck = "";
	//$gJSDataTableCheck = "";
	//$gCallbackFnCheck = ""; 
	$gAuditTrigger = "";

	if ($tfRequest->gAll=="gAll"){
		$gAllCheck = "checked";
	}
	if ($tfRequest->gModelBase=="gModelBase"){
		$gModelBaseCheck = "checked";
	}
	if ($tfRequest->gModel=="gModel"){
		$gModelCheck = "checked";
	}
	if ($tfRequest->gController=="gController"){
	    $gControllerCheck = "checked";
	}
	if ($tfRequest->gView=="gView"){
	   $gViewCheck = "checked";
	}
	if ($tfRequest->gDTView=="gDTView"){
	   $gDTViewCheck = "checked";
	}
	/*if ($tfRequest->gJSView=="gJSView"){
	   $gJSViewCheck = "checked";
	}
	if ($tfRequest->gJSDataTable=="gJSDataTable"){
	   $gJSDataTableCheck = "checked";
	}
	if ($tfRequest->gCallbackFn=="gCallbackFn"){
	   $gCallbackFnCheck = "checked";
	}*/
    if ($tfRequest->gAuditTrigger=="gAuditTrigger"){
	   $gAuditTrigger = "checked";
	} 

	switch  ($tfRequestAction){
		case "GN": 
		    $logs.='<div class="log started">'.date("h:i:s").' Event start log time</div>';
		   $tComponent = new TComponent($tfs);
		   $tComponent->populateName($entity[0],$entity[1]);
		   
        
            $dir='controller/'.$tComponent->getName().'.ctrl.php';
			if(is_file($dir)){			
				if ($tfRequest->gController=="gController"){
		            $file=fopen($dir,"w+");			
					fwrite ($file,$tComponent->gController());
					fclose($file);	
					$logs.='<div class="log success">'.date("h:i:s")." Controller file has been overwrite";
				}else{
					$logs.='<div class="log stop">'.date("h:i:s")." Controller file has not been overwrite";
				}				
			}else{										
				$file=fopen($dir,"w+");				
				fwrite ($file,$tComponent->gController());
				fclose($file);	
				$logs.='<div class="log success">'.date("h:i:s")." Controller file has been create";
            }

		    /*$dir='function/'.$tComponent->getName().'.fn.php';
			if(is_file($dir)){			
				if ($tfRequest->gCallbackFn=="gCallbackFn"){
					$file=fopen($dir,"w+");			
					fwrite ($file,$tComponent->gCallbackFn());
					fclose($file);	
					$logs.='<div class="log success">'.date("h:i:s")." Callback function file has been overwrite";
				}else{
					$logs.='<div class="log stop">'.date("h:i:s")." Callback function file has not been overwrite";
				}				
			}else{										
				$file=fopen($dir,"w+");				
				fwrite ($file,$tComponent->gCallbackFn());
				fclose($file);	
				$logs.='<div class="log success">'.date("h:i:s")." Callback function file has been create";		
			}

			$dir='javascript/'.$tComponent->getName().'.js';
			if(is_file($dir)){			
				if ($tfRequest->gJSView=="gJSView"){
					$file=fopen($dir,"w+");			
					fwrite ($file,$tComponent->gJSView());
					fclose($file);	
					$logs.='<div class="log success">'.date("h:i:s")." JS file of view has been overwrite";
				}else{
					$logs.='<div class="log stop">'.date("h:i:s")." JS file of view has not been overwrite";
				}				
			}else{										
				$file=fopen($dir,"w+");				
				fwrite ($file,$tComponent->gJSView());
				fclose($file);	
				$logs.='<div class="log success">'.date("h:i:s")." JS file of view has been create";		
			}

			$dir='javascript/'.$tComponent->getName().'.rep.js';
			if(is_file($dir)){			
				if ($tfRequest->gJSDataTable=="gJSDataTable"){
					$file=fopen($dir,"w+");			
					fwrite ($file,$tComponent->gJSDataTable());
					fclose($file);	
					$logs.='<div class="log success">'.date("h:i:s")." JS file of data table has been overwrite";
				}else{
					$logs.='<div class="log stop">'.date("h:i:s")." JS file of data table has not been overwrite";
				}				
			}else{										
				$file=fopen($dir,"w+");				
				fwrite ($file,$tComponent->gJSDataTable());
				fclose($file);	
				$logs.='<div class="log success">'.date("h:i:s")." JS file of data table has been create";		
			}
            */
			$dir='modelBase/'.$tComponent->getName().'Base.mdl.php';
			if(is_file($dir)){	
				if ($tfRequest->gModelBase=="gModelBase"){
					$file=fopen($dir,"w+");			
					fwrite ($file,$tComponent->gModelBase());
					fclose($file);	
					$logs.='<div class="log success">'.date("h:i:s")." Model Base class has been overwrite";
				}else{
					$logs.='<div class="log stop">'.date("h:i:s")." Model Base class has not been overwrite";
				}				
			}else{										
				$file=fopen($dir,"w+");				
				fwrite ($file,$tComponent->gModelBase());
				fclose($file);	
				$logs.='<div class="log success">'.date("h:i:s")." Model class has been create";			
			}
			
			$dir='model/'.$tComponent->getName().'.mdl.php';
			if(is_file($dir)){			
				if ($tfRequest->gModel=="gModel"){
					$file=fopen($dir,"w+");			
					fwrite ($file,$tComponent->gModel());
					fclose($file);	
					$logs.='<div class="log success">'.date("h:i:s")."  Model class has been overwrite";
				}else{
					$logs.='<div class="log stop">'.date("h:i:s")."  Model class has not been overwrite";
				}				
			}else{										
				$file=fopen($dir,"w+");				
				fwrite ($file,$tComponent->gModel());
				fclose($file);	
				$logs.='<div class="log success">'.date("h:i:s")."  Model class has been create";				
			}
			$dir='view/'.$tComponent->getName().'.rvw.php';
			if(is_file($dir)){			
				if ($tfRequest->gDTView=="gDTView"){
					$file=fopen($dir,"w+");			
					fwrite ($file,$tComponent->gDTView());
					fwrite ($file,'<script type="text/javascript">');
					fwrite ($file,$tComponent->gJSDataTable());
					fwrite ($file,'</script>');
					fclose($file);	
					$logs.='<div class="log success">'.date("h:i:s")." Data Table file has been overwrite";
				}else{
					$logs.='<div class="log stop">'.date("h:i:s")." Data Table file has not been overwrite";
				}				
			}else{										
				$file=fopen($dir,"w+");				
				fwrite ($file,$tComponent->gDTView());
				fwrite ($file,'<script type="text/javascript">');
				fwrite ($file,$tComponent->gJSDataTable());
				fwrite ($file,'</script>');
				fclose($file);	
				$logs.='<div class="log success">'.date("h:i:s")." Data Table file has been create";	
			}

			$dir='view/'.$tComponent->getName().'.vw.php';
			if(is_file($dir)){			
				if ($tfRequest->gView=="gView"){
					$file=fopen($dir,"w+");			
					fwrite ($file,$tComponent->gView());
					fwrite ($file,'<script type="text/javascript">');
					fwrite ($file,$tComponent->gJSView());
					fwrite ($file,'</script>');
					fclose($file);	
					$logs.='<div class="log success">'.date("h:i:s")." View file has been overwrite";
				}else{
					$logs.='<div class="log stop">'.date("h:i:s")." View file has not been overwrite";
				}				
			}else{										
				$file=fopen($dir,"w+");				
				fwrite ($file,$tComponent->gView());
					fwrite ($file,'<script type="text/javascript">');
					fwrite ($file,$tComponent->gJSView());
					fwrite ($file,'</script>');
				fclose($file);	
				$logs.='<div class="log success">'.date("h:i:s")." View file has been create";			
			}

			$dir='trigger/'.$tComponent->getName().'.sql';
			if(is_file($dir)){			
				if ($tfRequest->gAuditTrigger=="gAuditTrigger"){
					$file=fopen($dir,"w+");			
					fwrite ($file,$tComponent->gAuditTrigger());
					fclose($file);	
					$logs.='<div class="log success">'.date("h:i:s")." Audit trigger file has been overwrite";
				}else{
					$logs.='<div class="log stop">'.date("h:i:s")." Audit trigger file has not been overwrite";
				}				
			}else{										
				$file=fopen($dir,"w+");				
				fwrite ($file,$tComponent->gAuditTrigger());
				fclose($file);	
				$logs.='<div class="log success">'.date("h:i:s")." Audit trigger file has been create";			
			}
         
          $tfs->checkTrans();
		  $logs.='<div class="log finished">'.date("h:i:s").' Event end log time</div>';	
          break;
    }
require("view/TMenukatok.vw.php");
?>
