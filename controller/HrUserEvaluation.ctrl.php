<?php
  if ($tfRequestAction=="AL"){
     $hrUserEvaluationList=HrUserEvaluation::dataList($tfs);
  }else{
     $hrUserEvaluation = new HrUserEvaluation($tfs);
     $hrUserEvaluation->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrUserEvaluation->setCreatedBy($tfs->getUserId());
      $hrUserEvaluation->setCreatedDate(date("Y-m-d H:i:s"));
      $hrUserEvaluation->setValidations();
      $hrUserEvaluation->create();
      if ($hrUserEvaluation->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $hrUserEvaluation->update();
      if ($hrUserEvaluation->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrUserEvaluation","AE",$tfResponse->encrypt(array("hr_user_evaluation_id" => $hrUserEvaluation->getId())),2);
        $tfRequestAction="AE";
      break;

    case "AF":
    
      $hrUserEvaluationCriteriaList=HrUserEvaluationCriteria::dLbyEvaluation($tfs,$hrUserEvaluation->getId());
      $affected_rows=0;
      $right=0;
      $wrong=0;
      $empty=0;
      foreach ($hrUserEvaluationCriteriaList as $row){
        $hrUserEvaluationCriteria = new HrUserEvaluationCriteria($tfs);
        $hrUserEvaluationCriteria->populatePk($row["id"]);
        $hrUserEvaluationCriteria->setValue($tfRequest->get("hr_user_evaluation_criteria_".$row["id_evaluation_criteria"]));
        $hrUserEvaluationCriteria->setValidations();
        $hrUserEvaluationCriteria->update();
        if (!$hrUserEvaluationCriteria->isValid()){ 
          break;
        }else{
          if (in_array("This record has been updated", $hrUserEvaluationCriteria->getObjMsg())) {
           $affected_rows++;
          
           if (HrEvaluationCriteriaOption::isRight($tfs,$hrUserEvaluationCriteria->getValue())){
             $right++;
           }else{
             $wrong++;
           }
          }else{
            $empty++;
          }


        }
      }
      //if ($affected_rows>0){
        $hrUserEvaluation->setAreRight(strval($right));
        $hrUserEvaluation->setAreWrong(strval($wrong));
        $hrUserEvaluation->setAreEmpty(strval($empty));
        $hrUserEvaluation->setActive('N');
        $hrUserEvaluation->setEndTimestamp(date("Y-m-d H:i:s"));
        $hrUserEvaluation->setValidations();
        $hrUserEvaluation->update();
      //}
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);  
      break;
    case "AB":
      $hrUserEvaluation->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrUserEvaluation.rvw.php");
    }else{
      require("view/HrUserEvaluation.vw.php");
    } 
  }
?>
