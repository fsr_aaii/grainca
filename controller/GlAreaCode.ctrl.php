<?php
  if ($tfRequestAction=="AL"){
     $glAreaCodeList=GlAreaCode::dataList($tfs);
  }else{
     $glAreaCode = new GlAreaCode($tfs);
     $glAreaCode->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $glAreaCode->setValidations();
      $glAreaCode->create();
      if ($glAreaCode->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $glAreaCode->update();
      if ($glAreaCode->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("GlAreaCode","AE",$tfResponse->encrypt(array("gl_area_code_id" => $glAreaCode->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $glAreaCode->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/GlAreaCode.rvw.php");
    }else{
      require("view/GlAreaCode.vw.php");
    } 
  }
?>
