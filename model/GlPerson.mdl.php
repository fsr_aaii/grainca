<?php
  class GlPerson extends GlPersonBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "person_type"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM gl_person
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM gl_person
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.person_type,
                 b.name created_by,
                 a.created_date
            FROM gl_person a,
                 t_user b
           WHERE b.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function find(TfSession $tfs,$ninType,$nin,$ninControlDigit){
    if (in_array($ninType,array("V","E"))){
      $q = "SELECT id
              FROM gl_natural_person
             WHERE nin_type = ?
               AND nin = ?";
    
      $param = array($ninType,$nin);
      list($rs) = $tfs->executeQuery($q,$param);
    }else{
      $q = "SELECT id
              FROM gl_juridical_person
             WHERE nin_type = ?
               AND nin = ?
               AND nin_control_digit = ?";
    
      $param = array($ninType,$nin,$ninControlDigit);
      list($rs) = $tfs->executeQuery($q,$param);

    }  
    return $rs["id"];
  }

  public function getClientId(){
      $q = "SELECT a.id
            FROM gl_person_relationship a
           WHERE a.id_person = ?
             AND a.id_relationship = 1";
    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);

    return $rs["id"];
  }
}
?>
