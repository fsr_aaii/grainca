<?php
  class GlPersonContact extends GlPersonContactBase {
   
  protected $user;
   
  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_person"=>true,
                              "name"=>true,
                              "job"=>true,
                              "account"=>true,
                              "phone_number"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }

  public function setUser($value){
    $this->user=$value;
  }
  public function getUser(){
    return $this->user;
  }


  protected function dbPopulate($id){ 
    parent::dbPopulate($id);

     $q="SELECT active
          FROM t_user a 
         WHERE a.id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    if ($rs["active"]=='Y'){
       $this->user="checked";  
    }else{
       $this->user="";
    }
  }

  protected function uiPopulate($request){ 

    parent::uiPopulate($request);
    $this->user=$request->get("gl_person_contact_user");

  }


  public function otorgar(){ 

     $q="SELECT id
          FROM t_user a 
         WHERE a.login=?";

    $param = array($this->account);
    list($rs) = $this->tfs->executeQuery($q,$param);

    if ($rs["id"]!=''){
        $q="UPDATE t_user
               SET active='Y'
             WHERE login=?";

          $param = array($this->account);
          $this->tfs->execute($q,$param);
    }else{
      $q="INSERT INTO t_user(id,login,active,password,password_date,email_account,name,created_by,created_date)
          SELECT DISTINCT ?,account,'Y','202cb962ac59075b964b07152d234b70',current_date(),account,name,1,NOW()
            FROM gl_person_contact
           WHERE id = ?";

      $sq = TfEntity::getSequence($this->tfs,'t_user');  
      $param = array($sq,$this->id);
      $this->tfs->execute($q,$param);   

      $q="INSERT INTO t_user_role (id_user,id_functional_area_role) VALUES (?,3)";

      $param = array($sq);
      $this->tfs->execute($q,$param);          
    }

  }


  public function revocar(){ 

    $q="UPDATE t_user
           SET active='N'
         WHERE login=?";

    $param = array($this->account);
    $this->tfs->execute($q,$param);

  }

  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM gl_person_contact
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM gl_person_contact
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }

  public function personIsActive(){ 
    $q = "SELECT active
            FROM gl_person_relationship
           WHERE id_person=?
             AND id_relationship = 1";
             
    $param = array($this->id_person);
    list($rs) = $this->tfs->executeQuery($q,$param);

    return $rs["active"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_person,
                 a.name,
                 a.job,
                 a.account,
                 a.phone_number,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM gl_person_contact a,
                 gl_person b,
                 t_user c
           WHERE b.id = a.id_person
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

   public static function dLByPerson(TfSession $tfs,$idPerson){
    $q = "SELECT a.id,
                 a.id_person,
                 a.name,
                 a.job,
                 a.account,
                 a.phone_number,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM gl_person_contact a,
                 gl_person b,
                 t_user c
           WHERE b.id = a.id_person
           AND c.id = a.created_by
           AND a.id_person = ?";

    $param=array($idPerson);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function listByReport(TfSession $tfs,$idReport){
    $q = "SELECT a.name,
                 a.account                 
            FROM gl_person_contact a,
                 op_inspection_report b
           WHERE a.id_person = b.id_customer
           AND b.id = ?";

    $param=array($idReport);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }


  
}
?>
