<?php
  class OpDestination extends OpDestinationBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_inspection_report"=>true,
                              "name"=>true,
                              "qty"=>true,
                              "BL"=>true,
                              "active"=>true,
                              "row"=>true,
                              "process"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
   
  public function create(){
    parent::create();

    $client = OpInspectionReport::getClient();
    $sheet = new Google_Service_Sheets($client);

    $params = ['valueInputOption' => 'RAW'];
    $update_range ="Destinos!A".$this->row.":B".$this->row ;
    if (is_numeric($this->BL)){
      $values = [[$this->name,intval($this->BL)]];   
    }else{
      $values = [[$this->name,$this->BL]];   
    }
    $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
    $update_sheet = $sheet->spreadsheets_values->update(OpInspectionReport::idFile($this->tfs,$this->id_inspection_report), $update_range, $body, $params);

    

  } 

  public function update(){
    parent::update();

    $client = OpInspectionReport::getClient();
    $sheet = new Google_Service_Sheets($client);

   $params = ['valueInputOption' => 'RAW'];
     $update_range ="Destinos!A".$this->row.":B".$this->row ;
      if (is_numeric($this->BL)){
      $values = [[$this->name,intval($this->BL)]];   
    }else{
      $values = [[$this->name,$this->BL]];   
    } 
     $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update(OpInspectionReport::idFile($this->tfs,$this->id_inspection_report), $update_range, $body, $params);

  } 

  public function delete(){
    $this->active='N';
    parent::update();

    $client = OpInspectionReport::getClient();
    $sheet = new Google_Service_Sheets($client);

   $params = ['valueInputOption' => 'RAW'];
     $update_range ="Destinos!A".$this->row.":B".$this->row ;
     $values = [['','']];   
     $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update(OpInspectionReport::idFile($this->tfs,$this->id_inspection_report), $update_range, $body, $params);

  } 
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM op_destination
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }


  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM op_destination
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }


  public static function id(TfSession $tfs,$file,$name){ 
    $q = "SELECT a.id
            FROM op_destination a,
                 op_inspection_report b
           WHERE b.id_file = ?
             AND b.id = a.id_inspection_report
             AND a.active = 'Y'
             AND TRIM(a.name)=?";
    $param = array($file,$name);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["id"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_inspection_report,
                 a.name,
                 a.qty,
                 a.BL,
                 c.name created_by,
                 a.created_date
            FROM op_destination a,
                 op_inspection_report b,
                 t_user c
           WHERE b.id = a.id_inspection_report
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
   public static function dLByReport(TfSession $tfs,$id_inspection_report){
    $q = " SELECT b.id,b.name,b.qty, b.BL
      FROM op_destination b 
      WHERE b.active = 'Y'
        AND b.id_inspection_report = ?";
    
    $param = array($id_inspection_report);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

   public static function DetailByReport(TfSession $tfs,$id_inspection_report){
    $q = " SELECT x.id,x.destination name,cantidad_kgr qty,x.bl BL, 
      round(cantidad_kgr/round(x.KG_APROX_PESADO/x.VEH_PESADO,0),0) cant_veh_estim,round(x.KG_APROX_PESADO/x.VEH_PESADO,0) promedio_kgs,
      VEH_SICA, VEH_SICA*round(x.KG_APROX_PESADO/x.VEH_PESADO,0) KG_APROX_SICA,x.VEH_PESADO,x.KG_APROX_PESADO, cantidad_kgr-x.KG_APROX_PESADO remanente_kgs,round((cantidad_kgr-x.KG_APROX_PESADO)/round(x.KG_APROX_PESADO/x.VEH_PESADO,0),0) remanente_unidades      
  FROM (SELECT b.id,c.id id_inspection_report,b.name destination,b.qty cantidad_kgr, b.BL,COUNT(a.net_weight) VEH_SICA,SUM(CASE WHEN coalesce(a.net_weight,0) > 0 THEN 1 ELSE 0 END) VEH_PESADO,SUM(REPLACE(a.net_weight, \".\", \"\")) KG_APROX_PESADO 
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND a.active = 'Y'      
    GROUP BY c.id,b.id) x";
    
    $param = array($id_inspection_report);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  
  public static function detailTransport(TfSession $tfs,$id){
    $q = " SELECT TRIM(a.transport) transport,COUNT(a.net_weight) VEH_SICA,
          SUM(CASE WHEN coalesce(a.net_weight,0) > 0 THEN 1 ELSE 0 END) VEH_PESADO,SUM(REPLACE(a.net_weight, \".\", \"\")) KG_APROX_PESADO 
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id = ?
      AND b.id = a.id_destination
      AND a.BL IS NOT NULL
      AND a.active = 'Y'      
    GROUP BY TRIM(a.transport)
    ORDER BY 2 DESC,3 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function detailTransportCargado(TfSession $tfs,$id){
    $q = " SELECT TRIM(a.transport) transport,
                  COUNT(a.id) qty,
                  SUM(REPLACE(a.net_weight, \".\", \"\")) kg
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id = ?
      AND b.id = a.id_destination
      AND a.status_1 = 'CARGADO'
      AND a.id_travel IS NOT NULL
      AND a.BL IS NOT NULL
      AND a.active = 'Y'      
    GROUP BY TRIM(a.transport)
    ORDER BY 2 DESC,3 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function detailTransportDespachado(TfSession $tfs,$id){
    $q = " SELECT TRIM(a.transport) transport,
                  COUNT(a.id) guide_qty,
                  SUM(CASE WHEN a.status_2 = 'DESPACHADO' THEN 1 ELSE 0 END) qty,
                  SUM(REPLACE(a.net_weight, \".\", \"\")) kg
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id = ?
      AND b.id = a.id_destination
      AND a.status_2 = 'DESPACHADO'
      AND a.id_travel IS NOT NULL
      AND a.BL IS NOT NULL 
      AND a.active = 'Y'      
    GROUP BY TRIM(a.transport)
    ORDER BY 2 DESC,3 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  
   public static function detailTransportEmitida(TfSession $tfs,$id){
    $q = " SELECT TRIM(a.transport) transport,
                  COUNT(a.id) guide_qty,
                  SUM(CASE WHEN a.status_2 IS NOT NULL THEN 1 ELSE 0 END) qty,
                  SUM(REPLACE(a.net_weight, \".\", \"\")) kg
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id = ?
      AND b.id = a.id_destination
      AND a.BL IS NOT NULL 
      AND a.active = 'Y'      
    GROUP BY TRIM(a.transport)
    ORDER BY 2 DESC,3 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  
  public static function detailPorDespachar(TfSession $tfs,$id){

    $q = " SELECT b.id,a.transport,a.name,a.nin,a.chuto,a.SICA_guide
              FROM op_inspection_report_detail a,
                op_destination b,
                op_inspection_report c 
              WHERE c.id = b.id_inspection_report
              AND b.active = 'Y'
              AND b.id = ?
              AND b.id = a.id_destination
              AND a.status_2 IS NULL
              AND a.BL IS NOT NULL 
              AND a.active = 'Y'      
            ORDER BY TRIM(a.transport)";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function detailTransportDespachado2(TfSession $tfs,$id){
    $q = " SELECT TRIM(a.transport) transport,
                  COUNT(a.id) qty,
                  SUM(REPLACE(a.net_weight, \".\", \"\")) kg
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id = ?
      AND b.id = a.id_destination
      AND a.status_2 = 'DESPACHADO'
      AND a.id_travel IS NOT NULL
      AND a.BL IS NOT NULL 
      AND a.active = 'Y'      
    GROUP BY TRIM(a.transport)
    ORDER BY 2 DESC,3 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function qtyByReport(TfSession $tfs,$id_inspection_report){
    $q = "SELECT count(*) qty
            FROM op_destination a
           WHERE a.id_inspection_report = ?";
    
    $param = array($id_inspection_report);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["qty"];
  }
}
?>
