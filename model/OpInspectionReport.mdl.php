<?php

  use PhpOffice\PhpSpreadsheet\Spreadsheet;
  use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  use PhpOffice\PhpSpreadsheet\IOFactory;

  class OpInspectionReport extends OpInspectionReportBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_customer"=>true,
                              "vessel"=>true,
                              "id_port_pier"=>true,
                              "id_product_1"=>true,
                              "id_product_2"=>true,
                              "product_1_rob"=>true,
                              "product_1_other_rob"=>true,
                              "vessel_rob"=>true,
                              "vessel_other_rob"=>true,
                              "rob_date"=>true,
                              "BL"=>true,
                              "quantity"=>true,
                              "inspection_date"=>true,
                              "issued_date"=>true,
                              "completion_date"=>true,
                              "created_by"=>true,
                              "created_date"=>true,
                               "inspection_report_no"=>true,
                               "id_file"=>true,
                               "name"=>true,
                               "version"=>true,
                               "process"=>true,
                               "last_processing"=>true,
                              "active"=>true);



               

  }
  public function create(){

    $client = OpInspectionReport::getClient();
    $drive = new Google_Service_Drive($client); 
    $this->copyBaseFile($client,$drive);

    parent::create();  
  }


  public function update(){

    $client = OpInspectionReport::getClient();
    $drive = new Google_Service_Drive($client); 
    $this->updateBaseFile($client,$drive);

    parent::update();  
  }


  public function delete(){

    $client = OpInspectionReport::getClient();
    $drive = new Google_Service_Drive($client); 
    $response = $drive->files->delete($this->id_file);

    parent::delete();  
  }


private function copyBaseFile($client,$service){
  $origin_file_id = "1xecmHQRI1q2awSZcgsxcLs-9mO7DmbV2eLvZmxqptgw";
  $copiedFile = new Google_Service_Drive_DriveFile();
  $copiedFile->setParents(["1llssrd48gOZ_pKYAX0xlvcggK9m3fq_R"]);

   
  $copiedFile->setName($this->name);


  try {
      
      $response = $service->files->copy($origin_file_id, $copiedFile);


      $params = ['valueInputOption' => 'RAW'];

      $sheet = new Google_Service_Sheets($client);



      $update_range ="Base de Datos!B1" ;
      $values = [[$this->vessel]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);

      $update_range ="Base de Datos!F1" ;
      $values = [[OpPortPier::description($this->tfs,$this->id_port_pier)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);

      $update_range ="Base de Datos!H1" ;
      $values = [[intval($this->quantity)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);


      $update_range ="Base de Datos!B2" ;
      $values = [[OpProduct::description($this->tfs,$this->id_product_1)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);


      /*$update_range ="Base de Datos!B3" ;
      if ($this->id_product_2!=''){
         $values = [[OpProduct::description($this->tfs,$this->id_product_2)]];  
      }else{
         $values = [[""]];  
      }   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);*/

      $update_range ="Base de Datos!B3" ;
      $values = [[GlJuridicalPerson::description($this->tfs,$this->id_customer)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);

     
      //$ownerPermission = new Google_Service_Drive_Permission();
      //$ownerPermission->setEmailAddress("{myemailhere}");
      //$ownerPermission->setType('user');
      //$ownerPermission->setRole('owner');
      //$service->permissions->create("{sheet_id_here}", $ownerPermission, 
       //   ['emailMessage' => 'You added a file to ' .
       //   static::$applicationName . ': ' . "Does this work"]);
       //   
       $this->version=$response["version"];
       $this->id_file = $response["id"];


  } catch (Exception $e) {
      print "An error occurred: " . $e->getMessage();
  }
}


private function updateBaseFile($client,$service){
  
  try {
      
     
      /*$pos = strpos($this->product,' ');

     if ($pos === false) {
        $name = $this->getVessel().'-'.$this->getProduct().'-'.OpPortPier::description($tfs,$this->getIdPortPier()).'-'.date("YmdHis"));
    } else {
        $opInspectionReport->setName($opInspectionReport->getVessel().'-'.strstr($opInspectionReport->getProduct(), ' ', true).'-'.OpPortPier::description($tfs,$opInspectionReport->getIdPortPier()).'-'.date("YmdHis"));
    }

     $file = new Google_Service_Drive_DriveFile();
    $file->setTitle($newTitle);

    $updatedFile = $service->files->patch($fileId, $file, array(
      'fields' => 'title'
    ));
*/

      $params = ['valueInputOption' => 'RAW'];

      $sheet = new Google_Service_Sheets($client);



      $update_range ="Base de Datos!B1" ;
      $values = [[$this->vessel]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);

      $update_range ="Base de Datos!F1" ;
      $values = [[OpPortPier::description($this->tfs,$this->id_port_pier)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);

      $update_range ="Base de Datos!H1" ;
      $values = [[intval($this->quantity)]];    
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);


       $update_range ="Base de Datos!B2" ;
      $values = [[OpProduct::description($this->tfs,$this->id_product_1)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);


      /*$update_range ="Base de Datos!B3" ;
      if ($this->id_product_2!=''){
         $values = [[OpProduct::description($this->tfs,$this->id_product_2)]];  
      }else{
         $values = [[""]];  
      }   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);*/

      $update_range ="Base de Datos!B3" ;
      $values = [[GlJuridicalPerson::description($this->tfs,$this->id_customer)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);


  } catch (Exception $e) {
      print "An error occurred: " . $e->getMessage();
  }
}
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM op_inspection_report
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM op_inspection_report
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }

  public static function version(TfSession $tfs,$id){ 
    $q = "SELECT  version
            FROM op_inspection_report
           WHERE id_file=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["version"];
  }

   public static function idFile(TfSession $tfs,$id){ 
    $q = "SELECT id_file
            FROM op_inspection_report
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["id_file"];
  }
  
  public static function actives(TfSession $tfs){ 
    $q = "SELECT id_file
            FROM op_inspection_report
           WHERE active='Y'";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function info(TfSession $tfs,$id){ 
    $q = "SELECT a.id,
                 a.id_customer,
                 a.vessel,
                 a.BL,
                 c.name port_pier,
                 b.trade_name customer,
                 e.name product_1,
                 f.name product_2,
                 a.quantity,
                 a.product_1_rob,a.vessel_rob,a.completion_date,a.active
            FROM op_inspection_report a LEFT JOIN op_product e ON e.id = a.id_product_1
                                        LEFT JOIN op_product f ON f.id = a.id_product_2,
                 gl_juridical_person b,
                 op_port_pier c
           WHERE b.id = a.id_customer 
           AND b.id = a.id_customer
           AND c.id = a.id_port_pier
           AND a.id=?";
    

     $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;
  }

  
  public static function DetailByReport(TfSession $tfs,$id_inspection_report){
    $q = " SELECT cantidad_kgr qty,round(cantidad_kgr/round(x.KG_APROX_PESADO/x.VEH_PESADO,0),0) cant_veh_estim,
                  round(x.KG_APROX_PESADO/x.VEH_PESADO,0) promedio_kgs,x.created_date,
                  VEH_SICA, VEH_SICA*round(x.KG_APROX_PESADO/x.VEH_PESADO,0) KG_APROX_SICA,
                  x.VEH_PESADO,x.KG_APROX_PESADO, cantidad_kgr-x.KG_APROX_PESADO remanente_kgs,
                  round((cantidad_kgr-x.KG_APROX_PESADO)/round(x.KG_APROX_PESADO/x.VEH_PESADO,0),0) remanente_unidades,
                  CARGADO_POR_PESAR,A_COSTADO_BUQUE,round((cantidad_kgr-x.KG_APROX_PESADO)+x.vessel_other_rob) ROB_BUQUE,
                  round((cantidad_kgr-x.KG_APROX_PESADO)+x.product_1_other_rob) ROB_PRODUCTO,x.vessel_other_rob,x.product_1_other_rob,x.rob_date
             FROM (SELECT c.id,c.product_1_rob,c.product_1_other_rob,c.vessel_rob,c.vessel_other_rob,c.rob_date,
                          SUM(DISTINCT b.qty) cantidad_kgr,COUNT(b.net_weight) VEH_SICA,
                          SUM(CASE WHEN coalesce(b.net_weight,0) > 0 THEN 1 ELSE 0 END) VEH_PESADO,
                          SUM(REPLACE(b.net_weight, \".\", \"\")) KG_APROX_PESADO,
                          SUM(CASE WHEN UPPER(b.status_1) = 'CARGADO' THEN CASE WHEN coalesce(b.net_weight,0) > 0 THEN 0 ELSE 1 END ELSE 0 END)  CARGADO_POR_PESAR,
                          SUM(CASE WHEN UPPER(b.status_1) = 'COSTADO DE BUQUE'  THEN 1 ELSE 0 END)  A_COSTADO_BUQUE,
                          MAX(b.created_date) created_date
                FROM op_inspection_report c LEFT JOIN (SELECT b.id_inspection_report,b.qty,a.net_weight,a.status_1,a.created_date
                                                         FROM op_destination b LEFT JOIN op_inspection_report_detail a ON a.active = 'Y' AND  a.id_destination = b.id
                                                         WHERE b.active = 'Y') b ON b.id_inspection_report  = c.id           
                WHERE c.id = ?  
              GROUP BY c.id,c.product_1_rob,c.product_1_other_rob,c.vessel_rob,c.vessel_other_rob,c.rob_date) x";
    
    $param = array($id_inspection_report);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;
  }


  public static function detailDestinationCargado(TfSession $tfs,$id){
    $q = " SELECT b.id,TRIM(b.name) destination,
                  COUNT(a.id) qty,
                  SUM(REPLACE(a.net_weight, \".\", \"\")) kg
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND a.status_1 = 'CARGADO'
      AND a.BL IS NOT NULL
      AND a.active = 'Y'      
    GROUP BY b.id,TRIM(b.name)
     ORDER BY 2 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function detailDestinationDespachado(TfSession $tfs,$id){
    $q = " SELECT b.id,TRIM(b.name) destination,
                  COUNT(a.id) guide_qty,
                  SUM(CASE WHEN a.status_2 = 'DESPACHADO' THEN 1 ELSE 0 END) qty,
                  SUM(REPLACE(a.net_weight, \".\", \"\")) kg
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND a.id_travel IS NOT NULL
      AND a.BL IS NOT NULL
      AND a.active = 'Y'      
    GROUP BY b.id,TRIM(b.name)
     ORDER BY 2 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  
  public static function detailDestinationDespachado2(TfSession $tfs,$id){
    $q = " SELECT b.id,TRIM(b.name) destination,
                  COUNT(a.id) qty,
                  SUM(REPLACE(a.net_weight, \".\", \"\")) kg
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND a.status_2 = 'DESPACHADO'
      AND a.BL IS NOT NULL
      AND a.active = 'Y'      
    GROUP BY b.id,TRIM(b.name)
     ORDER BY 2 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  
  public static function detailDestinationEmitida(TfSession $tfs,$id){
    $q = " SELECT b.id,TRIM(b.name) destination,
                  COUNT(a.id) guide_qty,
                  SUM(CASE WHEN a.status_2 IS NOT NULL THEN 1 ELSE 0 END) qty,
                  SUM(REPLACE(a.net_weight, \".\", \"\")) kg
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND a.BL IS NOT NULL
      AND a.active = 'Y'      
    GROUP BY b.id,TRIM(b.name)
     ORDER BY 2 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  } 
  public static function detailPorDespachar(TfSession $tfs,$idDestination,$idTransport){

    $q = " SELECT b.id,a.transport,a.name,a.nin,a.chuto,a.SICA_guide
              FROM op_inspection_report_detail a,
                op_destination b,
                op_inspection_report c 
              WHERE c.id = b.id_inspection_report
              AND b.active = 'Y'
              AND b.id = ?
              AND b.id = a.id_destination
              AND TRIM(a.transport) = ?
              AND a.status_2 IS NULL
              AND a.BL IS NOT NULL 
              AND a.active = 'Y'";
    
    $param = array($idDestination,$idTransport);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  } 

  public static function allPorDespachar(TfSession $tfs,$id){

    $q = " SELECT b.id,b.name destination,TRIM(a.transport) transport,a.name,a.nin,a.chuto,a.SICA_guide
              FROM op_inspection_report_detail a,
                op_destination b,
                op_inspection_report c 
              WHERE c.id = b.id_inspection_report
              AND b.active = 'Y'
              AND b.id_inspection_report = ?
              AND b.id = a.id_destination
              AND a.status_2 IS NULL
              AND a.BL IS NOT NULL 
              AND a.active = 'Y'
              ORDER BY TRIM(a.transport)";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  } 
  public static function detailTransport(TfSession $tfs,$id){
    $q = " SELECT TRIM(a.transport) transport,COUNT(a.net_weight) VEH_SICA,
          SUM(CASE WHEN coalesce(a.net_weight,0) > 0 THEN 1 ELSE 0 END) VEH_PESADO,SUM(REPLACE(a.net_weight, \".\", \"\")) KG_APROX_PESADO 
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND a.active = 'Y'      
    GROUP BY TRIM(a.transport)
     ORDER BY 2 DESC,3 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }


  

  public static function detailTransport2(TfSession $tfs,$id){
    $q = " SELECT TRIM(a.transport) transport,a.SICA_guide,UPPER(a.name) name,b.name destination
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND coalesce(a.net_weight,0) = 0
      AND a.active = 'Y'    
     ORDER BY 2 DESC,3 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function detailTransport3(TfSession $tfs,$id){
    $q = " SELECT TRIM(a.transport) transport,count(*) qty
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND coalesce(a.net_weight,0) = 0
      AND a.active = 'Y'    
     GROUP BY 1
     ORDER BY 2 DESC  ";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function detailTransport4(TfSession $tfs,$id,$transport){
    $q = " SELECT b.id,TRIM(b.name) destination,count(*) qty
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND TRIM(a.transport) = ?
      AND coalesce(a.net_weight,0) = 0
      AND a.active = 'Y'    
     GROUP BY 1
     ORDER BY 2 DESC  ";
    
    $param = array($id,$transport);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function detailTransport5(TfSession $tfs,$idDestination,$transport){
    $q = " SELECT a.SICA_guide,UPPER(a.name) name,a.nin,a.chuto
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id = ?
      AND b.id = a.id_destination
      AND TRIM(a.transport) = ?
      AND coalesce(a.net_weight,0) = 0
      AND a.active = 'Y'
     ORDER BY 1 ASC  ";
    
    $param = array($idDestination,$transport);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_customer,
                 a.vessel,
                 a.BL,
                 c.name port_pier,
                 b.trade_name customer,
                 e.name product_1,
                 f.name product_2,
                 a.quantity,
                 a.inspection_date
            FROM op_inspection_report a LEFT JOIN op_product e ON e.id = a.id_product_1
                                        LEFT JOIN op_product f ON f.id = a.id_product_2,
                 gl_juridical_person b,
                 op_port_pier c
           WHERE b.id = a.id_customer 
           AND b.id = a.id_customer
           AND c.id = a.id_port_pier
           AND a.active='Y'
           ORDER BY a.inspection_date DESC";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }


  public static function dLByUser(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_customer,
                 a.vessel,
                 a.BL,
                 c.name port_pier,
                 b.trade_name customer,
                 e.name product_1,
                 f.name product_2,
                 a.quantity,
                 a.inspection_date
            FROM op_inspection_report a LEFT JOIN op_product e ON e.id = a.id_product_1
                                        LEFT JOIN op_product f ON f.id = a.id_product_2,
                 gl_juridical_person b,
                 op_port_pier c,
                 gl_person_contact d
           WHERE b.id = a.id_customer 
           AND b.id = a.id_customer
           AND c.id = a.id_port_pier
           AND a.active='Y'
           AND a.id_customer = d.id_person
           AND d.account=?
           ORDER BY a.inspection_date DESC";
    $param = array($tfs->getUserLogin());       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function HistoricalByUser(TfSession $tfs,$vessel,$product,$port_pier,$date_1,$date_2){
    
    if ($vessel!=''){
       $vessel = '%'.str_replace(' ', '%', strtoupper($vessel)).'%';
    }
    $q = "SELECT a.id,
                 a.id_customer,
                 a.vessel,
                 a.BL,
                 c.name port_pier,
                 b.trade_name customer,
                 e.name product_1,
                 f.name product_2,
                 a.quantity,
                 a.inspection_date
            FROM op_inspection_report a LEFT JOIN op_product e ON e.id = a.id_product_1
                                        LEFT JOIN op_product f ON f.id = a.id_product_2,
                 gl_juridical_person b,
                 op_port_pier c,
                 gl_person_contact d
           WHERE b.id = a.id_customer 
           AND b.id = a.id_customer
           AND c.id = a.id_port_pier
           AND (UPPER(a.vessel) LIKE ? OR 'X' = ?)
           AND (UPPER(a.id_product_1) = ? OR 'X' = ?)
           AND (UPPER(a.id_port_pier) = ? OR 'X' = ?)
           AND (a.inspection_date >= ? OR '3000-01-01' = ?)
           AND (a.inspection_date <= ? OR '3000-01-01' = ?)
           AND a.id_customer = d.id_person
           AND d.account=?
           ORDER BY a.inspection_date DESC";


    $param = array($vessel,($vessel == '') ? 'X' : 'Y',
                   $product,($product == '') ? 'X' : 'Y',
                   $port_pier,($port_pier == '') ? 'X' : 'Y',
                   ($date_1 == '') ? '3000-01-01' : $date_1,
                   ($date_1 == '') ? '3000-01-01' : '3000-01-02',
                   ($date_2 == '') ? '1900-01-01' : $date_2,
                   ($date_2 == '') ? '3000-01-01' : '3000-01-02',
                   $tfs->getUserLogin());     
  
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function Historical(TfSession $tfs,$customer,$vessel,$product,$port_pier,$date_1,$date_2){
    
    if ($vessel!=''){
       $vessel = '%'.str_replace(' ', '%', strtoupper($vessel)).'%';
    }
    $q = "SELECT a.id,
                 a.id_customer,
                 a.vessel,
                 a.BL,
                 c.name port_pier,
                 b.trade_name customer,
                 e.name product_1,
                 f.name product_2,
                 a.quantity,
                 a.inspection_date
            FROM op_inspection_report a LEFT JOIN op_product e ON e.id = a.id_product_1
                                        LEFT JOIN op_product f ON f.id = a.id_product_2,
                 gl_juridical_person b,
                 op_port_pier c
           WHERE b.id = a.id_customer 
           AND b.id = a.id_customer
           AND c.id = a.id_port_pier
           AND (UPPER(a.vessel) LIKE ? OR 'X' = ?)
           AND (UPPER(a.id_product_1) = ? OR 'X' = ?)
           AND (UPPER(a.id_port_pier) = ? OR 'X' = ?)
           AND (a.inspection_date >= ? OR '3000-01-01' = ?)
           AND (a.inspection_date <= ? OR '3000-01-01' = ?)
           AND (a.id_customer = ? OR 'X' = ?) 
           ORDER BY a.inspection_date DESC";


    $param = array($vessel,($vessel == '') ? 'X' : 'Y',
                   $product,($product == '') ? 'X' : 'Y',
                   $port_pier,($port_pier == '') ? 'X' : 'Y',
                   ($date_1 == '') ? '3000-01-01' : $date_1,
                   ($date_1 == '') ? '3000-01-01' : '3000-01-02',
                   ($date_2 == '') ? '1900-01-01' : $date_2,
                   ($date_2 == '') ? '3000-01-01' : '3000-01-02',
                   $customer,($customer == '') ? 'X' : 'Y');     
  
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }


  
  public static function process(TfSession $tfs){ 
    $q = "SELECT  id
            FROM op_inspection_report
           WHERE process='Y'";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function getClient(){
    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(array(Google_Service_Sheets::SPREADSHEETS_READONLY,Google_Service_Drive::DRIVE));

    $client->setAuthConfig('environment/credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'environment/google.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        }
    }
    return $client;
  }

  

static public function sync2(TfSession $tfs,$service, $client,$folderId) {

$optParams = array(
  'pageSize' => 100,
  'fields' => 'nextPageToken, files(id, name,headRevisionId,md5Checksum,modifiedTime,version,size)',
  'q' => "'".$folderId."' in parents",
);


$service->files->emptyTrash();

$results = $service->files->listFiles($optParams);

foreach ($results->getFiles() as $file) {
 //echo $file["name"]." - ".$file["id"]." - ".$file["size"]." - ".$file["md5Checksum"]." - ".$file["modifiedTime"]." - ".$file["version"]."<br>";
  

  $version=OpInspectionReport::version($tfs,$file["id"]);
  $process = FALSE;
  if ($version!=''){
    if ($version!=$file["version"]){
      $q = "UPDATE op_inspection_report 
               SET name=?,version=?,process='Y',last_processing=?
             WHERE id = ?";

      $param=array($file["name"],$file["version"],date("Y-m-d H:i:s"),$file["id"]);

      $rs = $tfs->execute($q,$param);
      $process = TRUE;
    }
  }else{
     $q = "INSERT INTO op_inspection_report (id,name,version,process,last_processing,created_by,created_date)
        VALUES (?,?,?,'Y',?,?,?)";


     $param=array($file["id"],$file["name"],$file["version"],date("Y-m-d H:i:s"),$tfs->getUserId(),date("Y-m-d H:i:s"));

     $rs = $tfs->execute($q,$param);
     $process = TRUE;
  }



 
  if ($process){
    $http = $client->authorize();
    $fp = fopen('upload/xlsx/'.$file["id"].'.xlsx', 'w');
    $chunkSizeBytes = 1 * 1024 * 1024;
    $chunkStart = 0;

    while ($chunkStart < $file["size"]) {
      $chunkEnd = $chunkStart + $chunkSizeBytes;
      $response = $http->request(
        'GET',
        sprintf('/drive/v3/files/%s', $file["id"]),
        [
          'query' => ['alt' => 'media'],
          'headers' => [
            'Range' => sprintf('bytes=%s-%s', $chunkStart, $chunkEnd)
          ]
        ]
      );
      $chunkStart = $chunkEnd + 1;
      fwrite($fp, $response->getBody()->getContents());
    }
    fclose($fp);

    }


    $opInspectionReportList = OpInspectionReport::process($tfs);

    foreach ($opInspectionReportList as $row){
     $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
      $spreadsheet = $reader->load('upload/xlsx/'.$file["id"].'.xlsx');
      
      $opInspectionReport = new OpInspectionReport($tfs);
      $opInspectionReport->populatePk($row["id"]);
      
      $GlPersonRelationshipId = GlPersonRelationship::customerId($tfs,$spreadsheet->getSheetByName("Portada")->getCell('F8'));
      
    
      if ($GlPersonRelationshipId!=''){
         $opInspectionReport->setIdCustomer($GlPersonRelationshipId);
      }
      $opInspectionReport->setProduct($spreadsheet->getSheetByName("Portada")->getCell('F11'));
      $opInspectionReport->setQuantity($spreadsheet->getSheetByName("Portada")->getCell('F14'));
      $opPortPierId = OpPortPier::id($tfs,$spreadsheet->getSheetByName("Portada")->getCell('F17'));
      if ($opPortPierId!=''){
         $opInspectionReport->setIdPortPier($opPortPierId);
      }
      $opInspectionReport->setVessel($spreadsheet->getSheetByName("Portada")->getCell('F20'));
      $opInspectionReport->setInspectionDate(TfWidget::dmy2ymd($spreadsheet->getSheetByName("Portada")->getCell('F23')));
      $opInspectionReport->setInspectionReportNo($spreadsheet->getSheetByName("Portada")->getCell('F26'));
      
      $opInspectionReport->setIssuedDate(TfWidget::dmy2ymd($spreadsheet->getSheetByName("Portada")->getCell('F29')));
      $opInspectionReport->setProcess('N');
      $opInspectionReport->setLastProcessing(date("Y-m-d H:i:s"));
      $opInspectionReport->setValidations();
      $opInspectionReport->update();
      if ($opInspectionReport->isValid()){ 
        $tfs->checkTrans();
      }
      
     
    }


    


//echo $spreadsheet->getSheetByName("Portada")->getCell('C9')->getValue();

  


  
  /*$http = $client->authorize();

    // Open a file for writing
    $fp = fopen('upload/xlsx/'.$file["id"].'.xlsx', 'w');

    // Download in 1 MB chunks
    $chunkSizeBytes = 1 * 1024 * 1024;
    $chunkStart = 0;

    // Iterate over each chunk and write it to our file
    while ($chunkStart < $file["size"]) {
      $chunkEnd = $chunkStart + $chunkSizeBytes;
      $response = $http->request(
        'GET',
        sprintf('/drive/v3/files/%s', $file["id"]),
        [
          'query' => ['alt' => 'media'],
          'headers' => [
            'Range' => sprintf('bytes=%s-%s', $chunkStart, $chunkEnd)
          ]
        ]
      );
      $chunkStart = $chunkEnd + 1;
      fwrite($fp, $response->getBody()->getContents());
    }
    // close the file pointer
    fclose($fp);

  */
  }
}



static public function sync(TfSession $tfs,$service, $client,$folderId) {

$optParams = array(
  'pageSize' => 100,
  'fields' => 'nextPageToken, files(id, name,headRevisionId,md5Checksum,modifiedTime,version,size)',
  'q' => "'".$folderId."' in parents",
);


$service->files->emptyTrash();

$results = $service->files->listFiles($optParams);

$files = $results->getFiles();

$rs = OpInspectionReport::actives($tfs);  

$actives = array_column($rs, 'id_file');

//print_r($actives);

foreach ($results->getFiles() as $file) {
 
 if (in_array($file["id"], $actives)) {
    echo $file["name"]." - ".$file["id"]." - ".$file["size"]." - ".$file["md5Checksum"]." - ".$file["modifiedTime"]." - ".$file["version"]."<br>";


  $version=OpInspectionReport::version($tfs,$file["id"]);
  echo "Version ".$version."<br>";

  $process = FALSE;
  if ($version!=''){
    if ($version!=$file["version"]){
      echo "New Version<br>";
      $sheets = new Google_Service_Sheets($client); // <--- This is from your script.
      $spreadsheetId = $file["id"];
      $range = 'Base de Datos!A4:I4';
    
      $robs = $sheets->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'COLUMNS']);
      $separators = array(".", ",", " ");

      if (count($robs)>0){
        $rob1 = str_replace($separators, "",$robs['values'][1][0]);
        $rob2 = str_replace($separators, "",$robs['values'][5][0]);
        $rob3 = str_replace($separators, "",$robs['values'][7][0]);
      }else{
          $rob1='';
          $rob2='';
          $rob3='';
      }
      /*print_r($robs['values']);
      echo $robs['values'][1][0]."x<br>";
      echo $robs['values'][5][0]."x<br>";
      echo $robs['values'][7][0]."x<br>";*/
      try{
        $q = "UPDATE op_inspection_report 
                 SET name=?,version=?,process='Y',last_processing=?,vessel_other_rob=?,product_1_other_rob=?,rob_date=?
               WHERE id_file = ?";

        $param=array($file["name"],$file["version"],date("Y-m-d H:i:s"),
                     $rob1==''?NULL:$rob1,
                     $rob2==''?NULL:$rob2,
                     $rob3==''?NULL:$rob3,$file["id"]);

        $rs = $tfs->execute($q,$param);

        $sheets = new Google_Service_Sheets($client); // <--- This is from your script.
        $spreadsheetId = $file["id"];
        $process = TRUE;

      }catch(Throwable $t){
        print_r($param);
        echo "<br>No Process: ".$t->getMessage()."<br>";
        $tfs->setState("Start");
        $process = FALSE;
      }
    }
  }else{
      echo "First Version<br>";
      
      $sheets = new Google_Service_Sheets($client); // <--- This is from your script.
      $spreadsheetId = $file["id"];
      $range = 'Base de Datos!A4:I4';
    
      $robs = $sheets->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'COLUMNS']);
      
      echo $robs['values'][7][0]==''?"NULL":"Z".$robs['values'][7][0];


      $q = "UPDATE op_inspection_report 
               SET name=?,version=?,process='Y',last_processing=?,vessel_other_rob=?,product_1_other_rob=?,rob_date=?
             WHERE id_file = ?";

      $param=array($file["name"],$file["version"],date("Y-m-d H:i:s"),
                   $robs['values'][1][0]==''?NULL:str_replace('.','',$robs['values'][1][0]),
                   $robs['values'][5][0]==''?NULL:str_replace('.','',$robs['values'][5][0]),
                   $robs['values'][7][0]==''?NULL:$robs['values'][7][0],$file["id"]);
      print_r($param);

      $rs = $tfs->execute($q,$param);

     
     
      $process = TRUE;
  }



  if ($process){
     echo "PROCESAR<br>";
    $listHash= OpInspectionReportDetail::listHash($tfs,$file["id"]);

    OpInspectionReportDetail::inactive($tfs,$file["id"]);

    $range = 'Base de Datos!A8:AB';
    
    $rows = $sheets->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);
    
    if (isset($rows['values'])) {
      $i="0"; 
      foreach ($rows['values'] as $row) {
       if (empty($row[3])) {
            echo "BREAK ".$row[0]."<br>";
            $tfs->checkTrans();
            break;
            
        }
        $opInspectionReportDetail = new OpInspectionReportDetail($tfs);

       if (md5(json_encode($row))==$listHash[$i]["hash"]){
        echo "SIN CAMBIOS ".$row[0]."<br>";
        $opInspectionReportDetail->populatePK($listHash[$i]["id"]);
        $opInspectionReportDetail->setActive('Y');

        $opInspectionReportDetail->update();
        if ($opInspectionReportDetail->isValid()){ 
          //$tfs->checkTrans();
        }else{
            print_r($opInspectionReportDetail->getAttrErrors());
            print_r($row);
            echo "<br>";
            $tfs->setState("Start");
            //break;
          }
       }else{
        echo "CAMBIO ".$row[0]."<br>";

        $opInspectionReportDetail->setRow($i);
        $opInspectionReportDetail->setHash(md5(json_encode($row)));
        $opInspectionReportDetail->setIdTravel($row[0]);
        $opInspectionReportDetail->setSICAGuide($row[1]);
        $opInspectionReportDetail->setIdList($row[2]);
        $opInspectionReportDetail->setTransport($row[3]);
        $opInspectionReportDetail->setName($row[4]);
        $opInspectionReportDetail->setNin($row[5]);
        $opInspectionReportDetail->setChuto($row[6]);
        $opInspectionReportDetail->setIdDestination(OpDestination::id($tfs,$file["id"],$row[7]));
        $opInspectionReportDetail->setBL($row[8]);
        $opInspectionReportDetail->setAdmissionDate($row[9]);
        $opInspectionReportDetail->setAdmissionTime($row[10]);
        $opInspectionReportDetail->setTaraNumber($row[11]);
        $opInspectionReportDetail->setStatus1($row[12]);
        $opInspectionReportDetail->setOperationDay($row[13]);
        $opInspectionReportDetail->setSurveyGuide($row[14]);
        $opInspectionReportDetail->setOcamarGuide($row[15]);
        $opInspectionReportDetail->setStore($row[16]);
        $opInspectionReportDetail->setGrossWeight($row[17]);
        $opInspectionReportDetail->setTaraWeight($row[18]);
        $opInspectionReportDetail->setNetWeight($row[19]);
        $opInspectionReportDetail->setAggregate($row[20]);
        $opInspectionReportDetail->setROB($row[21]);
        $opInspectionReportDetail->setStatus2($row[22]);
        $opInspectionReportDetail->setIdTravelOcamar($row[23]);
        $opInspectionReportDetail->setDepartureDate($row[24]);
        $opInspectionReportDetail->setDepartureTime($row[25]);
        $opInspectionReportDetail->setSeal($row[26]);
        $opInspectionReportDetail->setObservation($row[27]);
        $opInspectionReportDetail->setActive('Y');
        $opInspectionReportDetail->setCreatedBy($tfs->getUserId());
        $opInspectionReportDetail->setCreatedDate(date("Y-m-d H:i:s"));
        
        $opInspectionReportDetail->setValidations();
        
        try{
          $opInspectionReportDetail->create();
          if ($opInspectionReportDetail->isValid()){ 
            //$tfs->checkTrans();
          }else{
            print_r($opInspectionReportDetail->getAttrErrors());
            print_r($row);
            echo "<br>";
            $tfs->setState("Start");
            //break;
          }
        }catch(Throwable $t){
                 echo "catch".$t;
                 $tfs->setState("Start");
                 //break;
         }  
       }         
       $i++;
       //echo json_encode($row)."<br>";
      }
    }
  }
 } 
}


$q = "INSERT INTO op_inspection_report_detail_h
      SELECT * FROM op_inspection_report_detail
       WHERE active = 'N'";
$tfs->execute($q);

$q = "DELETE FROM op_inspection_report_detail
       WHERE active = 'N'";
$tfs->execute($q);

}



}
?>