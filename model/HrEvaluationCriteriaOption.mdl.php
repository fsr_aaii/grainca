<?php
  class HrEvaluationCriteriaOption extends HrEvaluationCriteriaOptionBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_evaluation_criteria"=>true,
                              "name"=>true,
                              "correct"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM hr_evaluation_criteria_option
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM hr_evaluation_criteria_option
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }

  public static function isRight(TfSession $tfs,$id){ 
    $q = "SELECT correct
            FROM hr_evaluation_criteria_option
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    if ($rs["correct"]=="Y"){
      return true;
    }else{
      return false; 
    }
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.description id_evaluation_criteria,
                 a.name,
                 a.correct,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM hr_evaluation_criteria_option a,
                 hr_evaluation_criteria b,
                 t_user c
           WHERE b.id = a.id_evaluation_criteria
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function dLbyCriteria(TfSession $tfs,$id_evaluation_criteria){
    $q = "SELECT a.id,a.id_evaluation_criteria,a.name
            FROM hr_evaluation_criteria_option a
           WHERE a.id_evaluation_criteria = ?";
    
    $param = array($id_evaluation_criteria);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
