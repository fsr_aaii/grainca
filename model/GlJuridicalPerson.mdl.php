<?php
  class GlJuridicalPerson extends GlJuridicalPersonBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "nin_type"=>true,
                              "nin"=>true,
                              "nin_control_digit"=>true,
                              "legal_name"=>true,
                              "trade_name"=>true,
                              "legal_address"=>true,
                              "id_area_code"=>true,
                              "phone_number"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", legal_name \"option\"
            FROM gl_juridical_person
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  legal_name description
            FROM gl_juridical_person
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.nin_type,
                 a.nin,
                 a.nin_control_digit,
                 a.legal_name,
                 a.trade_name,
                 a.legal_address,
                 b.description id_area_code,
                 a.phone_number,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM gl_juridical_person a,
                 gl_area_code b,
                 t_user c
           WHERE b.id = a.id_area_code
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
