<?php
  class GlPersonRelationship extends GlPersonRelationshipBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_person"=>true,
                              "id_relationship"=>true,
                              "date_from"=>true,
                              "date_to"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM gl_person_relationship
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM gl_person_relationship
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_person,
                 c.name id_relationship,
                 a.date_from,
                 a.date_to,
                 a.active,
                 d.name created_by,
                 a.created_date
            FROM gl_person_relationship a,
                 gl_person b,
                 gl_relationship c,
                 t_user d
           WHERE b.id = a.id_person
           AND c.id = a.id_relationship
           AND d.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function customerList(TfSession $tfs){
    $q = "SELECT b.id,b.trade_name name,CONCAT(b.nin_type,b.nin,'-',b.nin_control_digit) rif,
                 a.date_from,a.id id_relationship,b.phone_number
            FROM gl_person_relationship a,
               gl_juridical_person b
           WHERE b.id = a.id_person
             AND a.id_relationship = 1";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function customerId(TfSession $tfs,$name){
    $q = "SELECT b.id
            FROM gl_person_relationship a,
               gl_juridical_person b
           WHERE (UPPER(b.trade_name)=UPPER(?) OR UPPER(b.legal_name)=UPPER(?))
             AND b.id = a.id_person
             AND a.id_relationship = 1";
    $param = array($name,$name);
    
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs['id'];
  }
}
?>
