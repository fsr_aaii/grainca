<?php
  class GlNaturalPerson extends GlNaturalPersonBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "nin_type"=>true,
                              "nin"=>true,
                              "nin_control_digit"=>true,
                              "first_name"=>true,
                              "middle_name"=>true,
                              "last_name"=>true,
                              "second_last_name"=>true,
                              "photo"=>true,
                              "id_nationality_country"=>true,
                              "id_gender"=>true,
                              "id_birth_country"=>true,
                              "birthplace"=>true,
                              "birthdate"=>true,
                              "id_marital_status"=>true,
                              "height"=>true,
                              "size"=>true,
                              "weight"=>true,
                              "is_right_handed"=>true,
                              "id_blood_type"=>true,
                              "is_functional_diversity"=>true,
                              "id_study_level"=>true,
                              "id_residence_country"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM gl_natural_person
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM gl_natural_person
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.nin_type,
                 a.nin,
                 a.nin_control_digit,
                 a.first_name,
                 a.middle_name,
                 a.last_name,
                 a.second_last_name,
                 a.photo,
                 a.id_nationality_country,
                 a.id_gender,
                 a.id_birth_country,
                 a.birthplace,
                 a.birthdate,
                 a.id_marital_status,
                 a.height,
                 a.size,
                 a.weight,
                 a.is_right_handed,
                 a.id_blood_type,
                 a.is_functional_diversity,
                 a.id_study_level,
                 a.id_residence_country,
                 b.name created_by,
                 a.created_date
            FROM gl_natural_person a,
                 t_user b
           WHERE b.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
