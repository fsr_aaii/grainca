<?php
  class OpPortPier extends OpPortPierBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "name"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM op_port_pier
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM op_port_pier
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }

  public static function id(TfSession $tfs,$name){ 
    $q = "SELECT id description
            FROM op_port_pier
           WHERE UPPER(name)=UPPER(?)";
    $param = array($name);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.name,
                 CASE WHEN a.active = 'Y' THEN 'SI' ELSE 'NO' END active,
                 b.name created_by,
                 a.created_date
            FROM op_port_pier a,
                 t_user b
           WHERE b.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
