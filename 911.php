<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/png" href="<?php echo TF_BASE_PATH;?>/asset/images/shortcut.png">
  <title>Guaramo</title>

  <link rel="stylesheet" type="text/css" href="<?php echo TF_BASE_PATH;?>/vendor/bootstrap-4.5.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo TF_BASE_PATH;?>/font/fontawesome-free-5.12.1-web/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo TF_BASE_PATH;?>/font/fontawesome/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo TF_BASE_PATH;?>/font/font-lato/latofonts.css">
  <link rel="stylesheet" type="text/css" href="<?php echo TF_BASE_PATH;?>/font/font-lato/latostyle.css">
  <link rel="stylesheet" type="text/css" href="<?php echo TF_BASE_PATH;?>/asset/css/page-error.css">


</head>

<body  class="h-100" >
      <div id="page-error" >
        <div class="d-flex align-content-center flex-wrap">
          <div  class="col-2 mx-auto">
            <div  class="col-12 mx-auto">
              <img src="<?php echo TF_BASE_PATH;?>/asset/images/logologin.png" class="img-fluid logo-login">
            </div>
             <div class="col-12 pl-3 mt-3 body text-center">
                <span class="error"><?php echo $t->getCode();?> ERROR</span>
                <span class="message"><?php echo $t->getMessage();?></span>
             </div>  
          </div>
          <div class="row col-12">
            <div  class="col-lg-6 mx-auto">
               <div class="col-12 pl-3 mt-3 body text-center">
                  <span class="message">
                    <?php 
                      if (TF_PROFILE=='dev'){
                        print_r($t->getTrace());
                      }
                    ?>
                   </span> 
               </div>   
            </div>
           </div>   
        </div>
      </div>

</body>

</html>


