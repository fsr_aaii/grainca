<?php
  class GlPersonRelationshipBase extends TfEntity {
    protected $id;
    protected $id_person;
    protected $id_relationship;
    protected $date_from;
    protected $date_to;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="gl_person_relationship";
  }

  private function getAll(){

    $q="SELECT id,
               id_person,
               id_relationship,
               date_from,
               date_to,
               active,
               created_by,
               created_date
          FROM gl_person_relationship
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    $this->id_person=$rs["id_person"];
    $this->id_relationship=$rs["id_relationship"];
    $this->date_from=$rs["date_from"];
    $this->date_to=$rs["date_to"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->gl_person_relationship_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_gl_person_relationship){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_gl_person_relationship; 
    }

    if ($tfRequest->exist("gl_person_relationship_id_person")){
      $this->id_person=$tfRequest->gl_person_relationship_id_person;
    }
    if ($tfRequest->exist("gl_person_relationship_id_relationship")){
      $this->id_relationship=$tfRequest->gl_person_relationship_id_relationship;
    }
    if ($tfRequest->exist("gl_person_relationship_date_from")){
      $this->date_from=$tfRequest->gl_person_relationship_date_from;
    }
    if ($tfRequest->exist("gl_person_relationship_date_to")){
      $this->date_to=$tfRequest->gl_person_relationship_date_to;
    }
    if ($tfRequest->exist("gl_person_relationship_active")){
      $this->active=$tfRequest->gl_person_relationship_active;
    }
    if ($tfRequest->exist("gl_person_relationship_created_by")){
      $this->created_by=$tfRequest->gl_person_relationship_created_by;
    }
    if ($tfRequest->exist("gl_person_relationship_created_date")){
      $this->created_date=$tfRequest->gl_person_relationship_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_person"]=array("type"=>"number",
                                  "value"=>$this->id_person,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_relationship"]=array("type"=>"number",
                                  "value"=>$this->id_relationship,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["date_from"]=array("type"=>"date",
                                  "value"=>$this->date_from,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["date_to"]=array("type"=>"date",
                                  "value"=>$this->date_to,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdPerson($value){
  $this->id_person=$value;
  }
  public function getIdPerson(){
  return $this->id_person;
  }
  public function setIdRelationship($value){
  $this->id_relationship=$value;
  }
  public function getIdRelationship(){
  return $this->id_relationship;
  }
  public function setDateFrom($value){
  $this->date_from=$value;
  }
  public function getDateFrom(){
  return $this->date_from;
  }
  public function setDateTo($value){
  $this->date_to=$value;
  }
  public function getDateTo(){
  return $this->date_to;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    if ($this->id==""){
      $this->id = $this->sequence();
    }
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO gl_person_relationship(id,
                               id_person,
                               id_relationship,
                               date_from,
                               date_to,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_person==''?NULL:$this->id_person,
                     $this->id_relationship==''?NULL:$this->id_relationship,
                     $this->date_from==''?NULL:$this->date_from,
                     $this->date_to==''?NULL:$this->date_to,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="Tu registro ha sido creado";
      $rs=$this->getAll();
      $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="El campo ((id) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_person!= $rs["id_person"]){
          if ($this->updateable["id_person"]){
            $set.=$set_aux."id_person=?";
            $set_aux=",";
            $param[]=$this->id_person==''?NULL:$this->id_person;
          }else{
            $this->objError[]="El campo ((id_person) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_relationship!= $rs["id_relationship"]){
          if ($this->updateable["id_relationship"]){
            $set.=$set_aux."id_relationship=?";
            $set_aux=",";
            $param[]=$this->id_relationship==''?NULL:$this->id_relationship;
          }else{
            $this->objError[]="El campo ((id_relationship) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->date_from!= $rs["date_from"]){
          if ($this->updateable["date_from"]){
            $set.=$set_aux."date_from=?";
            $set_aux=",";
            $param[]=$this->date_from==''?NULL:$this->date_from;
          }else{
            $this->objError[]="El campo ((date_from) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->date_to!= $rs["date_to"]){
          if ($this->updateable["date_to"]){
            $set.=$set_aux."date_to=?";
            $set_aux=",";
            $param[]=$this->date_to==''?NULL:$this->date_to;
          }else{
            $this->objError[]="El campo ((date_to) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="El campo ((active) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="El campo ((created_by) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="El campo ((created_date) no se puede modificar";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE gl_person_relationship ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="Este registro ha sido actualizado";
            $rs=$this->getAll();
            $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
          }else{
            $this->objMsg[]="Este registro no tiene datos para actualizar";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM gl_person_relationship
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
