<?php
  class GlAreaCodeBase extends TfEntity {
    protected $id;
    protected $id_country;
    protected $code;
    protected $description;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="gl_area_code";
  }

  private function getAll(){

    $q="SELECT id,
               id_country,
               code,
               description
          FROM gl_area_code
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    $this->id_country=$rs["id_country"];
    $this->code=$rs["code"];
    $this->description=$rs["description"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->gl_area_code_id);
      if ($this->initialState!=""){
        if ($this->initialState!=$tfRequest->is_gl_area_code){
          $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
          $this->valid = false;
        }
    }else{
      $this->initialState=$tfRequest->is_gl_area_code; 
    }

    if ($tfRequest->exist("gl_area_code_id_country")){
      $this->id_country=$tfRequest->gl_area_code_id_country;
    }
    if ($tfRequest->exist("gl_area_code_code")){
      $this->code=$tfRequest->gl_area_code_code;
    }
    if ($tfRequest->exist("gl_area_code_description")){
      $this->description=$tfRequest->gl_area_code_description;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_country"]=array("type"=>"string",
                                  "value"=>$this->id_country,
                                  "length"=>2,
                                  "required"=>true);
    $this->validation["code"]=array("type"=>"number",
                                  "value"=>$this->code,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["description"]=array("type"=>"string",
                                  "value"=>$this->description,
                                  "length"=>40,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdCountry($value){
  $this->id_country=$value;
  }
  public function getIdCountry(){
  return $this->id_country;
  }
  public function setCode($value){
  $this->code=$value;
  }
  public function getCode(){
  return $this->code;
  }
  public function setDescription($value){
  $this->description=$value;
  }
  public function getDescription(){
  return $this->description;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO gl_area_code(id,
                               id_country,
                               code,
                               description)
            VALUES (?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_country==''?NULL:$this->id_country,
                     $this->code==''?NULL:$this->code,
                     $this->description==''?NULL:$this->description);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="Tu registro ha sido creado";
      $rs=$this->getAll();
      $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="El campo ((id) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_country!= $rs["id_country"]){
          if ($this->updateable["id_country"]){
            $set.=$set_aux."id_country=?";
            $set_aux=",";
            $param[]=$this->id_country==''?NULL:$this->id_country;
          }else{
            $this->objError[]="El campo ((id_country) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->code!= $rs["code"]){
          if ($this->updateable["code"]){
            $set.=$set_aux."code=?";
            $set_aux=",";
            $param[]=$this->code==''?NULL:$this->code;
          }else{
            $this->objError[]="El campo ((code) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->description!= $rs["description"]){
          if ($this->updateable["description"]){
            $set.=$set_aux."description=?";
            $set_aux=",";
            $param[]=$this->description==''?NULL:$this->description;
          }else{
            $this->objError[]="El campo ((description) no se puede modificar";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE gl_area_code ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="Este registro ha sido actualizado";
            $rs=$this->getAll();
            $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
          }else{
            $this->objMsg[]="Este registro no tiene datos para actualizar";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM gl_area_code
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
