<?php
  class HrUserHomeworkCriteriaBase extends TfEntity {
    protected $id;
    protected $id_user_homework;
    protected $id_evaluation_criteria;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="hr_user_homework_criteria";
  }

  private function getAll(){

    $q="SELECT id,
               id_user_homework,
               id_evaluation_criteria,
               created_by,
               created_date
          FROM hr_user_homework_criteria
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    $this->id_user_homework=$rs["id_user_homework"];
    $this->id_evaluation_criteria=$rs["id_evaluation_criteria"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->hr_user_homework_criteria_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_hr_user_homework_criteria){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_hr_user_homework_criteria; 
    }

    if ($tfRequest->exist("hr_user_homework_criteria_id_user_homework")){
      $this->id_user_homework=$tfRequest->hr_user_homework_criteria_id_user_homework;
    }
    if ($tfRequest->exist("hr_user_homework_criteria_id_evaluation_criteria")){
      $this->id_evaluation_criteria=$tfRequest->hr_user_homework_criteria_id_evaluation_criteria;
    }
    if ($tfRequest->exist("hr_user_homework_criteria_created_by")){
      $this->created_by=$tfRequest->hr_user_homework_criteria_created_by;
    }
    if ($tfRequest->exist("hr_user_homework_criteria_created_date")){
      $this->created_date=$tfRequest->hr_user_homework_criteria_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_user_homework"]=array("type"=>"number",
                                  "value"=>$this->id_user_homework,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_evaluation_criteria"]=array("type"=>"number",
                                  "value"=>$this->id_evaluation_criteria,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdUserHomework($value){
  $this->id_user_homework=$value;
  }
  public function getIdUserHomework(){
  return $this->id_user_homework;
  }
  public function setIdEvaluationCriteria($value){
  $this->id_evaluation_criteria=$value;
  }
  public function getIdEvaluationCriteria(){
  return $this->id_evaluation_criteria;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO hr_user_homework_criteria(id,
                               id_user_homework,
                               id_evaluation_criteria,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_user_homework==''?NULL:$this->id_user_homework,
                     $this->id_evaluation_criteria==''?NULL:$this->id_evaluation_criteria,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="Tu registro ha sido creado";
      $rs=$this->getAll();
      $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="El campo ((id) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_user_homework!= $rs["id_user_homework"]){
          if ($this->updateable["id_user_homework"]){
            $set.=$set_aux."id_user_homework=?";
            $set_aux=",";
            $param[]=$this->id_user_homework==''?NULL:$this->id_user_homework;
          }else{
            $this->objError[]="El campo ((id_user_homework) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_evaluation_criteria!= $rs["id_evaluation_criteria"]){
          if ($this->updateable["id_evaluation_criteria"]){
            $set.=$set_aux."id_evaluation_criteria=?";
            $set_aux=",";
            $param[]=$this->id_evaluation_criteria==''?NULL:$this->id_evaluation_criteria;
          }else{
            $this->objError[]="El campo ((id_evaluation_criteria) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="El campo ((created_by) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="El campo ((created_date) no se puede modificar";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE hr_user_homework_criteria ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="Este registro ha sido actualizado";
            $rs=$this->getAll();
            $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
          }else{
            $this->objMsg[]="Este registro no tiene datos para actualizar";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM hr_user_homework_criteria
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
