<?php
  class HrUserHomeworkBase extends TfEntity {
    protected $id;
    protected $id_user;
    protected $date;
    protected $active;
    protected $id_evaluation_type;
    protected $evaluator_type;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="hr_user_homework";
  }

  private function getAll(){

    $q="SELECT id,
               id_user,
               date,
               active,
               id_evaluation_type,
               evaluator_type,
               created_by,
               created_date
          FROM hr_user_homework
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    $this->id_user=$rs["id_user"];
    $this->date=$rs["date"];
    $this->active=$rs["active"];
    $this->id_evaluation_type=$rs["id_evaluation_type"];
    $this->evaluator_type=$rs["evaluator_type"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->hr_user_homework_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_hr_user_homework){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_hr_user_homework; 
    }

    if ($tfRequest->exist("hr_user_homework_id_user")){
      $this->id_user=$tfRequest->hr_user_homework_id_user;
    }
    if ($tfRequest->exist("hr_user_homework_date")){
      $this->date=$tfRequest->hr_user_homework_date;
    }
    if ($tfRequest->exist("hr_user_homework_active")){
      $this->active=$tfRequest->hr_user_homework_active;
    }
    if ($tfRequest->exist("hr_user_homework_id_evaluation_type")){
      $this->id_evaluation_type=$tfRequest->hr_user_homework_id_evaluation_type;
    }
    if ($tfRequest->exist("hr_user_homework_evaluator_type")){
      $this->evaluator_type=$tfRequest->hr_user_homework_evaluator_type;
    }
    if ($tfRequest->exist("hr_user_homework_created_by")){
      $this->created_by=$tfRequest->hr_user_homework_created_by;
    }
    if ($tfRequest->exist("hr_user_homework_created_date")){
      $this->created_date=$tfRequest->hr_user_homework_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_user"]=array("type"=>"number",
                                  "value"=>$this->id_user,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["date"]=array("type"=>"date",
                                  "value"=>$this->date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["id_evaluation_type"]=array("type"=>"number",
                                  "value"=>$this->id_evaluation_type,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["evaluator_type"]=array("type"=>"string",
                                  "value"=>$this->evaluator_type,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdUser($value){
  $this->id_user=$value;
  }
  public function getIdUser(){
  return $this->id_user;
  }
  public function setDate($value){
  $this->date=$value;
  }
  public function getDate(){
  return $this->date;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setIdEvaluationType($value){
  $this->id_evaluation_type=$value;
  }
  public function getIdEvaluationType(){
  return $this->id_evaluation_type;
  }
  public function setEvaluatorType($value){
  $this->evaluator_type=$value;
  }
  public function getEvaluatorType(){
  return $this->evaluator_type;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO hr_user_homework(id,
                               id_user,
                               date,
                               active,
                               id_evaluation_type,
                               evaluator_type,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_user==''?NULL:$this->id_user,
                     $this->date==''?NULL:$this->date,
                     $this->active==''?NULL:$this->active,
                     $this->id_evaluation_type==''?NULL:$this->id_evaluation_type,
                     $this->evaluator_type==''?NULL:$this->evaluator_type,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="Tu registro ha sido creado";
      $rs=$this->getAll();
      $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="El campo ((id) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_user!= $rs["id_user"]){
          if ($this->updateable["id_user"]){
            $set.=$set_aux."id_user=?";
            $set_aux=",";
            $param[]=$this->id_user==''?NULL:$this->id_user;
          }else{
            $this->objError[]="El campo ((id_user) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->date!= $rs["date"]){
          if ($this->updateable["date"]){
            $set.=$set_aux."date=?";
            $set_aux=",";
            $param[]=$this->date==''?NULL:$this->date;
          }else{
            $this->objError[]="El campo ((date) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="El campo ((active) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_evaluation_type!= $rs["id_evaluation_type"]){
          if ($this->updateable["id_evaluation_type"]){
            $set.=$set_aux."id_evaluation_type=?";
            $set_aux=",";
            $param[]=$this->id_evaluation_type==''?NULL:$this->id_evaluation_type;
          }else{
            $this->objError[]="El campo ((id_evaluation_type) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->evaluator_type!= $rs["evaluator_type"]){
          if ($this->updateable["evaluator_type"]){
            $set.=$set_aux."evaluator_type=?";
            $set_aux=",";
            $param[]=$this->evaluator_type==''?NULL:$this->evaluator_type;
          }else{
            $this->objError[]="El campo ((evaluator_type) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="El campo ((created_by) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="El campo ((created_date) no se puede modificar";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE hr_user_homework ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="Este registro ha sido actualizado";
            $rs=$this->getAll();
            $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
          }else{
            $this->objMsg[]="Este registro no tiene datos para actualizar";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM hr_user_homework
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
