<?php
  class OpInspectionReportBase extends TfEntity {
    protected $id;
    protected $inspection_report_no;
    protected $id_file;
    protected $name;
    protected $version;
    protected $id_customer;
    protected $vessel;
    protected $id_port_pier;
    protected $id_product_1;
    protected $id_product_2;
    protected $product_1_rob;
    protected $product_1_other_rob;
    protected $vessel_rob;
    protected $vessel_other_rob;
    protected $rob_date;
    protected $BL;
    protected $quantity;
    protected $inspection_date;
    protected $issued_date;
    protected $completion_date;
    protected $process;
    protected $last_processing;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="op_inspection_report";
  }

  private function getAll(){

    $q="SELECT id,
               inspection_report_no,
               id_file,
               name,
               version,
               id_customer,
               vessel,
               id_port_pier,
               id_product_1,
               id_product_2,
               product_1_rob,
               product_1_other_rob,
               vessel_rob,
               vessel_other_rob,
               rob_date,
               BL,
               quantity,
               inspection_date,
               issued_date,
               completion_date,
               process,
               last_processing,
               active,
               created_by,
               created_date
          FROM op_inspection_report
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    $this->inspection_report_no=$rs["inspection_report_no"];
    $this->id_file=$rs["id_file"];
    $this->name=$rs["name"];
    $this->version=$rs["version"];
    $this->id_customer=$rs["id_customer"];
    $this->vessel=$rs["vessel"];
    $this->id_port_pier=$rs["id_port_pier"];
    $this->id_product_1=$rs["id_product_1"];
    $this->id_product_2=$rs["id_product_2"];
    $this->product_1_rob=$rs["product_1_rob"];
    $this->product_1_other_rob=$rs["product_1_other_rob"];
    $this->vessel_rob=$rs["vessel_rob"];
    $this->vessel_other_rob=$rs["vessel_other_rob"];
    $this->rob_date=$rs["rob_date"];
    $this->BL=$rs["BL"];
    $this->quantity=$rs["quantity"];
    $this->inspection_date=$rs["inspection_date"];
    $this->issued_date=$rs["issued_date"];
    $this->completion_date=$rs["completion_date"];
    $this->process=$rs["process"];
    $this->last_processing=$rs["last_processing"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->op_inspection_report_id);
    
    if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_op_inspection_report){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_op_inspection_report; 
    }

    if ($tfRequest->exist("op_inspection_report_inspection_report_no")){
      $this->inspection_report_no=$tfRequest->op_inspection_report_inspection_report_no;
    }
    if ($tfRequest->exist("op_inspection_report_id_file")){
      $this->id_file=$tfRequest->op_inspection_report_id_file;
    }
    if ($tfRequest->exist("op_inspection_report_name")){
      $this->name=$tfRequest->op_inspection_report_name;
    }
    if ($tfRequest->exist("op_inspection_report_version")){
      $this->version=$tfRequest->op_inspection_report_version;
    }
    if ($tfRequest->exist("op_inspection_report_id_customer")){
      $this->id_customer=$tfRequest->op_inspection_report_id_customer;
    }
    if ($tfRequest->exist("op_inspection_report_vessel")){
      $this->vessel=$tfRequest->op_inspection_report_vessel;
    }
    if ($tfRequest->exist("op_inspection_report_id_port_pier")){
      $this->id_port_pier=$tfRequest->op_inspection_report_id_port_pier;
    }
    if ($tfRequest->exist("op_inspection_report_id_product_1")){
      $this->id_product_1=$tfRequest->op_inspection_report_id_product_1;
    }
    if ($tfRequest->exist("op_inspection_report_id_product_2")){
      $this->id_product_2=$tfRequest->op_inspection_report_id_product_2;
    }
    if ($tfRequest->exist("op_inspection_report_product_1_rob")){
      $this->product_1_rob=$tfRequest->op_inspection_report_product_1_rob;
    }
    if ($tfRequest->exist("op_inspection_report_product_1_other_rob")){
      $this->product_1_other_rob=$tfRequest->op_inspection_report_product_1_other_rob;
    }
    if ($tfRequest->exist("op_inspection_report_vessel_rob")){
      $this->vessel_rob=$tfRequest->op_inspection_report_vessel_rob;
    }
    if ($tfRequest->exist("op_inspection_report_vessel_other_rob")){
      $this->vessel_other_rob=$tfRequest->op_inspection_report_vessel_other_rob;
    }
    if ($tfRequest->exist("op_inspection_report_rob_date")){
      $this->rob_date=$tfRequest->op_inspection_report_rob_date;
    }
    if ($tfRequest->exist("op_inspection_report_BL")){
      $this->BL=$tfRequest->op_inspection_report_BL;
    }
    if ($tfRequest->exist("op_inspection_report_quantity")){
      $this->quantity=$tfRequest->op_inspection_report_quantity;
    }
    if ($tfRequest->exist("op_inspection_report_inspection_date")){
      $this->inspection_date=$tfRequest->op_inspection_report_inspection_date;
    }
    if ($tfRequest->exist("op_inspection_report_issued_date")){
      $this->issued_date=$tfRequest->op_inspection_report_issued_date;
    }
    if ($tfRequest->exist("op_inspection_report_completion_date")){
      $this->completion_date=$tfRequest->op_inspection_report_completion_date;
    }
    if ($tfRequest->exist("op_inspection_report_process")){
      $this->process=$tfRequest->op_inspection_report_process;
    }
    if ($tfRequest->exist("op_inspection_report_last_processing")){
      $this->last_processing=$tfRequest->op_inspection_report_last_processing;
    }
    if ($tfRequest->exist("op_inspection_report_active")){
      $this->active=$tfRequest->op_inspection_report_active;
    }
    if ($tfRequest->exist("op_inspection_report_created_by")){
      $this->created_by=$tfRequest->op_inspection_report_created_by;
    }
    if ($tfRequest->exist("op_inspection_report_created_date")){
      $this->created_date=$tfRequest->op_inspection_report_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["inspection_report_no"]=array("type"=>"string",
                                  "value"=>$this->inspection_report_no,
                                  "length"=>11,
                                  "required"=>false);
    $this->validation["id_file"]=array("type"=>"string",
                                  "value"=>$this->id_file,
                                  "length"=>200,
                                  "required"=>false);
    $this->validation["name"]=array("type"=>"string",
                                  "value"=>$this->name,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["version"]=array("type"=>"number",
                                  "value"=>$this->version,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_customer"]=array("type"=>"number",
                                  "value"=>$this->id_customer,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["vessel"]=array("type"=>"string",
                                  "value"=>$this->vessel,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["id_port_pier"]=array("type"=>"number",
                                  "value"=>$this->id_port_pier,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_product_1"]=array("type"=>"number",
                                  "value"=>$this->id_product_1,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_product_2"]=array("type"=>"number",
                                  "value"=>$this->id_product_2,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["product_1_rob"]=array("type"=>"number",
                                  "value"=>$this->product_1_rob,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["product_1_other_rob"]=array("type"=>"number",
                                  "value"=>$this->product_1_other_rob,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["vessel_rob"]=array("type"=>"number",
                                  "value"=>$this->vessel_rob,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["vessel_other_rob"]=array("type"=>"number",
                                  "value"=>$this->vessel_other_rob,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["rob_date"]=array("type"=>"string",
                                  "value"=>$this->rob_date,
                                  "length"=>20,
                                  "required"=>false);
    $this->validation["BL"]=array("type"=>"string",
                                  "value"=>$this->BL,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["quantity"]=array("type"=>"string",
                                  "value"=>$this->quantity,
                                  "length"=>20,
                                  "required"=>true);
    $this->validation["inspection_date"]=array("type"=>"date",
                                  "value"=>$this->inspection_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["issued_date"]=array("type"=>"date",
                                  "value"=>$this->issued_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["completion_date"]=array("type"=>"date",
                                  "value"=>$this->completion_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["process"]=array("type"=>"string",
                                  "value"=>$this->process,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["last_processing"]=array("type"=>"datetime",
                                  "value"=>$this->last_processing,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setInspectionReportNo($value){
  $this->inspection_report_no=$value;
  }
  public function getInspectionReportNo(){
  return $this->inspection_report_no;
  }
  public function setIdFile($value){
  $this->id_file=$value;
  }
  public function getIdFile(){
  return $this->id_file;
  }
  public function setName($value){
  $this->name=$value;
  }
  public function getName(){
  return $this->name;
  }
  public function setVersion($value){
  $this->version=$value;
  }
  public function getVersion(){
  return $this->version;
  }
  public function setIdCustomer($value){
  $this->id_customer=$value;
  }
  public function getIdCustomer(){
  return $this->id_customer;
  }
  public function setVessel($value){
  $this->vessel=$value;
  }
  public function getVessel(){
  return $this->vessel;
  }
  public function setIdPortPier($value){
  $this->id_port_pier=$value;
  }
  public function getIdPortPier(){
  return $this->id_port_pier;
  }
  public function setIdProduct1($value){
  $this->id_product_1=$value;
  }
  public function getIdProduct1(){
  return $this->id_product_1;
  }
  public function setIdProduct2($value){
  $this->id_product_2=$value;
  }
  public function getIdProduct2(){
  return $this->id_product_2;
  }
  public function setProduct1Rob($value){
  $this->product_1_rob=$value;
  }
  public function getProduct1Rob(){
  return $this->product_1_rob;
  }
  public function setProduct1OtherRob($value){
  $this->product_1_other_rob=$value;
  }
  public function getProduct1OtherRob(){
  return $this->product_1_other_rob;
  }
  public function setVesselRob($value){
  $this->vessel_rob=$value;
  }
  public function getVesselRob(){
  return $this->vessel_rob;
  }
  public function setVesselOtherRob($value){
  $this->vessel_other_rob=$value;
  }
  public function getVesselOtherRob(){
  return $this->vessel_other_rob;
  }
  public function setRobDate($value){
  $this->rob_date=$value;
  }
  public function getRobDate(){
  return $this->rob_date;
  }
  public function setBL($value){
  $this->BL=$value;
  }
  public function getBL(){
  return $this->BL;
  }
  public function setQuantity($value){
  $this->quantity=$value;
  }
  public function getQuantity(){
  return $this->quantity;
  }
  public function setInspectionDate($value){
  $this->inspection_date=$value;
  }
  public function getInspectionDate(){
  return $this->inspection_date;
  }
  public function setIssuedDate($value){
  $this->issued_date=$value;
  }
  public function getIssuedDate(){
  return $this->issued_date;
  }
  public function setCompletionDate($value){
  $this->completion_date=$value;
  }
  public function getCompletionDate(){
  return $this->completion_date;
  }
  public function setProcess($value){
  $this->process=$value;
  }
  public function getProcess(){
  return $this->process;
  }
  public function setLastProcessing($value){
  $this->last_processing=$value;
  }
  public function getLastProcessing(){
  return $this->last_processing;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO op_inspection_report(id,
                               inspection_report_no,
                               id_file,
                               name,
                               version,
                               id_customer,
                               vessel,
                               id_port_pier,
                               id_product_1,
                               id_product_2,
                               product_1_rob,
                               product_1_other_rob,
                               vessel_rob,
                               vessel_other_rob,
                               rob_date,
                               BL,
                               quantity,
                               inspection_date,
                               issued_date,
                               completion_date,
                               process,
                               last_processing,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->inspection_report_no==''?NULL:$this->inspection_report_no,
                     $this->id_file==''?NULL:$this->id_file,
                     $this->name==''?NULL:$this->name,
                     $this->version==''?NULL:$this->version,
                     $this->id_customer==''?NULL:$this->id_customer,
                     $this->vessel==''?NULL:$this->vessel,
                     $this->id_port_pier==''?NULL:$this->id_port_pier,
                     $this->id_product_1==''?NULL:$this->id_product_1,
                     $this->id_product_2==''?NULL:$this->id_product_2,
                     $this->product_1_rob==''?NULL:$this->product_1_rob,
                     $this->product_1_other_rob==''?NULL:$this->product_1_other_rob,
                     $this->vessel_rob==''?NULL:$this->vessel_rob,
                     $this->vessel_other_rob==''?NULL:$this->vessel_other_rob,
                     $this->rob_date==''?NULL:$this->rob_date,
                     $this->BL==''?NULL:$this->BL,
                     $this->quantity==''?NULL:$this->quantity,
                     $this->inspection_date==''?NULL:$this->inspection_date,
                     $this->issued_date==''?NULL:$this->issued_date,
                     $this->completion_date==''?NULL:$this->completion_date,
                     $this->process==''?NULL:$this->process,
                     $this->last_processing==''?NULL:$this->last_processing,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="Tu registro ha sido creado";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="El campo ((id) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->inspection_report_no!= $rs["inspection_report_no"]){
          if ($this->updateable["inspection_report_no"]){
            $set.=$set_aux."inspection_report_no=?";
            $set_aux=",";
            $param[]=$this->inspection_report_no==''?NULL:$this->inspection_report_no;
          }else{
            $this->objError[]="El campo ((inspection_report_no) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_file!= $rs["id_file"]){
          if ($this->updateable["id_file"]){
            $set.=$set_aux."id_file=?";
            $set_aux=",";
            $param[]=$this->id_file==''?NULL:$this->id_file;
          }else{
            $this->objError[]="El campo ((id_file) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->name!= $rs["name"]){
          if ($this->updateable["name"]){
            $set.=$set_aux."name=?";
            $set_aux=",";
            $param[]=$this->name==''?NULL:$this->name;
          }else{
            $this->objError[]="El campo ((name) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->version!= $rs["version"]){
          if ($this->updateable["version"]){
            $set.=$set_aux."version=?";
            $set_aux=",";
            $param[]=$this->version==''?NULL:$this->version;
          }else{
            $this->objError[]="El campo ((version) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_customer!= $rs["id_customer"]){
          if ($this->updateable["id_customer"]){
            $set.=$set_aux."id_customer=?";
            $set_aux=",";
            $param[]=$this->id_customer==''?NULL:$this->id_customer;
          }else{
            $this->objError[]="El campo ((id_customer) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->vessel!= $rs["vessel"]){
          if ($this->updateable["vessel"]){
            $set.=$set_aux."vessel=?";
            $set_aux=",";
            $param[]=$this->vessel==''?NULL:$this->vessel;
          }else{
            $this->objError[]="El campo ((vessel) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_port_pier!= $rs["id_port_pier"]){
          if ($this->updateable["id_port_pier"]){
            $set.=$set_aux."id_port_pier=?";
            $set_aux=",";
            $param[]=$this->id_port_pier==''?NULL:$this->id_port_pier;
          }else{
            $this->objError[]="El campo ((id_port_pier) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_product_1!= $rs["id_product_1"]){
          if ($this->updateable["id_product_1"]){
            $set.=$set_aux."id_product_1=?";
            $set_aux=",";
            $param[]=$this->id_product_1==''?NULL:$this->id_product_1;
          }else{
            $this->objError[]="El campo ((id_product_1) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_product_2!= $rs["id_product_2"]){
          if ($this->updateable["id_product_2"]){
            $set.=$set_aux."id_product_2=?";
            $set_aux=",";
            $param[]=$this->id_product_2==''?NULL:$this->id_product_2;
          }else{
            $this->objError[]="El campo ((id_product_2) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->product_1_rob!= $rs["product_1_rob"]){
          if ($this->updateable["product_1_rob"]){
            $set.=$set_aux."product_1_rob=?";
            $set_aux=",";
            $param[]=$this->product_1_rob==''?NULL:$this->product_1_rob;
          }else{
            $this->objError[]="El campo ((product_1_rob) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->product_1_other_rob!= $rs["product_1_other_rob"]){
          if ($this->updateable["product_1_other_rob"]){
            $set.=$set_aux."product_1_other_rob=?";
            $set_aux=",";
            $param[]=$this->product_1_other_rob==''?NULL:$this->product_1_other_rob;
          }else{
            $this->objError[]="El campo ((product_1_other_rob) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->vessel_rob!= $rs["vessel_rob"]){
          if ($this->updateable["vessel_rob"]){
            $set.=$set_aux."vessel_rob=?";
            $set_aux=",";
            $param[]=$this->vessel_rob==''?NULL:$this->vessel_rob;
          }else{
            $this->objError[]="El campo ((vessel_rob) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->vessel_other_rob!= $rs["vessel_other_rob"]){
          if ($this->updateable["vessel_other_rob"]){
            $set.=$set_aux."vessel_other_rob=?";
            $set_aux=",";
            $param[]=$this->vessel_other_rob==''?NULL:$this->vessel_other_rob;
          }else{
            $this->objError[]="El campo ((vessel_other_rob) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->rob_date!= $rs["rob_date"]){
          if ($this->updateable["rob_date"]){
            $set.=$set_aux."rob_date=?";
            $set_aux=",";
            $param[]=$this->rob_date==''?NULL:$this->rob_date;
          }else{
            $this->objError[]="El campo ((rob_date) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->BL!= $rs["BL"]){
          if ($this->updateable["BL"]){
            $set.=$set_aux."BL=?";
            $set_aux=",";
            $param[]=$this->BL==''?NULL:$this->BL;
          }else{
            $this->objError[]="El campo ((BL) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->quantity!= $rs["quantity"]){
          if ($this->updateable["quantity"]){
            $set.=$set_aux."quantity=?";
            $set_aux=",";
            $param[]=$this->quantity==''?NULL:$this->quantity;
          }else{
            $this->objError[]="El campo ((quantity) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->inspection_date!= $rs["inspection_date"]){
          if ($this->updateable["inspection_date"]){
            $set.=$set_aux."inspection_date=?";
            $set_aux=",";
            $param[]=$this->inspection_date==''?NULL:$this->inspection_date;
          }else{
            $this->objError[]="El campo ((inspection_date) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->issued_date!= $rs["issued_date"]){
          if ($this->updateable["issued_date"]){
            $set.=$set_aux."issued_date=?";
            $set_aux=",";
            $param[]=$this->issued_date==''?NULL:$this->issued_date;
          }else{
            $this->objError[]="El campo ((issued_date) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->completion_date!= $rs["completion_date"]){
          if ($this->updateable["completion_date"]){
            $set.=$set_aux."completion_date=?";
            $set_aux=",";
            $param[]=$this->completion_date==''?NULL:$this->completion_date;
          }else{
            $this->objError[]="El campo ((completion_date) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->process!= $rs["process"]){
          if ($this->updateable["process"]){
            $set.=$set_aux."process=?";
            $set_aux=",";
            $param[]=$this->process==''?NULL:$this->process;
          }else{
            $this->objError[]="El campo ((process) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->last_processing!= $rs["last_processing"]){
          if ($this->updateable["last_processing"]){
            $set.=$set_aux."last_processing=?";
            $set_aux=",";
            $param[]=$this->last_processing==''?NULL:$this->last_processing;
          }else{
            $this->objError[]="El campo ((last_processing) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="El campo ((active) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="El campo ((created_by) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="El campo ((created_date) no se puede modificar";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE op_inspection_report ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="Este registro ha sido actualizado";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="Este registro no tiene datos para actualizar";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM op_inspection_report
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
