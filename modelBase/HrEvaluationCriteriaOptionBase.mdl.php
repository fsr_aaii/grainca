<?php
  class HrEvaluationCriteriaOptionBase extends TfEntity {
    protected $id;
    protected $id_evaluation_criteria;
    protected $name;
    protected $correct;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="hr_evaluation_criteria_option";
  }

  private function getAll(){

    $q="SELECT id,
               id_evaluation_criteria,
               name,
               correct,
               active,
               created_by,
               created_date
          FROM hr_evaluation_criteria_option
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    $this->id_evaluation_criteria=$rs["id_evaluation_criteria"];
    $this->name=$rs["name"];
    $this->correct=$rs["correct"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->hr_evaluation_criteria_option_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_hr_evaluation_criteria_option){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_hr_evaluation_criteria_option; 
    }

    if ($tfRequest->exist("hr_evaluation_criteria_option_id_evaluation_criteria")){
      $this->id_evaluation_criteria=$tfRequest->hr_evaluation_criteria_option_id_evaluation_criteria;
    }
    if ($tfRequest->exist("hr_evaluation_criteria_option_name")){
      $this->name=$tfRequest->hr_evaluation_criteria_option_name;
    }
    if ($tfRequest->exist("hr_evaluation_criteria_option_correct")){
      $this->correct=$tfRequest->hr_evaluation_criteria_option_correct;
    }
    if ($tfRequest->exist("hr_evaluation_criteria_option_active")){
      $this->active=$tfRequest->hr_evaluation_criteria_option_active;
    }
    if ($tfRequest->exist("hr_evaluation_criteria_option_created_by")){
      $this->created_by=$tfRequest->hr_evaluation_criteria_option_created_by;
    }
    if ($tfRequest->exist("hr_evaluation_criteria_option_created_date")){
      $this->created_date=$tfRequest->hr_evaluation_criteria_option_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_evaluation_criteria"]=array("type"=>"number",
                                  "value"=>$this->id_evaluation_criteria,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["name"]=array("type"=>"string",
                                  "value"=>$this->name,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["correct"]=array("type"=>"string",
                                  "value"=>$this->correct,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdEvaluationCriteria($value){
  $this->id_evaluation_criteria=$value;
  }
  public function getIdEvaluationCriteria(){
  return $this->id_evaluation_criteria;
  }
  public function setName($value){
  $this->name=$value;
  }
  public function getName(){
  return $this->name;
  }
  public function setCorrect($value){
  $this->correct=$value;
  }
  public function getCorrect(){
  return $this->correct;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO hr_evaluation_criteria_option(id,
                               id_evaluation_criteria,
                               name,
                               correct,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_evaluation_criteria==''?NULL:$this->id_evaluation_criteria,
                     $this->name==''?NULL:$this->name,
                     $this->correct==''?NULL:$this->correct,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="Tu registro ha sido creado";
      $rs=$this->getAll();
      $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="El campo ((id) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_evaluation_criteria!= $rs["id_evaluation_criteria"]){
          if ($this->updateable["id_evaluation_criteria"]){
            $set.=$set_aux."id_evaluation_criteria=?";
            $set_aux=",";
            $param[]=$this->id_evaluation_criteria==''?NULL:$this->id_evaluation_criteria;
          }else{
            $this->objError[]="El campo ((id_evaluation_criteria) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->name!= $rs["name"]){
          if ($this->updateable["name"]){
            $set.=$set_aux."name=?";
            $set_aux=",";
            $param[]=$this->name==''?NULL:$this->name;
          }else{
            $this->objError[]="El campo ((name) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->correct!= $rs["correct"]){
          if ($this->updateable["correct"]){
            $set.=$set_aux."correct=?";
            $set_aux=",";
            $param[]=$this->correct==''?NULL:$this->correct;
          }else{
            $this->objError[]="El campo ((correct) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="El campo ((active) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="El campo ((created_by) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="El campo ((created_date) no se puede modificar";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE hr_evaluation_criteria_option ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="Este registro ha sido actualizado";
            $rs=$this->getAll();
            $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
          }else{
            $this->objMsg[]="Este registro no tiene datos para actualizar";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM hr_evaluation_criteria_option
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
