<?php
  class OpNotificationBase extends TfEntity {
    protected $id;
    protected $id_inspection_report;
    protected $subject;
    protected $body;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="op_notification";
  }

  private function getAll(){

    $q="SELECT id,
               id_inspection_report,
               subject,
               body,
               created_by,
               created_date
          FROM op_notification
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    $this->id_inspection_report=$rs["id_inspection_report"];
    $this->subject=$rs["subject"];
    $this->body=$rs["body"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->op_notification_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_op_notification){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_op_notification; 
    }

    if ($tfRequest->exist("op_notification_id_inspection_report")){
      $this->id_inspection_report=$tfRequest->op_notification_id_inspection_report;
    }
    if ($tfRequest->exist("op_notification_subject")){
      $this->subject=$tfRequest->op_notification_subject;
    }
    if ($tfRequest->exist("op_notification_body")){
      $this->body=$tfRequest->op_notification_body;
    }
    if ($tfRequest->exist("op_notification_created_by")){
      $this->created_by=$tfRequest->op_notification_created_by;
    }
    if ($tfRequest->exist("op_notification_created_date")){
      $this->created_date=$tfRequest->op_notification_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_inspection_report"]=array("type"=>"number",
                                  "value"=>$this->id_inspection_report,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["subject"]=array("type"=>"string",
                                  "value"=>$this->subject,
                                  "length"=>450,
                                  "required"=>true);
    $this->validation["body"]=array("type"=>"string",
                                  "value"=>$this->body,
                                  "length"=>5000,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdInspectionReport($value){
  $this->id_inspection_report=$value;
  }
  public function getIdInspectionReport(){
  return $this->id_inspection_report;
  }
  public function setSubject($value){
  $this->subject=$value;
  }
  public function getSubject(){
  return $this->subject;
  }
  public function setBody($value){
  $this->body=$value;
  }
  public function getBody(){
  return $this->body;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO op_notification(id,
                               id_inspection_report,
                               subject,
                               body,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_inspection_report==''?NULL:$this->id_inspection_report,
                     $this->subject==''?NULL:$this->subject,
                     $this->body==''?NULL:$this->body,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="Tu registro ha sido creado";
      $rs=$this->getAll();
      $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="Este registro está bloqueado por otro usuario, inténtalo más tarde";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="El campo ((id) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->id_inspection_report!= $rs["id_inspection_report"]){
          if ($this->updateable["id_inspection_report"]){
            $set.=$set_aux."id_inspection_report=?";
            $set_aux=",";
            $param[]=$this->id_inspection_report==''?NULL:$this->id_inspection_report;
          }else{
            $this->objError[]="El campo ((id_inspection_report) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->subject!= $rs["subject"]){
          if ($this->updateable["subject"]){
            $set.=$set_aux."subject=?";
            $set_aux=",";
            $param[]=$this->subject==''?NULL:$this->subject;
          }else{
            $this->objError[]="El campo ((subject) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->body!= $rs["body"]){
          if ($this->updateable["body"]){
            $set.=$set_aux."body=?";
            $set_aux=",";
            $param[]=$this->body==''?NULL:$this->body;
          }else{
            $this->objError[]="El campo ((body) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="El campo ((created_by) no se puede modificar";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="El campo ((created_date) no se puede modificar";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE op_notification ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="Este registro ha sido actualizado";
            $rs=$this->getAll();
            $this->initialState=count($rs)>0 ? hash(HASH_KEY,json_encode($rs)) : '';
          }else{
            $this->objMsg[]="Este registro no tiene datos para actualizar";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM op_notification
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
