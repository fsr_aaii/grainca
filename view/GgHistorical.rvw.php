<style type="text/css">
 .btn-tf {
    color: #fff !important;
    background-color: #3c3d38;
    border-color: #3c3d38;
 }
 .btn-tf:hover,.btn-tf:focus {
    color: #fff !important;
    background-color: #36b3b9;
    border-color: #36b3b9;
 }
</style>
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }
  

  $html='<div class="row">
            <div class="mx-auto col-lg-11 tf-card shadow mb-4">
               <div class="col-12 mb-5 title">Historial</div>  
               <div class="row col-lg-12 mb-4 p-0">
                 
                 <div class="col-lg-3 order-lg-12 p-0 filter">
                      <form id="op_inspection_report_form" name="op_inspection_report_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
					      <fieldset>
					      <div class="col-lg-12 container">
					       <label for="op_inspection_report_vessel" class="control-label">Buque:</label>
					        <input type="text" id="op_inspection_report_vessel" name="op_inspection_report_vessel" class="op_inspection_report_vessel form-control"  maxlength="200"  value="'.$tfRequest->op_inspection_report_vessel.'"  tabindex="2"/>
					      <label for="op_inspection_report_vessel" class="error"></label>
					      </div>
					      <div class="col-lg-12 container">
					       <label for="op_inspection_report_id_product_1" class="control-label">Producto:</label>
					        <select  id="op_inspection_report_id_product_1" name="op_inspection_report_id_product_1" class="op_inspection_report_id_product_1 form-control" tabindex="4">
					      <option value="">Seleccionar</option>'.
					      TfWidget::selectStructure(OpProduct::selectOptions($tfs),$tfRequest->op_inspection_report_id_product_1).
					'      </select>
					      <label for="op_inspection_report_id_product_1" class="error"></label>
					      </div>
					      <div class="col-lg-12 container">
					       <label for="op_inspection_report_id_port_pier" class="control-label">Puerto - Muelle:</label>
					        <select  id="op_inspection_report_id_port_pier" name="op_inspection_report_id_port_pier" class="op_inspection_report_id_port_pier form-control" tabindex="3">
					      <option value="">Seleccionar</option>'.
					      TfWidget::selectStructure(OpPortPier::selectOptions($tfs),$tfRequest->op_inspection_report_id_port_pier).
					'      </select>
					      <label for="op_inspection_report_id_port_pier" class="error"></label>
					      </div>
					      <div class="col-lg-12 container">
					       <label for="op_inspection_report_inspection_date_1" class="control-label">Fecha Desde :</label>
					        <input type="text" id="op_inspection_report_inspection_date_1" name="op_inspection_report_inspection_date_1" class="op_inspection_report_inspection_date_1 form-control"  maxlength="22"  value="'.$tfRequest->op_inspection_report_inspection_date_1.'"  tabindex="6"/>
					      <label for="op_inspection_report_inspection_date_1" class="error"></label>
					      </div>
					      <div class="col-lg-12 container">
					       <label for="op_inspection_report_inspection_date_2" class="control-label">Fecha Hasta :</label>
					        <input type="text" id="op_inspection_report_inspection_date_2" name="op_inspection_report_inspection_date_2" class="op_inspection_report_inspection_date_2 form-control"  maxlength="22"  value="'.$tfRequest->op_inspection_report_inspection_date_2.'"  tabindex="6"/>
					      <label for="op_inspection_report_inspection_date_2" class="error"></label>
					      </div>
					     
					     <div class="col-lg-12 container mb-5 mt-2 text-right">
                           <a class="col-lg-12 btn btn-tf" data-tf-form="#op_inspection_report_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgHistorical" data-tf-action="AC" onclick="TfRequest.do(this,false);">Buscar</a>
					     </div>

					   </fieldset>
					  </form>
                 </div>

                 <div class="col-lg-9 order-lg-1 p-0 list">

                      <div class="row col-12">';
                      $i=0;
                      foreach ($opInspectionReportList as $row){
                          $i++;
                          $tfData["op_inspection_report_id"] = $row["id"];
                          
                          $html.='  <div class="col-lg-6 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="AD" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                                  <div class="d-flex align-items-center bg-info p-4" >
                                      <div class="ml-2">
                                          <div class="epic text-uppercase h5 text-white m-0">'.$row["product_1"].'</div>
                                          <div class="epic h5 text-white m-0">'.$row["port_pier"].' - '.$row["vessel"].'</div>
                                          <div class="epic h5 text-white m-0">BL '.$row["BL"].'  '.TfWidget::qty($row["quantity"]).'Kgs</div>
                                          <div class="epic h4 text-white m-0">'.TfWidget::ymd2dmy($row["inspection_date"]).'</div>
                                      </div>
                                  </div>
                                </div>';  

                             if ($i%2==0){
                            $html.='</div>
                                <div class="row col-12">';
                          }    
                         }
                         $html.='</div>


                 </div>
               </div>
            </div>
          </div>';

  echo $html;     

  ?>
<script type="text/javascript">
  
  function opInspectionReportRules(){
  $("#op_inspection_report_form").validate();
  $("#op_inspection_report_id_customer").rules("add", {
    number:true,
    maxlength:22
  });
  $("#op_inspection_report_vessel").rules("add", {
    maxlength:200
  });
  $("#op_inspection_report_id_port_pier").rules("add", {
    number:true,
    maxlength:22
  });
  $("#op_inspection_report_id_product_1").rules("add", {
    maxlength:450
  });
  $("#op_inspection_report_inspection_date_1").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#op_inspection_report_inspection_date_2").rules("add", {
    isDate:true,
    maxlength:22
  });

}


$("#op_inspection_report_inspection_date_1").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
 $("#op_inspection_report_inspection_date_2").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  opInspectionReportRules();

  $("#op_inspection_report_inspection_date_1").css('vertical-align','top');
  $("#op_inspection_report_inspection_date_1").mask('y999-m9-d9');
  $("#op_inspection_report_inspection_date_2").css('vertical-align','top');
  $("#op_inspection_report_inspection_date_2").mask('y999-m9-d9');

})
</script>