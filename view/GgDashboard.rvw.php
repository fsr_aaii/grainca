
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }
  
  $kgAprox = $opInspectionReportDetail["KG_APROX_PESADO"]?:"0";
  $vehPesado = $opInspectionReportDetail["VEH_PESADO"]?:"0";
  

  if ($kgAprox=="0" OR $vehPesado=="0" ){
    $asignacion = "0";
  }else{
    $asignacion = round($opInspectionReportDetail["qty"]/($kgAprox/$vehPesado));
  }


  $html='<div class="row">
            <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-12 mb-5 title">Consultar de Inspecci&oacute;n</div>  
            <div class="row col-12 mb-2">
                <div class="col-lg-4 col-12 p-0">
                   <div class="col-12 p-0"><b>Cliente:</b></div> 
                   <div class="col-12 p-0">'.$opInspectionReport["customer"].'</div>  
               </div>  
                <div class="col-lg-4 col-12 p-0">
                 <div class="col-12 p-0"><b>Buque: </b></div>
                 <div class="col-12 p-0">'.$opInspectionReport["vessel"].'</div>  
                </div> 
                <div class="col-lg-4 col-12 p-0">
                  <div class="col-12 p-0"><b>Producto 1: </b> </div>
                  <div class="col-12 p-0">'.$opInspectionReport["product_1"].'</div>
                </div>  
            </div>
            <div class="row col-12 mb-2">
             <div class="col-lg-3 col-12 p-0">
                  <div class="col-12 p-0"><b>Muelle:</b> </div>
                  <div class="col-12 p-0">'.$opInspectionReport["port_pier"].'</div>
                </div> 
                <div class="col-lg-1 col-12 p-0">
                  <div class="col-12 p-0"><b>BL: </b></div> 
                  <div class="col-12 p-0">'.$opInspectionReport["BL"].'</div> 
                </div>
                <div class="col-lg-3 col-12 p-0">
                  <div class="col-12 p-0"><b>Buque (Kgs): </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReport["vessel_rob"]).'Kgs</div>
                </div>
                <div class="col-lg-3 col-12 p-0">
                  <div class="col-12 p-0"><b>Producto (Kgs): </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReport["product_1_rob"]).'Kgs</div>
                </div>    
                <div class="col-lg-2 col-12 p-0">
                  <div class="col-12 p-0"><b>Parcela (Kgs): </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReport["quantity"]).'Kgs</div>
                </div>  
                
            </div>  
            <div class="row col-12 mb-2">
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Estim.: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["cant_veh_estim"]).'</div> 
                 </div> 
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Prom:</b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["promedio_kgs"]).'Kgs</div>
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh.(SICA): </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["VEH_SICA"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Aprox.(SICA): </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["KG_APROX_SICA"]).'Kgs</div> 
                </div>   
            </div> 
            <div class="row col-12 mb-2">
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Pesado.: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["VEH_PESADO"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Prom Pesado:</b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["KG_APROX_PESADO"]).' Kgs</div>
                </div>';
        if ($opInspectionReport["completion_date"]!=''){
          $html.='  <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Remanente: </b> </div>
                  <div class="col-12 p-0">0</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">';
           if ($opInspectionReportDetail["remanente_kgs"]>=0){
             $html.='    <div class="col-12 p-0"><b>Faltantes: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]).' Kgs</div>  
                </div>  ';  
           } else{
             $html.='    <div class="col-12 p-0"><b>Excedentes: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]*(-1)).' Kgs</div>  
                </div>  ';
           }    
        }else{
          $html.='  <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Remanente: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_unidades"]).'</div> 
                </div> 
                <div class="col-lg-3 col-6 p-0"> ';
           if ($opInspectionReportDetail["remanente_kgs"]>=0){
             $html.='    <div class="col-12 p-0"><b>Remanente: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]).' Kgs</div>  
                </div>  ';  
           } else{
             $html.='    <div class="col-12 p-0"><b>Excedentes: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]*(-1)).' Kgs</div>  
                </div>  ';
           }    
        }         
        $html.='    </div>

            <div class="col-12">
              <div class="panel-group mt-5 accordion" id="accordion" role="tablist" aria-multiselectable="true">
                  

                  <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a id="gg-a-1" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        Resumen de Descarga
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" data-parent="#accordion" style="">
                                <div class="panel-body">
                                   <table class="table gTable">
                                      <thead>
                                        <tr>
                                          <th class="text-right" scope="col"></th>
                                          <th class="text-right" scope="col">Cant. Veh.</th>
                                          <th class="text-right" scope="col">Peso Kgs.</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <th scope="row">Asignacion</th>
                                          <td class="text-right">'.TfWidget::qty($asignacion).'</td>
                                          <td class="text-right">'.TfWidget::qty($opInspectionReport["quantity"]).' </td>
                                        </tr>
                                        <tr>
                                          <th scope="row">Peso Romana</th>
                                          <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["VEH_PESADO"]?:"0").'</td>
                                          <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["KG_APROX_PESADO"]?:"0").'</td>
                                        </tr>
                                        <tr>
                                          <th scope="row">Veh. Cargados Por Pesar</th>
                                          <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["CARGADO_POR_PESAR"]?:"0").'</td>
                                          <td class="text-right"></td>
                                        </tr>
                                        <tr>
                                          <th scope="row">Veh. A Costado de Buque</th>
                                          <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["A_COSTADO_BUQUE"]?:"0").'</td>
                                          <td class="text-right"></td>
                                        </tr>
                                        <tr>';
        if ($opInspectionReport["completion_date"]!=''){
           if (($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"])>=0){
             $html.='    <th scope="row">Faltante Asignacion</th>
                  <td class="text-right">'.TfWidget::qty("0").'</td>
                                          <td class="text-right">'.TfWidget::qty($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"]?:"0").'</td> ';  
           } else{
             $html.='    <th scope="row">Excedente Asignacion</th>
                  <td class="text-right">'.TfWidget::qty("0").'</td>
                                          <td class="text-right">'.TfWidget::qty(($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"])*(-1)?:"0").'</td>';
           }    
        }else{
         
           if (($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"])>=0){
             $html.=' <th scope="row">Remanente Asignacion</th>
                  <td class="text-right">'.TfWidget::qty($asignacion-$opInspectionReportDetail["VEH_PESADO"]?:"0").'</td>
                                          <td class="text-right">'.TfWidget::qty($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"]?:"0").'</td>';  
           } else{
             $html.='   <th scope="row">Excedente Asignacion</th>
                  <td class="text-right">'.TfWidget::qty($asignacion-$opInspectionReportDetail["VEH_PESADO"]?:"0").'</td>
                                          <td class="text-right">'.TfWidget::qty(($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"])*(-1)?:"0").'</td>';
           }    
        }         
        $html.='
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                              </div>
                          </div>


                  <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a id="gg-a-2" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" >
                                        Vehiculos Cargados
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" data-parent="#accordion" style="height: 0px;">
                                <div class="panel-body">
                                 <table class="table gTable">
                                    <thead>
                                      <tr>
                                        <th scope="col">Destino</th>
                                        <th class="text-right" scope="col">Cant. Veh.</th>
                                        <th class="text-right" scope="col">Peso kg.</th>
                                      </tr>
                                    </thead>
                                    <tbody>';

                                  $total1=0;$total6=0;
                                  foreach ($cargado as $t){
                                  $total1+=$t["qty"];
                                  $total6+=$t["kg"];
                                  $tfData["op_destination_id"] = $t["id"];
                                  $html.='<tr>
                                        <th scope="row"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="DVC" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.$t["destination"].'</a></th>
                                        <td class="text-right">'.TfWidget::qty($t["qty"]).'</td>
                                        <td class="text-right">'.TfWidget::qty($t["kg"]).'</td>
                                      </tr>';
                                  }


                                  $html.='<tfoot>
                                      <tr>
                                        <th scope="col">Total General</th>
                                        <th class="text-right" scope="col">'.$total1.'</th>
                                        <th class="text-right" scope="col">'.TfWidget::qty($total6).'</th>
                                      </tr>
                                    </tfoot>
                                    </tbody>
                                  </table>


                                  </div>
                                </div>
                            </div>

 
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a id="gg-a-3" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" >
                                        Guias Emitidas
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" data-parent="#accordion" style="height: 0px;">
                                <div class="panel-body">
                                    <div class="panel-body">
                                 <table class="table gTable">
                                    <thead>
                                      <tr>
                                        <th scope="col">Destino</th>
                                        <th class="text-right" scope="col">Guias</th>
                                        <th class="text-right" scope="col">Por Despachar</th>
                                      </tr>
                                    </thead>
                                    <tbody>';
                                    $total2=0;$tfData=array();
                                  foreach ($despachado as $t){
                                    $total2+=$t["qty"];
                                    $total4+=$t["guide_qty"];
                                    
                                    $diff = $t["guide_qty"]-$t["qty"];
                                    if ($diff!="0"){
                                            $class='text-danger';
                                          }else{
                                            $class='text-primary';
                                          }

                                    $total5+=$diff;      
                                    $tfData["op_destination_id"] = $t["id"];
                                    $tfData["op_inspection_report_id"] = $tfRequest->op_inspection_report_id;
                                    $html.='<tr>
                                        <th scope="row"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="DVP" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.$t["destination"].'</a></th>
                                        <td class="text-right">'.TfWidget::qty($t["guide_qty"]).'</td>';
                                    if ($diff>0){
                                      $html.='<td class="text-right"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="GPDD" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.TfWidget::qty($diff).'</a></td>';
                                    }else{
                                       $html.='<td class="text-right">'.TfWidget::qty($diff).'</td>';
                                    }
                                    $html.='</tr>';
                                  }


                                  $html.='<tfoot>
                                      <tr>
                                        <th scope="col">Total General</th>
                                        <th class="text-right" scope="col">'.$total4.'</th>';
                                    if ($total5>0){
                                      $html.='<th class="text-right" scope="col"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="GPDT2" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.TfWidget::qty($total5).'</a></th>';
                                    }else{
                                       $html.='<th class="text-right">'.TfWidget::qty($total5).'</th>';
                                    }
                                    $html.='</tr>
                                    </tfoot>
                                    </tbody>
                                  </table>

                                </div>
                            </div>
                        </div>
                    </div>';

                    if ($opInspectionReport["completion_date"]==''){



                    $html.='<div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading4">
                                <h4 class="panel-title">
                                    <a id="gg-a-4" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapseOne" >
                                        ROB
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading4" aria-expanded="false" data-parent="#accordion" style="">
                                <div class="panel-body">
                                   <table class="table gTable">
                                      <tbody>
                                        <tr>
                                          <th scope="row">Vehiculos Pendiente por Pesar</th>
                                          <td class="text-right">'.($total1-$total2).' </td>
                                        </tr>
                                        <tr>
                                          <th scope="row">ROB B/L '.$opInspectionReport["BL"].' </th>
                                          <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]).' Kgs</td>
                                        </tr>
                                        <tr>
                                          <th scope="row">ROB '.$opInspectionReport["product_1"].'</th>
                                          <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["ROB_PRODUCTO"]).' Kgs</td>
                                        </tr>
                                        <tr>
                                          <th scope="row">ROB DEL BUQUE</th>
                                          <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["ROB_BUQUE"]).' Kgs</td>
                                        </tr></tbody>
                                    </table>
                                    <span class="created_by pl-2">Fecha de actualizaci&oacute;n '.$opInspectionReportDetail["rob_date"].'</span>
                                    </div>
                            </div>
                        </div>';

              }



               $html.=' <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingA">
                      <h4 class="panel-title">
                        <a id="gg-a-A" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseA" aria-expanded="false" aria-controls="collapseA">Transportes (SICA/PESADOS)</a>
                      </h4>
                    </div>
                    <div id="collapseA" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingA" aria-expanded="false" data-parent="#accordion" style="">
                      <div class="panel-body">

                      <div class="row">';
                        foreach ($opInspectionReportDetailTransport as $t){
                          $diff = ($t["VEH_SICA"]-$t["VEH_PESADO"]);
                          if ($diff!="0"){
                            $class='text-danger';
                          }else{
                            $class='text-primary';
                          }
                          $html.='<div class="row col-12 mb-2 p-0">
                                    <div class="col-lg-8 col-9 p-0"><b>'.$t["transport"].'</b></div> 
                                    <div class="col-lg-4 col-3 p-0 text-right">'.$t["VEH_SICA"].'/'.$t["VEH_PESADO"].' <span class="'.$class.'">('.$diff.')</span></div> 
                                  </div> ';
                        }  
                     $html.='</div>

                      </div>
                    </div>
                  </div>


                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingB">
                      <h4 class="panel-title">
                        <a id="gg-a-B" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseB" aria-expanded="false" aria-controls="collapseB">Transportes (Por Cargar)</a>
                      </h4>
                    </div>
                    <div id="collapseB" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingB" aria-expanded="false" data-parent="#accordion" style="">
                      <div class="panel-body">
                      <table class="table gTable">
                                    <thead>
                                      <tr>
                                        <th scope="col">Transporte</th>
                                        <th class="text-right" scope="col">Cantidad</th>
                                      </tr>
                                    </thead>
                                    <tbody>';
                                    $totalQ=0;$tfData=array();
                                  foreach ($opInspectionReportDetailTransport2 as $t){
                                    $totalQ+=$t["qty"];
                                    
                                    $tfData["op_inspection_report_id"] = $tfRequest->op_inspection_report_id;
                                    $tfData["op_inspection_report_detail_transport"] = $t["transport"];
                                    
                                    $html.='<tr>
                                        <th scope="row"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="TPC1" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.$t["transport"].'</a></th>
                                        <td class="text-right">'.TfWidget::qty($t["qty"]).'</td> 
                                        </tr> ';
                                    
                                  }


                                  $html.='<tfoot>
                                      <tr>
                                        <th scope="col">Total General</th>
                                        <th class="text-right" scope="col">'.$totalQ.'</th>
                                        </tr>
                                    </tfoot>
                                    </tbody>
                                  </table> ';
                       /* foreach ($opInspectionReportDetailTransport2 as $t){
                          $html.='<div class="row col-12 mb-2 p-0">
                                  <div class="col-lg-3 col-6 p-0"><b>'.$t["transport"].'</b></div> 
                                  <div class="col-lg-3 col-6 p-0">'.$t["destination"].'</div> 
                                  <div class="col-lg-3 col-6 p-0">'.$t["SICA_guide"].' </div> 
                                  <div class="col-lg-3 col-6 p-0">'.$t["name"].'</div> 
                              </div>';
                       } */

                       
                     $html.='
                      </div>
                    </div>
                  </div>


                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingC">
                      <h4 class="panel-title">
                        <a id="gg-a-C" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseC" aria-expanded="false" aria-controls="collapseC">Estadisticas</a>
                      </h4>
                    </div>
                    <div id="collapseC" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingC" aria-expanded="false" data-parent="#accordion" style="">
                      <div class="panel-body">

                      <div class="row">';
                        
                        $html.='<div class="row col-12 mb-2">
                                <div class="col-lg-6">
                                  <div class="col-12 text-center subtitle mt-4">Vehiculos con Guia</div>
                                  <div class="col-lg-12 container c-chart">
                                    <div class="card mb-12">
                                      <div class="card-body"> 
                                        <div class="chart-container fixed-height-chart">
                                          <canvas id="chart-pr1"></canvas>
                                        </div>
                                      </div>        
                                    </div>
                                  </div>
                                  <div class="col-lg-12 text-center container">
                                    <span class="created_by">Generado el '.$generateDate.'</span>
                                  </div>
                              </div>    
                              <div class="col-lg-6">
                                  <div class="col-12 text-center subtitle mt-4">Vehiculos Pesados</div>
                                  <div class="col-lg-12 container c-chart">
                                    <div class="card mb-12">
                                      <div class="card-body"> 
                                        <div class="chart-container fixed-height-chart">
                                          <canvas id="chart-pr2"></canvas>
                                        </div>
                                      </div>        
                                    </div>
                                  </div>
                                  <div class="col-lg-12 text-center container">
                                    <span class="created_by">Generado el '.$generateDate.'</span>
                                  </div>
                                </div>';

                     $html.='</div>

                      </div>
                    </div>
                  </div>
              

              </div>  
            </div>';

    $html.='<div class="col-12 mt-3 mb-5 title">Destinos</div> ';

   foreach ($opDestinationList as $s){
    $html.='<div class="row col-12 mb-2">
                <div class="col-lg-6 col-12 p-0">
                  <div class="col-12 p-0"><b>Destino: </b></div>
                  <div class="col-12 p-0">'.$s["name"].'</div> 
                </div>  
                <div class="col-lg-3 col-12 p-0">
                  <div class="col-12 p-0"><b>BL: </b></div>
                  <div class="col-12 p-0">'.$s["BL"].'</div>  
                </div>  
                <div class="col-lg-3 col-12 p-0">
                  <div class="col-12 p-0"><b>Cantidad:</b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["qty"]).' Kgs</div>     
                </div>             
            </div> 
            <div class="row col-12 mb-2">
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Estim.: </b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["cant_veh_estim"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Prom:</b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["promedio_kgs"]).' Kgs</div>
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh.(SICA): </b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["VEH_SICA"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Aprox.(SICA): </b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["KG_APROX_SICA"]).' Kgs</div>  
                </div>  
            </div> 
            <div class="row col-12 mb-2">
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Pesado.: </b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["VEH_PESADO"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Prom Pesado:</b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["KG_APROX_PESADO"]).' Kgs</div>
                </div>  ';
        if ($opInspectionReport["completion_date"]!=''){
          $html.='  <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Remanente: </b> </div>
                  <div class="col-12 p-0">0</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">';
           if ($s["remanente_kgs"]>=0){
             $html.='    <div class="col-12 p-0"><b>Faltantes: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["remanente_kgs"]).' Kgs</div>  
                </div>  ';  
           } else{
             $html.='    <div class="col-12 p-0"><b>Excedentes: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["remanente_kgs"]*(-1)).' Kgs</div>  
                </div>  ';
           }    
        }else{
          $html.='  <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Remanente: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["remanente_unidades"]).'</div> 
                </div> 
                <div class="col-lg-3 col-6 p-0"> ';
           if ($s["remanente_kgs"]>=0){
             $html.='    <div class="col-12 p-0"><b>Remanente: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["remanente_kgs"]).' Kgs</div>  
                </div>  ';  
           } else{
             $html.='    <div class="col-12 p-0"><b>Excedentes: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["remanente_kgs"]*(-1)).' Kgs</div>  
                </div>  ';
           }    
        }         
        $html.='</div> 
                 <div class="col-12">
              <div class="panel-group mt-5 accordion" id="accordion-d'.$s["id"].'" role="tablist" aria-multiselectable="true">


              <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingA'.$s["id"].'">
                      <h4 class="panel-title">
                        <a id="gg-a-A'.$s["id"].'" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion-d'.$s["id"].'" href="#collapseA'.$s["id"].'" aria-expanded="false" aria-controls="collapseA'.$s["id"].'">Transportes (SICA/PESADOS)</a>
                      </h4>
                    </div>
                    <div id="collapseA'.$s["id"].'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingA'.$s["id"].'" aria-expanded="false" data-parent="#accordion-d'.$s["id"].'" style="">
                      <div class="panel-body">

                      <div class="row">';
                      $opTransportList = OpDestination::detailTransport($tfs,$s["id"]);   
                      foreach ($opTransportList as $t){
                          $diff = ($t["VEH_SICA"]-$t["VEH_PESADO"]);
                          if ($diff!="0"){
                            $class='text-danger';
                          }else{
                            $class='text-primary';
                          }
                          $html.='<div class="row col-12 mb-2 p-0">
                                  <div class="col-lg-8 col-9 p-0"><b>'.$t["transport"].'</b></div> 
                                  <div class="col-lg-4 col-3 p-0 text-right">'.$t["VEH_SICA"].'/'.$t["VEH_PESADO"].' <span class="'.$class.'">('.$diff.')</span></div> 
                              </div> ';
                      } 

                     $html.='</div>

                      </div>
                    </div>
                  </div>

              </div>  
            </div>'; 
   }


   $html.='                         
              </div>';


     $html.="<script type=\"text/javascript\">
 Chart.defaults.global.defaultFontFamily = \"'Roboto', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif\";
Chart.defaults.global.legend.position = 'bottom';
Chart.defaults.global.legend.labels.usePointStyle = true;
Chart.defaults.global.legend.labels.boxWidth = 15;
Chart.defaults.global.tooltips.backgroundColor = '#000';

var ggColors = [['rgba(192, 57, 43, 1)', 'rgba(192, 57, 43, 0.8)'],
                      ['rgba(234, 67, 53, 1)', 'rgba(234, 67, 53, 0.8)'],
                      ['rgba(17, 103, 177, 1)', 'rgba(17, 103, 177, 0.8)'],
                      ['rgba(24, 123, 205, 1)', 'rgba(24, 123, 205, 0.8)'],
                      ['rgba(52, 152, 219, 1)', 'rgba(52, 152, 219, 0.8)'],
                      ['rgba(42, 157, 244, 1)', 'rgba(42, 157, 244, 0.8)'],
                      ['rgba(22, 160, 133, 1)', 'rgba(22, 160, 133, 0.8)'],
                      ['rgba(39, 174, 96, 1)', 'rgba(39, 174, 96, 0.8)'],
                      ['rgba(46, 204, 113, 1)', 'rgba(46, 204, 113, 0.8)'],
                      ['rgba(241, 196, 15, 1)', 'rgba(241, 196, 15, 0.8)'],
                      ['rgba(243, 156, 18, 1)', 'rgba(243, 156, 18, 0.8)'],
                      ['rgba(230, 126, 34, 1)', 'rgba(230, 126, 34, 0.8)'],
                      ['rgba(211, 84, 0, 1)', 'rgba(211, 84, 0, 0.8)'],
                      ['rgba(255, 99, 88, 1)', 'rgba(255, 99, 88, 0.8)'],
                      ['rgba(128, 0, 128, 1)', 'rgba(128, 0, 128 , 0.8)'],
                      ['rgba(155, 89, 182, 1)', 'rgba(155, 89, 182, 0.8)'],
                      ['rgba(142, 68, 173, 1)', 'rgba(142, 68, 173, 0.8)'],
                      ['rgba(255, 67, 127, 1)', 'rgba(255, 67, 127, 0.8)'],
                      ['rgba(255, 53, 167, 1)', 'rgba(255, 53, 167, 0.8)'],
                      ['rgba(189, 195, 199, 1)', 'rgba(189, 195, 199, 0.8)'],
                      ['rgba(149, 165, 166, 1)', 'rgba(149, 165, 166, 0.8)'],
                      ['rgba(127, 140, 141, 1)', 'rgba(127, 140, 141, 0.8)'],
                      ['rgba(149, 112, 108, 1)', 'rgba(149, 112, 108, 0.8)'],
                      ['rgba(170, 85, 76, 1)', 'rgba(170, 85, 76, 0.8)']];     
    
var chartColors = [ggColors[0][0],ggColors[1][0],ggColors[2][0],ggColors[3][0],
                   ggColors[4][0],ggColors[5][0],ggColors[6][0],ggColors[7][0],
                   ggColors[8][0],ggColors[9][0],ggColors[10][0],ggColors[11][0],
                   ggColors[12][0],ggColors[13][0],ggColors[14][0],ggColors[15][0],
                   ggColors[16][0],ggColors[17][0],ggColors[18][0],ggColors[19][0],
                   ggColors[20][0],ggColors[21][0],ggColors[22][0],ggColors[23][0]];                   
                       
var chartColorsHover = [ggColors[0][1],ggColors[1][1],ggColors[2][1],ggColors[3][1],
                        ggColors[4][1],ggColors[5][1],ggColors[6][1],ggColors[7][1],
                        ggColors[8][1],ggColors[9][1],ggColors[10][1],ggColors[11][0],
                        ggColors[12][1],ggColors[13][1],ggColors[14][1],ggColors[15][1],
                        ggColors[16][1],ggColors[17][1],ggColors[18][1],ggColors[19][1],
                        ggColors[20][1],ggColors[21][1],ggColors[22][1],ggColors[23][1]];

var chartColorsShort = [ggColors[1][0],ggColors[3][0],ggColors[7][0],ggColors[9][0],ggColors[15][0]];
                       
var chartColorsShortHover = [ggColors[1][1],ggColors[3][1],ggColors[7][1],ggColors[9][1],ggColors[15][1]];

var ctx = document.getElementById('chart-pr1').getContext('2d');

  var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ".$chart_pr_label.",
        datasets: [{
           data: ".$chart_pr_data1.",
            borderWidth: 2,
                        hoverBorderWidth: 5,
                        backgroundColor: chartColors,
                        hoverbackgroundColor: chartColorsHover,
                        hoverBorderColor: chartColorsHover,
                        borderColor: chartColors,
        }]
    },
    options: {
                          responsive: true,
                          plugins: {
                              datalabels: {
                                  formatter: (value, ctx) => {
                                      let sum = 0;
                                      let dataArr = ctx.chart.data.datasets[0].data;
                                      dataArr.map(data => {
                                          sum += parseInt(data);
                                      });
                                      let percentage = (value*100 / sum).toFixed(2)+\"%\";
                                      return percentage;
                                  }
                              }
                          },
                          legend: {
                            position: 'bottom',
                          },
                          title: {
                            display: true,
                            text: ' '
                          },
                          animation: {
                            animateScale: true,
                            animateRotate: true
                          },
                          responsive: true,
                          maintainAspectRatio: false,

                        }
});
if ($(document).width()<450){
        myChart.options.legend.display=false;
}

var ctx = document.getElementById('chart-pr2').getContext('2d');

  var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ".$chart_pr_label.",
        datasets: [{
           data: ".$chart_pr_data2.",
            borderWidth: 2,
                        hoverBorderWidth: 5,
                        backgroundColor: chartColors,
                        hoverbackgroundColor: chartColorsHover,
                        hoverBorderColor: chartColorsHover,
                        borderColor: chartColors,
        }]
    },
    options: {
                          responsive: true,
                          plugins: {
                              datalabels: {
                                  formatter: (value, ctx) => {
                                      let sum = 0;
                                      let dataArr = ctx.chart.data.datasets[0].data;
                                      dataArr.map(data => {
                                          sum += parseInt(data);
                                      });
                                      let percentage = (value*100 / sum).toFixed(2)+\"%\";
                                      return percentage;
                                  }
                              }
                          },
                          legend: {
                            position: 'bottom',
                          },
                          title: {
                            display: true,
                            text: ' '
                          },
                          animation: {
                            animateScale: true,
                            animateRotate: true
                          },
                          responsive: true,
                          maintainAspectRatio: false,

                        }
});
if ($(document).width()<450){
        myChart.options.legend.display=false;
}
</script>";         
  echo $html;   
?>


