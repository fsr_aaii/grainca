<style type="text/css">
    
  #cu_forecast_dt .fixed{
    width: 10%; 

  }
  .c-chart .card{
    padding: 0;
    border: none !important;
  }
  .c-chart .card-body{
    padding: 0;
  }
  .c-chart .chart-container{
    padding: 0;
  }
.fixed-height-chart{
  height: 450px;
}
.my-primary-color {
    color: #36b3b9 !important;
}    


.heading-title
{
  margin-bottom: 100px;
}
#accordion .panel{
    border: none;
    box-shadow: none;
    margin-bottom: 5px;
}

#accordion .panel-heading{
    padding: 0;
    border: none;
    background: transparent;
}

#accordion .panel-title a{
    display: block;
    padding: 14px 50px 14px 30px;
    font-size: .9rem;
    color: #fff;
    background: #36b3b9;
    position: relative;
    border-bottom: none;
    border-radius: 5px 5px 0 0;
}

#accordion .panel-title a.collapsed{
    border-radius: 5px;
}


#accordion .panel-title a:before,
#accordion .panel-title a.collapsed:before{
    content: "\f106";
    font-family: 'Font Awesome 5 Pro';
    font-size: 20px;
    position: absolute;
    top: 12px;
    right: 30px;
}

#accordion .panel-title a.collapsed:before{
    content: "\f107";
}

#accordion .panel-body{
    font-size: 14px;
    color: #555;
    line-height: 25px;
    position: relative;
    border: none;
}


.gTable td, .gTable th {
  font-size: .9rem;
 padding: .75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}


@media (max-width: 1200px) {
  .gTable td, .gTable th {
    font-size: calc(.4rem + 1vw);
    padding: .75rem .25rem;
  }
}



</style>
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }
  

 $html='<div class="row">
            <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-12 container title">Transportes (Por Cargar)</div>
             <div class="col-12 mb-5 subtitle">'.$tfRequest->op_inspection_report_detail_transport.', '.OpDestination::description($tfs,$tfRequest->op_destination_id).'</div>  
             <div class="col-12 container text-right">
               <a class="btn-guaramo-text" data-tf-table="#tpc_dt" data-tf-file="TransportesPorCargar" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div> 
            
                                 <table id="tpc_dt" name="tpc_dt" class="display responsive" style="width:100%">
  <thead>
    <tr>
      <th scope="col" class="all">Guia SICA</th>
      <th class="desktop" scope="col">Nombre</th>
      <th class="desktop" scope="col">Cedula</th>
      <th scope="col" class="all text-right">Chuto</th>

      
    </tr>
  </thead>
  <tbody>';
  
foreach ($opInspectionReportDetailTransport5 as $t){

  $total2+=$t["qty"];
  
  $tfData["op_destination_id"] = $t["id"]; 
    $html.='<tr>
          <th scope="row">'.$t["SICA_guide"].'</th>
          <th scope="row">'.$t["name"].'</th>
          <th scope="row">'.$t["nin"].'</th>
          <td class="text-right">'.$t["chuto"].'</td>
          </tr>';
}


$html.='
  </tbody>
</table>

                                </div>
                           
                </div>



            </div>';


         
  echo $html;   
?>


<script type="text/javascript">$(document).ready(function() {
  $("#tpc_dt").DataTable({
    info:false,
    paging:false,
    lengthChange:false,
    dom: 'frtip',
  });
});
</script>