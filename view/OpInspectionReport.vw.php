<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

 switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_inspection_report_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpInspectionReport" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$opInspectionReport->getCreatedBy()).'  el '.$opInspectionReport->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_inspection_report_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpInspectionReport" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#op_inspection_report_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpInspectionReport" data-tf-action="AX" onclick="op_inspection_report_close_do(this);">Culminar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#op_inspection_report_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpInspectionReport" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($opInspectionReport->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($opInspectionReport->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }

  $tfData["op_destination_id_inspection_report"] = $opInspectionReport->getId();
  $tfData["op_notification_id_inspection_report"] = $opInspectionReport->getId();

  $html = '<div class="row">
  <div class="mx-auto col-lg-10 tf-card shadow mb-4">
    <form id="op_inspection_report_form" name="op_inspection_report_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Informe de Inspecci&oacute;n</div>
      <div class="col-lg-6 container">'.$audit.'</div>
      <div id="op_inspection_report_alert" name="op_inspection_report_alert" class="col-lg-12 container mt-5">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_op_inspection_report" name="is_op_inspection_report" value="'.$opInspectionReport->getInitialState().'">
         <input type="hidden" id="op_inspection_report_id" name="op_inspection_report_id" maxlength="22" value="'.$opInspectionReport->getId().'">

      </div>
      <div class="col-lg-5 container">
       <label for="op_inspection_report_id_customer" class="control-label">Cliente:</label>
        <select  id="op_inspection_report_id_customer" name="op_inspection_report_id_customer" class="op_inspection_report_id_customer form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(GlJuridicalPerson::selectOptions($tfs),$opInspectionReport->getIdCustomer()).
'      </select>
      <label for="op_inspection_report_id_customer" class="error">'.$opInspectionReport->getAttrError("id_customer").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="op_inspection_report_vessel" class="control-label">Buque:</label>
        <input type="text" id="op_inspection_report_vessel" name="op_inspection_report_vessel" class="op_inspection_report_vessel form-control"  maxlength="200"  value="'.$opInspectionReport->getVessel().'"  tabindex="2"/>
      <label for="op_inspection_report_vessel" class="error">'.$opInspectionReport->getAttrError("vessel").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="op_inspection_report_id_product_1" class="control-label">Producto:</label>
        <select  id="op_inspection_report_id_product_1" name="op_inspection_report_id_product_1" class="op_inspection_report_id_product_1 form-control" tabindex="4">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(OpProduct::selectOptions($tfs),$opInspectionReport->getIdProduct1()).
'      </select>
      <label for="op_inspection_report_id_product_1" class="error">'.$opInspectionReport->getAttrError("id_product_1").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="op_inspection_report_id_port_pier" class="control-label">Puerto - Muelle:</label>
        <select  id="op_inspection_report_id_port_pier" name="op_inspection_report_id_port_pier" class="op_inspection_report_id_port_pier form-control" tabindex="3">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(OpPortPier::selectOptions($tfs),$opInspectionReport->getIdPortPier()).
'      </select>
      <label for="op_inspection_report_id_port_pier" class="error">'.$opInspectionReport->getAttrError("id_port_pier").'</label>
      </div>
      <div class="col-lg-1 container">
       <label for="op_inspection_report_BL" class="control-label">BL:</label>
        <input type="text" id="op_inspection_report_BL" name="op_inspection_report_BL" class="op_inspection_report_BL form-control"  maxlength="22"  value="'.$opInspectionReport->getBL().'"  tabindex="5"/>
      <label for="op_inspection_report_BL" class="error">'.$opInspectionReport->getAttrError("BL").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="op_inspection_report_vessel_rob" class="control-label">Buque (Kgs):</label>
        <input type="text" id="op_inspection_report_vessel_rob" name="op_inspection_report_vessel_rob" class="op_inspection_report_vessel_rob form-control"  maxlength="22"  value="'.$opInspectionReport->getVesselRob().'"  tabindex="5"/>
      <label for="op_inspection_report_vessel_rob" class="error">'.$opInspectionReport->getAttrError("vessel_rob").'</label>
      </div> 
      <div class="col-lg-3 container">
       <label for="op_inspection_report_product_1_rob" class="control-label">Producto (Kgs):</label>
        <input type="text" id="op_inspection_report_product_1_rob" name="op_inspection_report_product_1_rob" class="op_inspection_report_product_1_rob form-control"  maxlength="22"  value="'.$opInspectionReport->getProduct1Rob().'"  tabindex="5"/>
      <label for="op_inspection_report_product_1_rob" class="error">'.$opInspectionReport->getAttrError("product_1_rob").'</label>
      </div>
       <div class="col-lg-2 container">
       <label for="op_inspection_report_quantity" class="control-label">Parcela (Kgs):</label>
        <input type="text" id="op_inspection_report_quantity" name="op_inspection_report_quantity" class="op_inspection_report_quantity form-control"  maxlength="22"  value="'.$opInspectionReport->getQuantity().'"  tabindex="5"/>
      <label for="op_inspection_report_quantity" class="error">'.$opInspectionReport->getAttrError("quantity").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="op_inspection_report_inspection_date" class="control-label">Fecha Inspecci&oacute;n :</label>
        <input type="text" id="op_inspection_report_inspection_date" name="op_inspection_report_inspection_date" class="op_inspection_report_inspection_date form-control"  maxlength="22"  value="'.$opInspectionReport->getInspectionDate().'"  tabindex="6"/>
      <label for="op_inspection_report_inspection_date" class="error">'.$opInspectionReport->getAttrError("inspection_date").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="op_inspection_report_issued_date" class="control-label">Fecha de Emision:</label>
        <input type="text" id="op_inspection_report_issued_date" name="op_inspection_report_issued_date" class="op_inspection_report_issued_date form-control"  maxlength="22"  value="'.$opInspectionReport->getIssuedDate().'"  tabindex="7"/>
      <label for="op_inspection_report_issued_date" class="error">'.$opInspectionReport->getAttrError("issued_date").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="op_inspection_report_completion_date" class="control-label">Fecha de Culminacion:</label>
        <input type="text" id="op_inspection_report_completion_date" name="op_inspection_report_completion_date" class="op_inspection_report_completion_date form-control"  maxlength="22"  value="'.$opInspectionReport->getCompletionDate().'"  tabindex="7"/>
      <label for="op_inspection_report_completion_date" class="error">'.$opInspectionReport->getAttrError("completion_date").'</label>
      </div>

   
     <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>

   </fieldset>
  </form>';
if ($opInspectionReport->getId()!=''){             

$html.='<div class="row p-0 col-12">
 <div class="col-6 title">Destinos</div>
             <div class="col-6 text-right action">
               <a class="btn btn-guaramo-text" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpDestination" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                 Nuevo</a>
             </div>
</div>


<div class="row col-12">';
$i=0;
foreach ($opDestinationList as $row){
    $i++;
 
    $tfData["op_destination_id"] = $row["id"];
    
    $html.='  <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpDestination" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);" data-string="'.$row["name"].''.$row["rif"].'">
            <div class="d-flex align-items-center bg-info p-4" >
                <div class="ml-2">
                    <div class="epic h2 text-white m-0">'.$row["name"].'</div>
                    <div class="epic h4 text-white m-0">BL: '.$row["BL"].' '.$row["qty"].' Kgs.</div>
                </div>
            </div>
          </div>';  

       if ($i%3==0){
      $html.='</div>
          <div class="row col-12">';
    }    
   }


   $html.='</div>
            <div class="row p-0 col-12">
 <div class="col-6 title">Notificaciones</div>
             <div class="col-6 text-right action">
               <a class="btn btn-guaramo-text" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpNotification" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                 Nueva</a>
             </div>
</div>             

<div class="row col-12">';
$i=0;
foreach ($opNotificationList as $row){
    $i++;
 
    $tfData["op_notification_id"] = $row["id"];
    
    $html.='  <div class="col-lg-6 coaching">
            <div class="d-flex align-items-center bg-dark p-4" >
                <div class="ml-2">
                    <div class="epic h2 text-white m-0">'.$row["subject"].'</div>
                    <div class="epic h4 text-white m-0">'.$row["body"].'</div>
                    <div class="epic h5 text-white m-0">Creado por '.$row["created_by"].'  el '.$row["created_date"].'</div>
                </div>
            </div>
          </div>';  

       if ($i%2==0){
      $html.='</div>
          <div class="row col-12">';
    }    
   }

   
   $html.='</div>';
  } 
 $html.='</div>
</div>';
  echo $html;
?>
<script type="text/javascript">
  function op_inspection_report_close_do(element){
    if ($("#op_inspection_report_completion_date").val() != "") {
      TfRequest.do(element,true);
      console.log("DO");
   }else{
      $("#op_inspection_report_alert").html("<div class=\"notice notice-danger\"><span class=\"far fa-bell\"></span> Para CULMINAR este reporte debe introducir la fecha de culminaci&oacute;n</div>");
   }
    
  }
  function opInspectionReportRules(){
  $("#op_inspection_report_form").validate();
  $("#op_inspection_report_id_customer").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#op_inspection_report_vessel").rules("add", {
    required:true,
    maxlength:200
  });
  $("#op_inspection_report_id_port_pier").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#op_inspection_report_id_product_1").rules("add", {
    required:true,
    maxlength:450
  });
  $("#op_inspection_report_BL").rules("add", {
    required:true,
    maxlength:45
  });
  $("#op_inspection_report_vessel_rob").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#op_inspection_report_product_1_rob").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#op_inspection_report_quantity").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#op_inspection_report_inspection_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#op_inspection_report_issued_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#op_inspection_report_completion_date").rules("add", {
    isDate:true,
    maxlength:22
  });

}


$("#op_inspection_report_inspection_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#op_inspection_report_issued_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#op_inspection_report_completion_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});  

$(document).ready(function(){
  opInspectionReportRules();

  $("#op_inspection_report_inspection_date").css('vertical-align','top');
  $("#op_inspection_report_inspection_date").mask('y999-m9-d9');
  $("#op_inspection_report_issued_date").css('vertical-align','top');
  $("#op_inspection_report_issued_date").mask('y999-m9-d9');
  $("#op_inspection_report_completion_date").css('vertical-align','top');
  $("#op_inspection_report_completion_date").mask('y999-m9-d9');

})
</script>

