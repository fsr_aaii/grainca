<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_destination_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpDestination" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$opDestination->getCreatedBy()).'  el '.$opDestination->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_destination_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpDestination" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#op_destination_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpDestination" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($opDestination->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($opDestination->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="op_destination_form" name="op_destination_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Destino</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_op_destination" name="is_op_destination" value="'.$opDestination->getInitialState().'">
         <input type="hidden" id="op_destination_id" name="op_destination_id" maxlength="22" value="'.$opDestination->getId().'">
         <input type="hidden" id="op_destination_id_inspection_report" name="op_destination_id_inspection_report" maxlength="22" value="'.$opDestination->getIdInspectionReport().'">

      </div>
      <div class="col-lg-12 container">
       <label for="op_destination_name" class="control-label">Destino:</label>
        <input type="text" id="op_destination_name" name="op_destination_name" class="op_destination_name form-control"  maxlength="200"  value="'.$opDestination->getName().'"  tabindex="2"/>
      <label for="op_destination_name" class="error">'.$opDestination->getAttrError("name").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="op_destination_qty" class="control-label">Kgs:</label>
        <input type="text" id="op_destination_qty" name="op_destination_qty" class="op_destination_qty form-control"  maxlength="22"  value="'.$opDestination->getQty().'"  tabindex="3"/>
      <label for="op_destination_qty" class="error">'.$opDestination->getAttrError("qty").'</label>
      </div>
      <div class="col-lg-8 container">
       <label for="op_destination_BL" class="control-label">BL:</label>
        <input type="text" id="op_destination_BL" name="op_destination_BL" class="op_destination_BL form-control"  maxlength="45"  value="'.$opDestination->getBL().'"  tabindex="4"/>
      <label for="op_destination_BL" class="error">'.$opDestination->getAttrError("BL").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function opDestinationRules(){
  $("#op_destination_form").validate();
  $("#op_destination_id_inspection_report").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#op_destination_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#op_destination_qty").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#op_destination_BL").rules("add", {
    required:true,
    maxlength:45
  });

}


$(document).ready(function(){
  opDestinationRules();


})
</script>