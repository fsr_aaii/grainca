<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_product_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="opProduct" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$opProduct->getCreatedBy()).'  el '.$opProduct->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_product_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="opProduct" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#op_product_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="opProduct" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($opProduct->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($opProduct->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-10 tf-card shadow mb-4">
    <form id="op_product_form" name="op_product_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Op Product</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_op_product" name="is_op_product" value="'.$opProduct->getInitialState().'">
         <input type="hidden" id="op_product_id" name="op_product_id" maxlength="22" value="'.$opProduct->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="op_product_name" class="control-label">Name:</label>
        <input type="text" id="op_product_name" name="op_product_name" class="op_product_name form-control"  maxlength="200"  value="'.$opProduct->getName().'"  tabindex="1"/>
      <label for="op_product_name" class="error">'.$opProduct->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_product_active" class="control-label">Active:</label>
        <select  id="op_product_active" name="op_product_active" class="op_product_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($opProduct->getActive(),'Y').
'      </select>
      <label for="op_product_active" class="error">'.$opProduct->getAttrError("active").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function opProductRules(){
  $("#op_product_form").validate();
  $("#op_product_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#op_product_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  opProductRules();


})
</script>