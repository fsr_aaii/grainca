<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#hr_user_evaluation_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrUserEvaluationCriteria" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$hrUserEvaluationCriteria->getCreatedBy()).'  el '.$hrUserEvaluationCriteria->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#hr_user_evaluation_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrUserEvaluationCriteria" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#hr_user_evaluation_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrUserEvaluationCriteria" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrUserEvaluationCriteria->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrUserEvaluationCriteria->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-10 tf-card shadow mb-4">
    <form id="hr_user_evaluation_criteria_form" name="hr_user_evaluation_criteria_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr User Evaluation Criteria</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_user_evaluation_criteria" name="is_hr_user_evaluation_criteria" value="'.$hrUserEvaluationCriteria->getInitialState().'">
         <input type="hidden" id="hr_user_evaluation_criteria_id" name="hr_user_evaluation_criteria_id" maxlength="22" value="'.$hrUserEvaluationCriteria->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_user_evaluation_criteria_id_user_evaluation" class="control-label">Id User Evaluation:</label>
        <select  id="hr_user_evaluation_criteria_id_user_evaluation" name="hr_user_evaluation_criteria_id_user_evaluation" class="hr_user_evaluation_criteria_id_user_evaluation form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrUserEvaluation::selectOptions($tfs),$hrUserEvaluationCriteria->getIdUserEvaluation()).
'      </select>
      <label for="hr_user_evaluation_criteria_id_user_evaluation" class="error">'.$hrUserEvaluationCriteria->getAttrError("id_user_evaluation").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_user_evaluation_criteria_id_evaluation_criteria" class="control-label">Id Evaluation Criteria:</label>
        <select  id="hr_user_evaluation_criteria_id_evaluation_criteria" name="hr_user_evaluation_criteria_id_evaluation_criteria" class="hr_user_evaluation_criteria_id_evaluation_criteria form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrEvaluationCriteria::selectOptions($tfs),$hrUserEvaluationCriteria->getIdEvaluationCriteria()).
'      </select>
      <label for="hr_user_evaluation_criteria_id_evaluation_criteria" class="error">'.$hrUserEvaluationCriteria->getAttrError("id_evaluation_criteria").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_user_evaluation_criteria_value" class="control-label">Value:</label>
        <input type="text" id="hr_user_evaluation_criteria_value" name="hr_user_evaluation_criteria_value" class="hr_user_evaluation_criteria_value form-control"  maxlength="22"  value="'.$hrUserEvaluationCriteria->getValue().'"  tabindex="3"/>
      <label for="hr_user_evaluation_criteria_value" class="error">'.$hrUserEvaluationCriteria->getAttrError("value").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrUserEvaluationCriteriaRules(){
  $("#hr_user_evaluation_criteria_form").validate();
  $("#hr_user_evaluation_criteria_id_user_evaluation").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_user_evaluation_criteria_id_evaluation_criteria").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_user_evaluation_criteria_value").rules("add", {
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  hrUserEvaluationCriteriaRules();


})
</script>