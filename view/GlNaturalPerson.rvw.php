<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8">
            <div class="col-6 title">Gl Natural Person</div>
             <div class="col-6 text-right action">
               <a class="button" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlNaturalPerson" data-tf-action="AN" onclick="TfRequest.do(this);">
                 <span>Add</span><div class="icon"><span class="far fa-plus"></span></div>
               </a>
               <a class="button" role="button" data-tf-table="#gl_natural_person_dt" data-tf-file="Gl Natural Person" onclick="TfExport.excel(this);">
                 <span>Excel</span><div class="icon"><span class="far fa-download"></span></div>
               </a>
             </div>
       <table id="gl_natural_person_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Nin Type</th>
             <th class="all">Nin</th>
             <th class="all">Nin Control Digit</th>
             <th class="all">First Name</th>
             <th class="all">Middle Name</th>
             <th class="all">Last Name</th>
             <th class="all">Second Last Name</th>
             <th class="all">Photo</th>
             <th class="all">Id Nationality Country</th>
             <th class="all">Id Gender</th>
             <th class="all">Id Birth Country</th>
             <th class="all">Birthplace</th>
             <th class="all">Birthdate</th>
             <th class="all">Id Marital Status</th>
             <th class="all">Height</th>
             <th class="all">Size</th>
             <th class="all">Weight</th>
             <th class="all">Is Right Handed</th>
             <th class="all">Id Blood Type</th>
             <th class="all">Is Functional Diversity</th>
             <th class="all">Id Study Level</th>
             <th class="all">Id Residence Country</th>
             <th class="none">Created by</th>
             <th class="none">Created date</th>
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($glNaturalPersonList as $row){
    $tfData["gl_natural_person_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["nin_type"].'</td>
            <td>'.$row["nin"].'</td>
            <td>'.$row["nin_control_digit"].'</td>
            <td>'.$row["first_name"].'</td>
            <td>'.$row["middle_name"].'</td>
            <td>'.$row["last_name"].'</td>
            <td>'.$row["second_last_name"].'</td>
            <td>'.$row["photo"].'</td>
            <td>'.$row["id_nationality_country"].'</td>
            <td>'.$row["id_gender"].'</td>
            <td>'.$row["id_birth_country"].'</td>
            <td>'.$row["birthplace"].'</td>
            <td>'.$row["birthdate"].'</td>
            <td>'.$row["id_marital_status"].'</td>
            <td>'.$row["height"].'</td>
            <td>'.$row["size"].'</td>
            <td>'.$row["weight"].'</td>
            <td>'.$row["is_right_handed"].'</td>
            <td>'.$row["id_blood_type"].'</td>
            <td>'.$row["is_functional_diversity"].'</td>
            <td>'.$row["id_study_level"].'</td>
            <td>'.$row["id_residence_country"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="dt-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlNaturalPerson" data-tf-action="AE" data-tf-data="'.tfRequest::encrypt($tfData).'"onclick="TfRequest.do(this);">
                     <span class="far fa-pencil"></span>
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#gl_natural_person_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>