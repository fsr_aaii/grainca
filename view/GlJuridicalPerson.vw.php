<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_juridical_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="glJuridicalPerson" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$glJuridicalPerson->getCreatedBy()).'  el '.$glJuridicalPerson->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_juridical_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="glJuridicalPerson" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#gl_juridical_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="glJuridicalPerson" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($glJuridicalPerson->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($glJuridicalPerson->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-10 tf-card shadow mb-4">
    <form id="gl_juridical_person_form" name="gl_juridical_person_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Gl Juridical Person</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_gl_juridical_person" name="is_gl_juridical_person" value="'.$glJuridicalPerson->getInitialState().'">
         <input type="hidden" id="gl_juridical_person_id" name="gl_juridical_person_id" maxlength="22" value="'.$glJuridicalPerson->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="gl_juridical_person_nin_type" class="control-label">Nin Type:</label>
        <input type="text" id="gl_juridical_person_nin_type" name="gl_juridical_person_nin_type" class="gl_juridical_person_nin_type form-control"  maxlength="1"  value="'.$glJuridicalPerson->getNinType().'"  tabindex="1"/>
      <label for="gl_juridical_person_nin_type" class="error">'.$glJuridicalPerson->getAttrError("nin_type").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_juridical_person_nin" class="control-label">Nin:</label>
        <input type="text" id="gl_juridical_person_nin" name="gl_juridical_person_nin" class="gl_juridical_person_nin form-control"  maxlength="22"  value="'.$glJuridicalPerson->getNin().'"  tabindex="2"/>
      <label for="gl_juridical_person_nin" class="error">'.$glJuridicalPerson->getAttrError("nin").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_juridical_person_nin_control_digit" class="control-label">Nin Control Digit:</label>
        <input type="text" id="gl_juridical_person_nin_control_digit" name="gl_juridical_person_nin_control_digit" class="gl_juridical_person_nin_control_digit form-control"  maxlength="22"  value="'.$glJuridicalPerson->getNinControlDigit().'"  tabindex="3"/>
      <label for="gl_juridical_person_nin_control_digit" class="error">'.$glJuridicalPerson->getAttrError("nin_control_digit").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_juridical_person_legal_name" class="control-label">Legal Name:</label>
        <input type="text" id="gl_juridical_person_legal_name" name="gl_juridical_person_legal_name" class="gl_juridical_person_legal_name form-control"  maxlength="200"  value="'.$glJuridicalPerson->getLegalName().'"  tabindex="4"/>
      <label for="gl_juridical_person_legal_name" class="error">'.$glJuridicalPerson->getAttrError("legal_name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_juridical_person_trade_name" class="control-label">Trade Name:</label>
        <input type="text" id="gl_juridical_person_trade_name" name="gl_juridical_person_trade_name" class="gl_juridical_person_trade_name form-control"  maxlength="200"  value="'.$glJuridicalPerson->getTradeName().'"  tabindex="5"/>
      <label for="gl_juridical_person_trade_name" class="error">'.$glJuridicalPerson->getAttrError("trade_name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_juridical_person_legal_address" class="control-label">Legal Address:</label>
        <textarea id="gl_juridical_person_legal_address" name="gl_juridical_person_legal_address" class="gl_juridical_person_legal_address form-control" rows="3" >'.$glJuridicalPerson->getLegalAddress().'</textarea>
      <label for="gl_juridical_person_legal_address" class="error">'.$glJuridicalPerson->getAttrError("legal_address").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_juridical_person_phone_number" class="control-label">Phone Number:</label>
        <input type="text" id="gl_juridical_person_phone_number" name="gl_juridical_person_phone_number" class="gl_juridical_person_phone_number form-control"  maxlength="20"  value="'.$glJuridicalPerson->getPhoneNumber().'"  tabindex="7"/>
      <label for="gl_juridical_person_phone_number" class="error">'.$glJuridicalPerson->getAttrError("phone_number").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_juridical_person_active" class="control-label">Active:</label>
        <select  id="gl_juridical_person_active" name="gl_juridical_person_active" class="gl_juridical_person_active form-control" tabindex="8">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($glJuridicalPerson->getActive(),'Y').
'      </select>
      <label for="gl_juridical_person_active" class="error">'.$glJuridicalPerson->getAttrError("active").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function glJuridicalPersonRules(){
  $("#gl_juridical_person_form").validate();
  $("#gl_juridical_person_nin_type").rules("add", {
    required:true,
    maxlength:1
  });
  $("#gl_juridical_person_nin").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_juridical_person_nin_control_digit").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_juridical_person_legal_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#gl_juridical_person_trade_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#gl_juridical_person_legal_address").rules("add", {
    required:true,
    maxlength:1000
  });
  $("#gl_juridical_person_phone_number").rules("add", {
    required:true,
    maxlength:20
  });
  $("#gl_juridical_person_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  glJuridicalPersonRules();


})
</script>