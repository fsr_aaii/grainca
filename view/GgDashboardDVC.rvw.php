<style type="text/css">
    
  #cu_forecast_dt .fixed{
    width: 10%; 

  }
  .c-chart .card{
    padding: 0;
    border: none !important;
  }
  .c-chart .card-body{
    padding: 0;
  }
  .c-chart .chart-container{
    padding: 0;
  }
.fixed-height-chart{
  height: 450px;
}
.my-primary-color {
    color: #36b3b9 !important;
}    


.heading-title
{
  margin-bottom: 100px;
}
#accordion .panel{
    border: none;
    box-shadow: none;
    margin-bottom: 5px;
}

#accordion .panel-heading{
    padding: 0;
    border: none;
    background: transparent;
}

#accordion .panel-title a{
    display: block;
    padding: 14px 50px 14px 30px;
    font-size: .9rem;
    color: #fff;
    background: #36b3b9;
    position: relative;
    border-bottom: none;
    border-radius: 5px 5px 0 0;
}

#accordion .panel-title a.collapsed{
    border-radius: 5px;
}


#accordion .panel-title a:before,
#accordion .panel-title a.collapsed:before{
    content: "\f106";
    font-family: 'Font Awesome 5 Pro';
    font-size: 20px;
    position: absolute;
    top: 12px;
    right: 30px;
}

#accordion .panel-title a.collapsed:before{
    content: "\f107";
}

#accordion .panel-body{
    font-size: 14px;
    color: #555;
    line-height: 25px;
    position: relative;
    border: none;
}


.gTable td, .gTable th {
  font-size: .9rem;
}

@media (max-width: 1200px) {
  .gTable td, .gTable th {
    font-size: calc(.5rem + 1vw);
  }
}
/* CSS Document */



</style>
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }
  

  
  $html='<div class="row">
            <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-12 mb-0 title">Vehiculos Cargados</div>  
            <div class="col-12 mb-5 subtitle">'.OpDestination::description($tfs,$tfRequest->op_destination_id).'</div>  
           
                                 <table class="table gTable">
  <thead>
    <tr>
      <th scope="col">Transporte</th>
      <th class="text-right" scope="col">Cant. Veh.</th>
      <th class="text-right" scope="col">Peso kg.</th>
    </tr>
  </thead>
  <tbody>';

$total1=0;$total5=0;  
foreach ($cargado as $t){
$total1+=$t["qty"];
$total5+=$t["kg"];
$html.='<tr>
      <th scope="row">'.$t["transport"].'</th>
      <td class="text-right">'.TfWidget::qty($t["qty"]).'</td>
      <td class="text-right">'.TfWidget::qty($t["kg"]).'</td>
    </tr>';
}


$html.='<tfoot>
    <tr>
      <th scope="col">Total General</th>
      <th class="text-right" scope="col">'.$total1.'</th>
      <th class="text-right" scope="col">'.TfWidget::qty($total5).'</th>
    </tr>
  </tfoot>
  </tbody>
</table>

                </div>



            </div>';


         
  echo $html;   
?>


