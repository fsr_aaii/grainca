<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }
  
  $tfData["hr_user_evaluation_id"] = $hrUserEvaluation->getId();

  $html='<div class="row">
           <div class="mx-auto col-lg-8 info-panel">
           <div class="jumbotron">
  <h1 class="display-4">DIA DE PRUEBA!</h1>
  <p class="lead">Hoy es el dia para que consolides tus conocimientos, a continuaci&oacute;n se te har&aacute;n <?php echo APP_EVALUATION_QTY?> preguntas que estaban en tu guia de <?php echo APP_HOMEWORK_QTY?> preguntas, y tendras <?php echo APP_EVALUATION_TIME?> minutos para contestar.</p>
  <hr class="my-4">
  <p>El tiempo empezar&aacute; a correr despues de que le des clic al boton "Contestar".</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgMiGuia" data-tf-action="AT" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">Contestar</a>

  </p>
</div>
         </div>
        </div>';
 echo $html;
?>