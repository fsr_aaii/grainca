<?php

 
$html='<div class="row">
            <div class="mx-auto col-lg-10 tf-card shadow mb-4 ">
              <div class="col-lg-12 title system">RAD tool</div>
              <div class="col-lg-6 multi-col">
                <h5>Options</h5>
                <form id="mt_form" name="mt_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
                  <fieldset>
                     <div class="col-lg-12 container">
                     '.$objAlerts.'
                     </div>
                     <div class="col-lg-12 container">
                        <label for="mt_entity"  class="control-label">Entity:</label>
                        <div class="input-group">
                        <select id="mt_entity" name="mt_entity" class="mt_entity form-control">
                            <option value="">Select a option</option>
                        '.TfWidget::selectStructure($entities,$tfRequest->mt_entity).'
                        </select>
                        <span class="input-group-btn">
                         <a class="btn btn-outline-info" role="button" data-tf-form="#mt_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TMenukatok" data-tf-action="GN" onclick="TfRequest.do(this,true);"><span class="fas fa-cog"></span> Generate</a>
                       </span>
                     </div>
                        <label for="mt_entity" class="error"></label>
                     </div>
      
                      <div class="col-lg-12 container">
<ul class="list-group ">
      <li class="list-group-item form-control">
      <input type="checkbox" id="gAll" name="gAll" class="gAll" value="gAll" '.$gAll.' '.$gAllCheck.'> Select all
      </li>
      <li class="list-group-item form-control">
      <input type="checkbox" id="gModelBase" name="gModelBase" class="gCheck gModelBase" value="gModelBase" '.$gModelBaseCheck.'> Model Base class (.mdl.php):
      </li>
      <li class="list-group-item form-control">
       <input type="checkbox" id="gModel" name="gModel" class="gCheck gModel" value="gModel" '.$gModelCheck.'> Model class (.mdl.php):
      </li>
      <li class="list-group-item form-control">
       <input type="checkbox" id="gController" name="gController" class="gCheck gController" value="gController" '.$gControllerCheck.'> Controller file (.ctrl.php):
      </li>
      <li class="list-group-item form-control">
       <input type="checkbox" id="gView" name="gView" class="gCheck gView" value="gView" '.$gViewCheck.'> View file (.vw.php):
      </li>
      <li class="list-group-item form-control">
       <input type="checkbox" id="gDTView" name="gDTView" class="gCheck gDTView" value="gDTView" '.$gDTViewCheck.'> Data table file (.rvw.php):
      </li>
      <li class="list-group-item form-control">
       <input type="checkbox" id="gAuditTrigger" name="gAuditTrigger" class="gCheck gAuditTrigger" value="gAuditTrigger" '.$gAuditTrigger.'> Audit trigger file (.sql):
      </li>
    </ul>         
                       </div>
                  </fieldset>
                </form>
              </div>
              <div class="col-lg-6 multi-col">
               <h5>Generation logs</h5>
               '.$logs.'
              </div> 
            </div>
          </div>';  
echo $html;
?>
<script type="text/javascript">
  
function tMenukatokRules(){
  $("#mt_form").validate();

  $("#mt_entity").rules("add", {
    required:true,
    maxlength:30
  });
}


$(document).ready(function(){
   
  $("#gAll").on('change',function(){  
    var checkboxes = $(this).closest('form').find(':checkbox');
    checkboxes.prop('checked', $(this).is(':checked'));
  });

   tMenukatokRules();
})

</script>

