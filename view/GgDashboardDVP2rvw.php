<style type="text/css">
    
  #cu_forecast_dt .fixed{
    width: 10%; 

  }
  .c-chart .card{
    padding: 0;
    border: none !important;
  }
  .c-chart .card-body{
    padding: 0;
  }
  .c-chart .chart-container{
    padding: 0;
  }
.fixed-height-chart{
  height: 450px;
}
.my-primary-color {
    color: #36b3b9 !important;
}    


.heading-title
{
  margin-bottom: 100px;
}
#accordion .panel{
    border: none;
    box-shadow: none;
    margin-bottom: 5px;
}

#accordion .panel-heading{
    padding: 0;
    border: none;
    background: transparent;
}

#accordion .panel-title a{
    display: block;
    padding: 14px 50px 14px 30px;
    font-size: .9rem;
    color: #fff;
    background: #36b3b9;
    position: relative;
    border-bottom: none;
    border-radius: 5px 5px 0 0;
}

#accordion .panel-title a.collapsed{
    border-radius: 5px;
}


#accordion .panel-title a:before,
#accordion .panel-title a.collapsed:before{
    content: "\f106";
    font-family: 'Font Awesome 5 Pro';
    font-size: 20px;
    position: absolute;
    top: 12px;
    right: 30px;
}

#accordion .panel-title a.collapsed:before{
    content: "\f107";
}

#accordion .panel-body{
    font-size: 14px;
    color: #555;
    line-height: 25px;
    position: relative;
    border: none;
}


.gTable td, .gTable th {
  font-size: .9rem;
}

@media (max-width: 1200px) {
  .gTable td, .gTable th {
    font-size: calc(.5rem + 1vw);
  }
}
/* CSS Document */



</style>
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }
  

  $asignacion = round($opInspectionReport["quantity"]/($opInspectionReportDetail["KG_APROX_PESADO"]/$opInspectionReportDetail["VEH_PESADO"]));
  $html='<div class="row">
            <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-12 mb-0 title">Vehiculos Pesados</div>  
            <div class="col-12 mb-5 subtitle">'.OpDestination::description($tfs,$tfRequest->op_destination_id).'</div>  
            
                                 <table class="table gTable">
  <thead>
    <tr>
      <th scope="col">Destino</th>
      <th class="text-right" scope="col">Cant. Veh.</th>
      <th class="text-right" scope="col">Peso Neto kg.</th>
    </tr>
  </thead>
  <tbody>';
  $total2=0;$total3=0;$tfData=array();
foreach ($despachado as $t){
  $total2+=$t["qty"];
  $total3+=$t["kg"];
$html.='<tr>
      <th scope="row">'.$t["transport"].'</th>
      <td class="text-right">'.TfWidget::qty($t["qty"]).'</td>
      <td class="text-right">'.TfWidget::qty($t["kg"]).'</td>
    </tr>';
}


$html.='<tfoot>
    <tr>
      <th scope="col">Total General</th>
      <th class="text-right" scope="col">'.$total2.'</th>
      <th class="text-right" scope="col">'.TfWidget::qty($total3).'</th>
    </tr>
  </tfoot>
  </tbody>
</table>

                                </div>
                           
                </div>



            </div>';


         
  echo $html;   
?>


