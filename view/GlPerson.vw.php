<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn" role="button" data-tf-form="#gl_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="glPerson" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$glPerson->getCreatedBy()).'  on '.$glPerson->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn" role="button" data-tf-form="#gl_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="glPerson" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn" role="button" data-tf-form="#gl_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="glPerson" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($glPerson->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($glPerson->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-10 tf-card shadow mb-4">
    <form id="gl_person_form" name="gl_person_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title mb-5">Gl Person</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_gl_person" name="is_gl_person" value="'.$glPerson->getInitialState().'">
         <input type="hidden" id="gl_person_id" name="gl_person_id" maxlength="22" value="'.$glPerson->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_person_type" class="control-label">Person Type:</label>
        <input type="text" id="gl_person_person_type" name="gl_person_person_type" class="gl_person_person_type form-control"  maxlength="1"  value="'.$glPerson->getPersonType().'"  tabindex="1"/>
      <label for="gl_person_person_type" class="error">'.$glPerson->getAttrError("person_type").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function glPersonRules(){
  $("#gl_person_form").validate();
  $("#gl_person_person_type").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  glPersonRules();


})
</script>