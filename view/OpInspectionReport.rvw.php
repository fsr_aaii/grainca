<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            
            <div class="col-6 title">Informes de Inspecci&oacute;n</div>
             <div class="col-6 text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpInspectionReport" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>


             <div class="col-12 container">
 <div class="row justify-content-end">

    <div class="col-lg-4 input-group mb-3">
  <input type="text" class="filter form-control" placeholder="Buscar..." aria-label="Username" aria-describedby="basic-addon1">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1"><i class="bx bx-search"></i></span>
  </div>
</div>
  </div>
</div>

<div class="row col-12">';
$i=0;
foreach ($opInspectionReportList as $row){
    $i++;
    $tfData["op_inspection_report_id"] = $row["id"];
    
    $html.='  <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpInspectionReport" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);" data-string="'.$row["customer"].' '.$row["port_pier"].' '.$row["vessel"].' '.$row["BL"].' '.$row["product_1"].' '.$row["product_2"].'">
            <div class="d-flex align-items-center bg-info p-4" >
                <div class="ml-2">
                    <div class="epic h5 text-white m-0">'.$row["customer"].'</div>                    
                    <div class="epic text-uppercase h5 text-white m-0">'.$row["product_1"].'</div>
                    <div class="epic h5 text-white m-0">'.$row["port_pier"].' - '.$row["vessel"].'</div>
                    <div class="epic h5 text-white m-0">BL '.$row["BL"].'  '.TfWidget::qty($row["quantity"]).'Kgs</div>
                    <div class="epic h5 text-white m-0">'.TfWidget::ymd2dmy($row["inspection_date"]).'</div>
                    
                </div>
            </div>
          </div>';  

       if ($i%3==0){
      $html.='</div>
          <div class="row col-12">';
    }    
   }
   $html.='</div>
          </div>
        </div>';
 echo $html;
?>

<script type="text/javascript">
  
  $(".filter").on("keyup", function() {
  var input = $(this).val().toUpperCase();

  $(".coaching").each(function() {
    if ($(this).data("string").toUpperCase().indexOf(input) < 0) {
      $(this).hide();
    } else {
      $(this).show();
    }
  })
});

</script>