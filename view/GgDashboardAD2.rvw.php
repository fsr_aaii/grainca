<style type="text/css">
    
  #cu_forecast_dt .fixed{
    width: 10%; 

  }
  .c-chart .card{
    padding: 0;
    border: none !important;
  }
  .c-chart .card-body{
    padding: 0;
  }
  .c-chart .chart-container{
    padding: 0;
  }
.fixed-height-chart{
  height: 450px;
}
.my-primary-color {
    color: #36b3b9 !important;
}    


.heading-title
{
  margin-bottom: 100px;
}
#accordion .panel{
    border: none;
    box-shadow: none;
    margin-bottom: 5px;
}

#accordion .panel-heading{
    padding: 0;
    border: none;
    background: transparent;
}

#accordion .panel-title a{
    display: block;
    padding: 14px 50px 14px 30px;
    font-size: .9rem;
    color: #fff;
    background: #36b3b9;
    position: relative;
    border-bottom: none;
    border-radius: 5px 5px 0 0;
}

#accordion .panel-title a.collapsed{
    border-radius: 5px;
}


#accordion .panel-title a:before,
#accordion .panel-title a.collapsed:before{
    content: "\f106";
    font-family: 'Font Awesome 5 Pro';
    font-size: 20px;
    position: absolute;
    top: 12px;
    right: 30px;
}

#accordion .panel-title a.collapsed:before{
    content: "\f107";
}

#accordion .panel-body{
    font-size: 14px;
    color: #555;
    line-height: 25px;
    position: relative;
    border: none;
}


.gTable td, .gTable th {
  font-size: .9rem;
}

@media (max-width: 1200px) {
  .gTable td, .gTable th {
    font-size: calc(.5rem + 1vw);
  }
}
/* CSS Document */



</style>
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }
  

  $asignacion = round($opInspectionReport["quantity"]/($opInspectionReportDetail["KG_APROX_PESADO"]/$opInspectionReportDetail["VEH_PESADO"]));
  $html='<div class="row">
            <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-12 mb-5 title">Consulta de Inspecci&oacute;n</div>  
            <div class="row col-12 mb-2">
                <div class="col-lg-3 col-12 p-0">
                  <div class="col-12 p-0"><b>Muelle:</b> </div>
                  <div class="col-12 p-0">'.$opInspectionReport["port_pier"].'</div>
                </div>  
                <div class="col-lg-4 col-12 p-0">
                 <div class="col-12 p-0"><b>Buque: </b></div>
                 <div class="col-12 p-0">'.$opInspectionReport["vessel"].'</div>  
                </div> 
                <div class="col-lg-3 col-12 p-0">
                 <div class="col-12 p-0"><b>Cantidad:</b></div>
                 <div class="col-12 p-0">'.TfWidget::qty($opInspectionReport["quantity"]).' Kgs</div>  
                </div> 
                <div class="col-lg-2 col-12 p-0">
                  <div class="col-12 p-0"><b>BL: </b></div> 
                  <div class="col-12 p-0">'.$opInspectionReport["BL"].'</div> 
                </div> 
            </div>
            <div class="row col-12 mb-2">
                <div class="col-lg-6 col-12 p-0">
                  <div class="col-12 p-0"><b>Producto 1: </b> </div>
                  <div class="col-12 p-0">'.$opInspectionReport["product_1"].'</div>
                </div>  
                <div class="col-lg-6 col-12 p-0">
                  <div class="col-12 p-0"><b>Producto 2: </b> </div>
                  <div class="col-12 p-0">'.$opInspectionReport["product_2"].'</div> 
                </div> 
            </div>  



<div class="col-12">
                    <div class="panel-group mt-5" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a id="gg-a-1" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        Resumen de Descarga
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" data-parent="#accordion" style="">
                                <div class="panel-body">
                                   <table class="table gTable">
  <thead>
    <tr>
      <th class="text-right" scope="col"></th>
      <th class="text-right" scope="col">Cant. Veh.</th>
      <th class="text-right" scope="col">Peso Kgs.</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Asignacion</th>
      <td class="text-right">'.TfWidget::qty($asignacion).'</td>
      <td class="text-right">'.TfWidget::qty($opInspectionReport["quantity"]).' </td>
    </tr>
    <tr>
      <th scope="row">Peso Romana</th>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["VEH_PESADO"]).'</td>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["KG_APROX_PESADO"]).'</td>
    </tr>
    <tr>
      <th scope="row">Veh. Cargados Por Pesar</th>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["CARGADO_POR_PESAR"]).'</td>
      <td class="text-right"></td>
    </tr>
    <tr>
      <th scope="row">Veh. A Costado de Buque</th>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["A_COSTADO_BUQUE"]).'</td>
      <td class="text-right"></td>
    </tr>
    <tr>
      <th scope="row">Remanente Asignacion</th>
      <td class="text-right">'.TfWidget::qty($asignacion-$opInspectionReportDetail["VEH_PESADO"]).'</td>
      <td class="text-right">'.TfWidget::qty($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"]).'</td>
    </tr>
  </tbody>
</table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a id="gg-a-2" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" >
                                        Vehiculos Cargados
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" data-parent="#accordion" style="height: 0px;">
                                <div class="panel-body">
                                 <table class="table gTable">
  <thead>
    <tr>
      <th scope="col">Destino</th>
      <th class="text-right" scope="col">Cant. Veh.</th>
    </tr>
  </thead>
  <tbody>';

$total1=0;  
foreach ($cargado as $t){
$total1+=$t["qty"];
$tfData["op_destination_id"] = $t["id"];
$html.='<tr>
      <th scope="row"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="DVC" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.$t["destination"].'</a></th>
      <td class="text-right">'.TfWidget::qty($t["qty"]).'</td>
    </tr>';
}


$html.='<tfoot>
    <tr>
      <th scope="col">Total General</th>
      <th class="text-right" scope="col">'.$total1.'</th>
    </tr>
  </tfoot>
  </tbody>
</table>


</div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a id="gg-a-3" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" >
                                        Vehiculos Pesados
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" data-parent="#accordion" style="height: 0px;">
                                <div class="panel-body">
                                    <div class="panel-body">
                                 <table class="table gTable">
  <thead>
    <tr>
      <th scope="col">Destino</th>
      <th class="text-right" scope="col">Cant. Veh.</th>
      <th class="text-right" scope="col">Peso Neto kg.</th>
    </tr>
  </thead>
  <tbody>';
  $total2=0;$total3=0;$tfData=array();
foreach ($despachado as $t){
  $total2+=$t["qty"];
  $total3+=$t["kg"];
  $tfData["op_destination_id"] = $t["id"];
$html.='<tr>
      <th scope="row"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="DVP" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.$t["destination"].'</a></th>
      <td class="text-right">'.TfWidget::qty($t["qty"]).'</td>
      <td class="text-right">'.TfWidget::qty($t["kg"]).'</td>
    </tr>';
}


$html.='<tfoot>
    <tr>
      <th scope="col">Total General</th>
      <th class="text-right" scope="col">'.$total2.'</th>
      <th class="text-right" scope="col">'.TfWidget::qty($total3).'</th>
    </tr>
  </tfoot>
  </tbody>
</table>

                                </div>
                            </div>
                        </div>
                    </div>';

                    if ($opInspectionReport["completion_date"]==''){



                    $html.='

                     <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading4">
                                <h4 class="panel-title">
                                    <a id="gg-a-4" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapseOne" >
                                        ROB
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading4" aria-expanded="false" data-parent="#accordion" style="">
                                <div class="panel-body">
                                   <table class="table gTable">
  <tbody>
    <tr>
      <th scope="row">Vehiculos Pendiente por Pesar</th>
      <td class="text-right">'.($total1-$total2).' </td>
    </tr>
    <tr>
      <th scope="row">ROB B/L  '.$opInspectionReport["BL"].'</th>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]).' Kgs</td>
    </tr>
    <tr>
  </tbody>
</table>
                                </div>
                            </div>
                        </div>';

              }



               $html.=' 



                        
                </div>



            </div>';


         
  echo $html;   
?>


<script type="text/javascript">

  function scrollFocus(){
    var section= sessionStorage.getItem('section');
    if (section != null) {
          $('#'+section).click();
          $('html, body').stop().animate({scrollTop: ($('#'+section).offset().top)}, 1000, 'easeInOutExpo');
    } 
  } 
    
  $('.gg-accordion').on('click', function () {
    if ($(this).hasClass( "collapsed" )){
      sessionStorage.setItem('section', $(this).attr('id'));
      console.log($(this).attr('id')+" "+sessionStorage.getItem('section'));
    }
  });

  $(document).ready(function(){
         scrollFocus();
    });
  

</script>