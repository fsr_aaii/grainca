<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }
  
  $kgAprox = $opInspectionReportDetail["KG_APROX_PESADO"]?:"0";
  $vehPesado = $opInspectionReportDetail["VEH_PESADO"]?:"0";
  

  if ($kgAprox=="0" OR $vehPesado=="0" ){
    $asignacion = "0";
  }else{
    $asignacion = round($opInspectionReportDetail["qty"]/($kgAprox/$vehPesado));
  }

  $html='<div class="row">
            <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-12 mb-5 title">Consulta de Inspecci&oacute;n</div>  
            <div class="row col-12 mb-2">
                <div class="col-lg-4 col-12 p-0">
                 <div class="col-12 p-0"><b>Buque: </b></div>
                 <div class="col-12 p-0">'.$opInspectionReport["vessel"].'</div>  
                </div> 
                <div class="col-lg-4 col-12 p-0">
                  <div class="col-12 p-0"><b>Producto: </b> </div>
                  <div class="col-12 p-0">'.$opInspectionReport["product_1"].'</div>
                </div> 
                <div class="col-lg-1 col-12 p-0">
                  <div class="col-12 p-0"><b>BL: </b></div> 
                  <div class="col-12 p-0">'.$opInspectionReport["BL"].'</div> 
                </div> 
                <div class="col-lg-3 col-12 p-0">
                  <div class="col-12 p-0"><b>Muelle:</b> </div>
                  <div class="col-12 p-0">'.$opInspectionReport["port_pier"].'</div>
                </div>  
                
                
                
            </div>
            <div class="row col-12 mb-2">
              <div class="col-lg-4 col-12 p-0">
                 <div class="col-12 p-0"><b>Buque (Kgs):</b></div>
                 <div class="col-12 p-0">'.TfWidget::qty($opInspectionReport["vessel_rob"]).' Kgs</div>  
                </div> 
               <div class="col-lg-4 col-12 p-0">
                 <div class="col-12 p-0"><b>Producto (Kgs):</b></div>
                 <div class="col-12 p-0">'.TfWidget::qty($opInspectionReport["product_1_rob"]).' Kgs</div>  
                </div> 
                <div class="col-lg-4 col-12 p-0">
                 <div class="col-12 p-0"><b>Parcela (Kgs):</b></div>
                 <div class="col-12 p-0">'.TfWidget::qty($opInspectionReport["quantity"]).' Kgs</div>  
                </div>  ';
        if ($opInspectionReport["completion_date"]!=''){
          $html.='  <div class="col-lg-3 col-6 p-0">';
           if ($opInspectionReportDetail["remanente_kgs"]>=0){
             $html.='    <div class="col-12 p-0"><b>Faltantes: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]).' Kgs</div>  
                </div>  ';  
           } else{
             $html.='    <div class="col-12 p-0"><b>Excedentes: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]*(-1)).' Kgs</div>  
                </div>  ';
           }    
        }else{
          $html.='  <div class="col-lg-3 col-6 p-0">';
           if ($opInspectionReportDetail["remanente_kgs"]>=0){
             $html.='    <div class="col-12 p-0"><b>Remanente: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]).' Kgs</div>  
                </div>  ';  
           } else{
             $html.='    <div class="col-12 p-0"><b>Excedentes: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]*(-1)).' Kgs</div>  
                </div>  ';
           }    
        }         
        $html.=' </div>      



<div class="col-12">
                    <div class="panel-group mt-5" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a id="gg-a-1" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        Resumen de Descarga
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" data-parent="#accordion" style="">
                                <div class="panel-body">
                                   <table class="table gTable">
  <thead>
    <tr>
      <th class="text-right" scope="col"></th>
      <th class="text-right" scope="col">Cant. Veh.</th>
      <th class="text-right" scope="col">Peso Kgs.</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Asignacion</th>
      <td class="text-right">'.TfWidget::qty($asignacion).'</td>
      <td class="text-right">'.TfWidget::qty($opInspectionReport["quantity"]).' </td>
    </tr>
    <tr>
      <th scope="row">Peso Romana</th>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["VEH_PESADO"]?:"0").'</td>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["KG_APROX_PESADO"]?:"0").'</td>
    </tr>
    <tr>
      <th scope="row">Veh. Cargados Por Pesar</th>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["CARGADO_POR_PESAR"]?:"0").'</td>
      <td class="text-right"></td>
    </tr>
    <tr>
      <th scope="row">Veh. A Costado de Buque</th>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["A_COSTADO_BUQUE"]?:"0").'</td>
      <td class="text-right"></td>
    </tr>
    <tr>';
        if ($opInspectionReport["completion_date"]!=''){
          if ($opInspectionReportDetail["remanente_kgs"]>=0){
             $html.='    <th scope="row">Faltantes: </b> </div>
                  <td class="text-right">'.TfWidget::qty($asignacion-$opInspectionReportDetail["VEH_PESADO"]?:"0").'</td>
      <td class="text-right">'.TfWidget::qty($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"]?:"0").'</td>';  
           } else{
             $html.='    <th scope="row">Excedentes: </b> </div>
                  <td class="text-right">'.TfWidget::qty($asignacion-$opInspectionReportDetail["VEH_PESADO"]?:"0").'</td>
      <td class="text-right">'.TfWidget::qty(($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"])*(-1)?:"0").'</td>';
           }    
        }else{
          if ($opInspectionReportDetail["remanente_kgs"]>=0){
             $html.='<th scope="row">Remanente: </b> </div>
                  <<td class="text-right">'.TfWidget::qty($asignacion-$opInspectionReportDetail["VEH_PESADO"]?:"0").'</td>
      <td class="text-right">'.TfWidget::qty($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"]?:"0").'</td>';  
           } else{
             $html.='<th scope="row">Excedentes: </b> </div>
                  <td class="text-right">'.TfWidget::qty($asignacion-$opInspectionReportDetail["VEH_PESADO"]?:"0").'</td>
      <td class="text-right">'.TfWidget::qty(($opInspectionReport["quantity"]-$opInspectionReportDetail["KG_APROX_PESADO"])*(-1)?:"0").'</td>';
           }    
        }         
        $html.=' 
  </tbody>
</table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a id="gg-a-2" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" >
                                        Vehiculos Cargados
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" data-parent="#accordion" style="height: 0px;">
                                <div class="panel-body">
                                 <table class="table gTable">
  <thead>
    <tr>
      <th scope="col">Destino</th>
      <th class="text-right" scope="col">Cant. Veh.</th>
      <th class="text-right" scope="col">Peso kg.</th>
    </tr>
  </thead>
  <tbody>';

$total1=0;$total6=0;
foreach ($cargado as $t){
$total1+=$t["qty"];
$total6+=$t["kg"];
$tfData["op_destination_id"] = $t["id"];
$html.='<tr>
      <th scope="row"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="DVC" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.$t["destination"].'</a></th>
      <td class="text-right">'.TfWidget::qty($t["qty"]).'</td>
      <td class="text-right">'.TfWidget::qty($t["kg"]).'</td>
    </tr>';
}


$html.='<tfoot>
    <tr>
      <th scope="col">Total General</th>
      <th class="text-right" scope="col">'.$total1.'</th>
      <th class="text-right" scope="col">'.TfWidget::qty($total6).'</th>
    </tr>
  </tfoot>
  </tbody>
</table>


</div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a id="gg-a-3" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" >
                                        Guias Emitidas
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" data-parent="#accordion" style="height: 0px;">
                                <div class="panel-body">
                                    <div class="panel-body">
                                 <table class="table gTable">
  <thead>
    <tr>
      <th scope="col">Destino</th>
      <th class="text-right" scope="col">Guias</th>
      <th class="text-right" scope="col">Por Despachar</th>
    </tr>
  </thead>
  <tbody>';
  $total2=0;$tfData=array();
foreach ($despachado as $t){
  $total2+=$t["qty"];
  $total4+=$t["guide_qty"];
  
  $diff = $t["guide_qty"]-$t["qty"];
  if ($diff!="0"){
          $class='text-danger';
        }else{
          $class='text-primary';
        }

  $total5+=$diff;      
  $tfData["op_destination_id"] = $t["id"];
  $tfData["op_inspection_report_id"] = $tfRequest->op_inspection_report_id;
  $html.='<tr>
      <th scope="row"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="DVP" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.$t["destination"].'</a></th>
      <td class="text-right">'.TfWidget::qty($t["guide_qty"]).'</td>';
  if ($diff>0){
    $html.='<td class="text-right"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="GPDD" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.TfWidget::qty($diff).'</a></td>';
  }else{
     $html.='<td class="text-right">'.TfWidget::qty($diff).'</td>';
  }
  $html.='</tr>';
}


$html.='<tfoot>
    <tr>
      <th scope="col">Total General</th>
      <th class="text-right" scope="col">'.$total4.'</th>';
  if ($total5>0){
    $html.='<th class="text-right" scope="col"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="GPDT2" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.TfWidget::qty($total5).'</a></th>';
  }else{
     $html.='<th class="text-right">'.TfWidget::qty($total5).'</th>';
  }
  $html.='</tr>
  </tfoot>
  </tbody>
</table>

                                </div>
                            </div>
                        </div>
                    </div>';
        if ($opInspectionReport["completion_date"]==''){
          
             $html.=' <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading4">
                                <h4 class="panel-title">
                                    <a id="gg-a-4" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapseOne" >
                                        ROB
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading4" aria-expanded="false" data-parent="#accordion" style="">
                                <div class="panel-body">
                                   <table class="table gTable">
  <tbody>
    <tr>
      <th scope="row">Vehiculos Pendiente por Pesar</th>
      <td class="text-right">'.($total1-$total2).' </td>
    </tr>
    <tr>
      <th scope="row">ROB B/L '.$opInspectionReport["BL"].'</th>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]).' Kgs</td>
    </tr><tr>
      <th scope="row">ROB '.$opInspectionReport["product_1"].'</th>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["ROB_PRODUCTO"]).' Kgs</td>
    </tr>
    <tr>
      <th scope="row">ROB DEL BUQUE</th>
      <td class="text-right">'.TfWidget::qty($opInspectionReportDetail["ROB_BUQUE"]).' Kgs</td>
    </tr>*
    </tbody>
</table>
<span class="created_by pl-2">&Uacute;ltima de actualizaci&oacute;n Parcial '.TfWidget::ymdH2dmyH($opInspectionReportDetail["created_date"]).'<br></span>     
<span class="created_by pl-2">&Uacute;ltima de actualizaci&oacute;n Total '.$opInspectionReportDetail["rob_date"].'</span>                            </div>
                            </div>
                        </div>';
        }      



       $html.=' <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingB">
                      <h4 class="panel-title">
                        <a id="gg-a-B" class="collapsed gg-accordion" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseB" aria-expanded="false" aria-controls="collapseB">Transportes (Por Cargar)</a>
                      </h4>
                    </div>
                    <div id="collapseB" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingB" aria-expanded="false" data-parent="#accordion" style="">
                      <div class="panel-body">
                      <table class="table gTable">
                                    <thead>
                                      <tr>
                                        <th scope="col">Transporte</th>
                                        <th class="text-right" scope="col">Cantidad</th>
                                      </tr>
                                    </thead>
                                    <tbody>';
                                    $totalQ=0;$tfData=array();
                                  foreach ($opInspectionReportDetailTransport2 as $t){
                                    $totalQ+=$t["qty"];
                                    
                                    $tfData["op_inspection_report_id"] = $tfRequest->op_inspection_report_id;
                                    $tfData["op_inspection_report_detail_transport"] = $t["transport"];
                                    
                                    $html.='<tr>
                                        <th scope="row"><a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GgDashboard" data-tf-action="TPC1" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.$t["transport"].'</a></th>
                                        <td class="text-right">'.TfWidget::qty($t["qty"]).'</td> 
                                        </tr> ';
                                    
                                  }


                                  $html.='<tfoot>
                                      <tr>
                                        <th scope="col">Total General</th>
                                        <th class="text-right" scope="col">'.$totalQ.'</th>
                                        </tr>
                                    </tfoot>
                                    </tbody>
                                  </table> ';
                       /* foreach ($opInspectionReportDetailTransport2 as $t){
                          $html.='<div class="row col-12 mb-2 p-0">
                                  <div class="col-lg-3 col-6 p-0"><b>'.$t["transport"].'</b></div> 
                                  <div class="col-lg-3 col-6 p-0">'.$t["destination"].'</div> 
                                  <div class="col-lg-3 col-6 p-0">'.$t["SICA_guide"].' </div> 
                                  <div class="col-lg-3 col-6 p-0">'.$t["name"].'</div> 
                              </div>';
                       } */

                       
                     $html.='
                      </div>
                    </div>
                  </div>';   
        $html.='
                </div>



            </div>';


         
  echo $html;   
?>


<script type="text/javascript">

  function scrollFocus(){
    var section= sessionStorage.getItem('section');
    if (section != null) {
          $('#'+section).click();
          $('html, body').stop().animate({scrollTop: ($('#'+section).offset().top)}, 1000, 'easeInOutExpo');
    } 
  } 
    
  $('.gg-accordion').on('click', function () {
    if ($(this).hasClass( "collapsed" )){
      sessionStorage.setItem('section', $(this).attr('id'));
      console.log($(this).attr('id')+" "+sessionStorage.getItem('section'));
    }
  });

  $(document).ready(function(){
         scrollFocus();
    });
  

</script>