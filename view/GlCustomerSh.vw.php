<style type="text/css">
  .alert-info {
    color: #FFFF;
    background-color: #77415d;
    border: 0;
    font-size: .75rem;
}
</style>
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }


  $html = '<div class="row">
  <div class="mx-auto card col-lg-6 form-frame shadow mb-4">
    <form id="gl_customer_form" name="gl_customer_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title mb-5">Nuevo Cliente</div>
      <div class="alert alert-info col-12" role="alert">                
          Por Favor ingrese el RIF/CI del Nuevo Cliente
      </div>
      <div class="col-lg-2 container">
       <label for="gl_customer_nin_type" class="control-label">Tipo:</label>
      
         <select  id="gl_customer_nin_type" name="gl_customer_nin_type" class="gl_customer_nin_type form-control" tabindex="1">
           <option value="J">J</option>
           <option value="G">G</option>
         </select>

      <label for="gl_customer_nin_type" class="error"></label>
      </div>
      <div class="col-lg-6 container">
       <label for="gl_customer_nin" class="control-label">RIF/CI:</label>
        <input type="text" id="gl_customer_nin" name="gl_customer_nin" class="gl_customer_nin form-control"  maxlength="9" tabindex="2"/>
      <label for="gl_customer_nin" class="error"></label>
      </div>
      <div class="col-lg-4 container">
       <label for="gl_customer_nin_control_digit" class="control-label">Control:</label>
        <select  id="gl_customer_nin_control_digit" name="gl_customer_nin_control_digit" class="gl_customer_nin_control_digit form-control" tabindex="3">
           <option value=""></option>
           <option value="0">0</option>
           <option value="1">1</option>
           <option value="2">2</option>
           <option value="3">3</option>
           <option value="4">4</option>
           <option value="5">5</option>
           <option value="6">6</option>
           <option value="7">7</option>
           <option value="8">8</option>
           <option value="9">9</option>
         </select>

      <label for="gl_customer_nin_control_digit" class="error"></label>
      </div>
      <div class="col-lg-12 container text-right keypad">
        <a class="btn" role="button" data-tf-form="#gl_customer_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlCustomer" data-tf-action="AN" onclick="TfRequest.do(this,true);">Continuar</a>
      </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
  function glCustomerRules(){
  $("#gl_customer_form").validate();
  $("#gl_customer_nin_type").rules("add", {
    required:true,
    maxlength:1
  });
  $("#gl_customer_nin").rules("add", {
    required:true,
    number:true,
    maxlength:9
  });
  $("#gl_customer_nin_control_digit").rules("add", {
    number:true,
    maxlength:1,
    required: function(element){
            return $.inArray($("#gl_customer_nin_type").val(), ['J','G']) !== -1;
        }
  });
 
}


$(document).ready(function(){
  glCustomerRules();

})

</script>
