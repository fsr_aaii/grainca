<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#hr_user_homework_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrUserHomeworkCriteria" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$hrUserHomeworkCriteria->getCreatedBy()).'  el '.$hrUserHomeworkCriteria->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#hr_user_homework_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrUserHomeworkCriteria" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#hr_user_homework_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrUserHomeworkCriteria" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrUserHomeworkCriteria->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrUserHomeworkCriteria->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-10 tf-card shadow mb-4">
    <form id="hr_user_homework_criteria_form" name="hr_user_homework_criteria_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr User Homework Criteria</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_user_homework_criteria" name="is_hr_user_homework_criteria" value="'.$hrUserHomeworkCriteria->getInitialState().'">
         <input type="hidden" id="hr_user_homework_criteria_id" name="hr_user_homework_criteria_id" maxlength="22" value="'.$hrUserHomeworkCriteria->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_user_homework_criteria_id_user_homework" class="control-label">Id User Homework:</label>
        <select  id="hr_user_homework_criteria_id_user_homework" name="hr_user_homework_criteria_id_user_homework" class="hr_user_homework_criteria_id_user_homework form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrUserHomework::selectOptions($tfs),$hrUserHomeworkCriteria->getIdUserHomework()).
'      </select>
      <label for="hr_user_homework_criteria_id_user_homework" class="error">'.$hrUserHomeworkCriteria->getAttrError("id_user_homework").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_user_homework_criteria_id_evaluation_criteria" class="control-label">Id Evaluation Criteria:</label>
        <select  id="hr_user_homework_criteria_id_evaluation_criteria" name="hr_user_homework_criteria_id_evaluation_criteria" class="hr_user_homework_criteria_id_evaluation_criteria form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrEvaluationCriteria::selectOptions($tfs),$hrUserHomeworkCriteria->getIdEvaluationCriteria()).
'      </select>
      <label for="hr_user_homework_criteria_id_evaluation_criteria" class="error">'.$hrUserHomeworkCriteria->getAttrError("id_evaluation_criteria").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrUserHomeworkCriteriaRules(){
  $("#hr_user_homework_criteria_form").validate();
  $("#hr_user_homework_criteria_id_user_homework").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_user_homework_criteria_id_evaluation_criteria").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  hrUserHomeworkCriteriaRules();
  $('select').niceSelect();


})
</script>