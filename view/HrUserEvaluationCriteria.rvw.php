<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8 info-panel">
             <form id="hr_user_evaluation_form" name="hr_user_evaluation_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="row">
      <div class="col-2 col-xl-1 container title"><label id="tiempo" data-minutes="'.$minutes.'" class="position-fixed" >'.str_pad($minutes, 2, "0", STR_PAD_LEFT).':00</label></div>
      <div class="col-10 col-xl-11 container title">Learning agility evaluation</div>
      </div>
      <div class="col-12 container">
        '.$objAlerts.'
      </div>
      <div class="col-12 container">
         <input type="hidden" id="is_hr_user_evaluation" name="is_hr_user_evaluation" value="'.$hrUserEvaluation->getInitialState().'">
         <input type="hidden" id="hr_user_evaluation_id" name="hr_user_evaluation_id" class="hr_user_evaluation_id" maxlength="22" value="'.$hrUserEvaluation->getId().'">
      </div>';

      $range = array(1,2,3,4,5,6);
      $group='';
      $i=0;
      foreach ($hrUserEvaluationCriteriaList as $row){
        $i++;
       if ($group!=$row['group']){
          $html.='<div class="col-12 text-left evaluation-group">
                    <span>'.$row['group'].'</span>
                  </div>';    
          $group=$row['group'];        
        }
        $html.='<div class="row">
                  <div class="col-2 col-xl-1 text-left"></div>
                 <div class="col-10 col-xl-11 text-left evaluation-criteria">
                  <span id="hr_user_evaluation_criteria_label_'.$row['id'].'">'.$i.' '.$row['evaluation_criteria'].'</span>
                </div> 
                </div> 
                <div class="row">
                <div class="col-2 col-xl-1 text-left"></div>               
                <div id="hr_user_evaluation_criteria_box_'.$row['id'].'" name="hr_user_evaluation_criteria_box_'.$row['id'].'" class="col-10 col-xl-11 container hr_user_evaluation_criteria_box evaluation_box" data-radio-id="hr_user_evaluation_criteria_'.$row['id'].'" data-label-id="hr_user_evaluation_criteria_label_'.$row['id'].'">';
        
        $option=HrEvaluationCriteriaOption::dLbyCriteria($tfs,$row['id_evaluation_criteria']);

        foreach ($option as $r) {
       
         /* $html.='<div class="col-12 container">
                   <label for="" class="form-check-label">'.$r['name'].'</label>
                   <input type="radio" class="hr_user_evaluation_criteria  option-input radio" name="hr_user_evaluation_criteria_'.$row['id'].'" value="'.$row['id'].'" data-label-id="hr_user_evaluation_criteria_label_'.$row['id'].'">
                  </div>';*/

          $html.='<div class="form-check">
  <label class="form-check-label text-left">
    <input type="radio" class="hr_user_evaluation_criteria form-check-input" name="hr_user_evaluation_criteria_'.$r['id_evaluation_criteria'].'" value="'.$r['id'].'" data-label-id="hr_user_evaluation_criteria_label_'.$r['id_evaluation_criteria'].'">'.$r['name'].'
  </label>
</div>
   ';     
          
        }  
        $html.='</div>
        </div> ';
    }
             
     $html.=' <div class="col-lg-12 container mb-5 mt-2 text-right">
                <a id="hr_user_evaluation_btn" name="hr_user_evaluation_btn" class="btn-guaramo-text" data-tf-form="#hr_user_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrUserEvaluation" data-tf-action="AF" onclick="hrUserEvaluationDo(this,true);">Finalizar</a>
              </div>
              </fieldset>
              </form>
          </div>
        </div>';
 echo $html;


?>
<script type="text/javascript">
 function hrUserEvaluationDo(element,confirm=false){
   clearInterval(intervalo);
   TfRequest.do(element,confirm);
 }

  var salida = document.getElementById("tiempo");
  
  var  minutos = salida.dataset.minutes,
    segundos = 0,
    intervalo = setInterval(function(){
        if (--segundos < 0){
            segundos = 59;
            minutos--;
        }
      
        if (!minutos && !segundos){
         clearInterval(intervalo);
         hrUserEvaluationDo(document.getElementById("hr_user_evaluation_btn"),false);
        }
            
         
  
        salida.innerHTML = minutos + ":" + (segundos < 10 ? "0" + segundos : segundos);
    }, 1000);
</script>