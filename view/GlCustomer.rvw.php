<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            
            <div class="col-6 title">Clientes</div>
             <div class="col-6 text-right action">
               <a class="btn btn-guaramo-text" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlCustomer" data-tf-action="SH" onclick="TfRequest.do(this);">
                 Nuevo</a>
             </div>


             <div class="col-12 container">
 <div class="row justify-content-end">

    <div class="col-lg-4 input-group mb-3">
  <input type="text" class="filter form-control" placeholder="Buscar..." aria-label="Username" aria-describedby="basic-addon1">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1"><i class="bx bx-search"></i></span>
  </div>
</div>
  </div>
</div>

<div class="row col-12">';
$i=0;
foreach ($glPersonRelationshipList as $row){
    $i++;
    $tfData["gl_person_id"] = $row["id"];
    $tfData["gl_juridical_person_id"] = $row["id"];
    $tfData["gl_person_relationship_id"] = $row["id_relationship"];
    
    $html.='  <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlCustomer" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);" data-string="'.$row["name"].''.$row["rif"].'">
            <div class="d-flex align-items-center bg-info p-4" >
                <div class="ml-2">
                    <div class="epic h5 text-white m-0">'.$row["rif"].'</div>
                    <div class="epic h4 text-white m-0">'.$row["name"].'</div>
                    <div class="epic h5 text-white m-0"><i class="bx bx-phone"></i> '.$row["phone_number"].'</div>
                </div>
            </div>
          </div>';  

       if ($i%3==0){
      $html.='</div>
          <div class="row col-12">';
    }    
   }
   $html.='</div>
          </div>
        </div>';
 echo $html;
?>

<script type="text/javascript">
  
  $(".filter").on("keyup", function() {
  var input = $(this).val().toUpperCase();

  $(".coaching").each(function() {
    if ($(this).data("string").toUpperCase().indexOf(input) < 0) {
      $(this).hide();
    } else {
      $(this).show();
    }
  })
});

</script>