DELIMITER ;;
CREATE OR REPLACE TRIGGER OpNotification_aui_trg BEFORE INSERT ON OpNotification FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_inspection_report";"subject";"body";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_inspection_report,'";"',NEW.subject,'";"',NEW.body,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','OpNotification',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER OpNotification_aud_trg BEFORE DELETE ON OpNotification FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_inspection_report";"subject";"body";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_inspection_report,'";"',OLD.subject,'";"',OLD.body,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','OpNotification',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER OpNotification_auu_trg BEFORE UPDATE ON OpNotification FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_inspection_report!=OLD.id_inspection_report THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_inspection_report"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_inspection_report,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_inspection_report,'"');
     SET v_separator=';';
   END IF;
   IF NEW.subject!=OLD.subject THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"subject"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.subject,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.subject,'"');
     SET v_separator=';';
   END IF;
   IF NEW.body!=OLD.body THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"body"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.body,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.body,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','OpNotification',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

