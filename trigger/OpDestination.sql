DELIMITER ;;
CREATE OR REPLACE TRIGGER OpDestination_aui_trg BEFORE INSERT ON OpDestination FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_inspection_report";"name";"qty";"BL";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_inspection_report,'";"',NEW.name,'";"',NEW.qty,'";"',NEW.BL,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','OpDestination',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER OpDestination_aud_trg BEFORE DELETE ON OpDestination FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_inspection_report";"name";"qty";"BL";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_inspection_report,'";"',OLD.name,'";"',OLD.qty,'";"',OLD.BL,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','OpDestination',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER OpDestination_auu_trg BEFORE UPDATE ON OpDestination FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_inspection_report!=OLD.id_inspection_report THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_inspection_report"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_inspection_report,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_inspection_report,'"');
     SET v_separator=';';
   END IF;
   IF NEW.name!=OLD.name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.qty!=OLD.qty THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"qty"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.qty,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.qty,'"');
     SET v_separator=';';
   END IF;
   IF NEW.BL!=OLD.BL THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"BL"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.BL,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.BL,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','OpDestination',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

