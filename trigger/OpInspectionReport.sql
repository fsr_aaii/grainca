DELIMITER ;;
CREATE OR REPLACE TRIGGER OpInspectionReport_aui_trg BEFORE INSERT ON OpInspectionReport FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_customer";"vessel";"id_port_pier";"product";"quantity";"inspection_date";"issued_date";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_customer,'";"',NEW.vessel,'";"',NEW.id_port_pier,'";"',NEW.product,'";"',NEW.quantity,'";"',NEW.inspection_date,'";"',NEW.issued_date,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','OpInspectionReport',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER OpInspectionReport_aud_trg BEFORE DELETE ON OpInspectionReport FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_customer";"vessel";"id_port_pier";"product";"quantity";"inspection_date";"issued_date";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_customer,'";"',OLD.vessel,'";"',OLD.id_port_pier,'";"',OLD.product,'";"',OLD.quantity,'";"',OLD.inspection_date,'";"',OLD.issued_date,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','OpInspectionReport',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER OpInspectionReport_auu_trg BEFORE UPDATE ON OpInspectionReport FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_customer!=OLD.id_customer THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_customer"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_customer,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_customer,'"');
     SET v_separator=';';
   END IF;
   IF NEW.vessel!=OLD.vessel THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"vessel"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.vessel,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.vessel,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_port_pier!=OLD.id_port_pier THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_port_pier"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_port_pier,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_port_pier,'"');
     SET v_separator=';';
   END IF;
   IF NEW.product!=OLD.product THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"product"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.product,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.product,'"');
     SET v_separator=';';
   END IF;
   IF NEW.quantity!=OLD.quantity THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"quantity"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.quantity,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.quantity,'"');
     SET v_separator=';';
   END IF;
   IF NEW.inspection_date!=OLD.inspection_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"inspection_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.inspection_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.inspection_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.issued_date!=OLD.issued_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"issued_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.issued_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.issued_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','OpInspectionReport',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

