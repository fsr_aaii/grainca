DELIMITER ;;
CREATE OR REPLACE TRIGGER GlJuridicalPerson_aui_trg BEFORE INSERT ON GlJuridicalPerson FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"nin_type";"nin";"nin_control_digit";"legal_name";"trade_name";"legal_address";"phone_number";"active";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.nin_type,'";"',NEW.nin,'";"',NEW.nin_control_digit,'";"',NEW.legal_name,'";"',NEW.trade_name,'";"',NEW.legal_address,'";"',NEW.phone_number,'";"',NEW.active,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','GlJuridicalPerson',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER GlJuridicalPerson_aud_trg BEFORE DELETE ON GlJuridicalPerson FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"nin_type";"nin";"nin_control_digit";"legal_name";"trade_name";"legal_address";"phone_number";"active";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.nin_type,'";"',OLD.nin,'";"',OLD.nin_control_digit,'";"',OLD.legal_name,'";"',OLD.trade_name,'";"',OLD.legal_address,'";"',OLD.phone_number,'";"',OLD.active,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','GlJuridicalPerson',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER GlJuridicalPerson_auu_trg BEFORE UPDATE ON GlJuridicalPerson FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.nin_type!=OLD.nin_type THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"nin_type"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.nin_type,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.nin_type,'"');
     SET v_separator=';';
   END IF;
   IF NEW.nin!=OLD.nin THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"nin"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.nin,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.nin,'"');
     SET v_separator=';';
   END IF;
   IF NEW.nin_control_digit!=OLD.nin_control_digit THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"nin_control_digit"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.nin_control_digit,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.nin_control_digit,'"');
     SET v_separator=';';
   END IF;
   IF NEW.legal_name!=OLD.legal_name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"legal_name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.legal_name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.legal_name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.trade_name!=OLD.trade_name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"trade_name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.trade_name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.trade_name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.legal_address!=OLD.legal_address THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"legal_address"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.legal_address,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.legal_address,'"');
     SET v_separator=';';
   END IF;
   IF NEW.phone_number!=OLD.phone_number THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"phone_number"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.phone_number,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.phone_number,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','GlJuridicalPerson',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

