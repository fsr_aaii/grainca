class TfCryptography {

    
	randomPublicKey(length) {
       var result           = '';
	   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	   var charactersLength = characters.length;
	   for ( var i = 0; i < length; i++ ) {
	      result += characters.charAt(Math.floor(Math.random() * charactersLength));
	   }
	   return result;
    }

    encryptJSON(jsonObj,randomPublicKey){
   
		 let encryption = new Encryption();
         return encryption.encrypt(JSON.stringify(jsonObj), randomPublicKey);        
          
    }

    encryptRSA(jsonObj){
        let pk="-----BEGIN PUBLIC KEY-----\
                MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAh0dbZp/ZvqD1W+A3VOiT\
                56P3OrcWoyRkFLC+/l2CU6bBKcZusMemFRDlizWYX/8N0QDa69ehqHnhwmd+97TB\
                pyWBEKIVL3Jwl5F0I85ya5c6UwIUBw5Qg1PvF29lqoZmDkY7itZe/2ifqyu7CjOt\
                +p7yOpUFv80Sc5w1iKHtK8kX6ytalD/DUaFo+8uiIW03k/RuVQ7MMW6r0L5ImtFj\
                nThCXUSXxZ8tahVPpYzk2STjYXmpOLYRD45xCXRzqpAqfgBRi1swps7DooQ/0DWR\
                bQRbedJI1VcT4me8x+F+qupXBSJy6xe2wgOYkk7BhACtVhT20sC86dNuPZDwJCbA\
                4YyuLlSOMUWm5S6d1WEhny/qqSwOf06mDAJfnvVxjFYQUQ+HITND0rcC/TPVPKxm\
                34dd6W+x7f+ll9rQxqDU1IlxaALNVHfmuut5CvAOQwus9Vg+7pc12Z36E9YTrmYb\
                4608HVQrwjQJfTONY9RG6Glkl53uPgbNhKF8LL5ZUHlz+A8vCDppYKtA0iqOYSE2\
                faWLrWYPaSa559xwWmgVThi3DCkzPM7hv4DzH0RtFeCp0NBvx7kQy0mUGqRULIiY\
                6Ac4SLXqvJLrFz5IMBCKY6OXNWdyYl8y+j6uNHox8HispHWijAH1/DomLwj6rwyi\
                5gVxTFx6nR/uRmzsRmnN2ssCAwEAAQ==\
                -----END PUBLIC KEY-----";      
        var encrypt = new JSEncrypt();
        encrypt.setPublicKey(pk);         
        
        
        return encrypt.encrypt(JSON.stringify(jsonObj));
   
    }
    
    decryptResponse(response,randomPublicKey){
      let encryption = new Encryption();
      return encryption.decrypt(response, randomPublicKey);
    } 
    

   
}