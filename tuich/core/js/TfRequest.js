class TfRequest{
	
	static login(jsonObject){
		let str = window.location.href;
        let root = str.replace("#", "");

        if (root.substr(root.length - 1)=="/"){
          root+="ve";
        }else{
          root+="/ve";
        }
        console.log(root);
		let JDATA = new Object();
		let cryptography  = new TfCryptography();
		sessionStorage.setItem('RANDOM_PUBLIC_KEY', cryptography.randomPublicKey(8));
        sessionStorage.setItem('TF_TRAIL_ID', cryptography.randomPublicKey(8));
        
        jsonObject.tfTrailId = sessionStorage.getItem('TF_TRAIL_ID');
        jsonObject.tfRandomPublicKey = sessionStorage.getItem('RANDOM_PUBLIC_KEY');

		let auth = 'N';
          $.ajax({
		    data: {"request-data": cryptography.encryptRSA(jsonObject)},
		    url: root+"/login",    
		    type: 'POST',
		    async : false,
			 success:function(response){
			    try {
			        JDATA=JSON.parse(response);  
                    
                    if (JDATA.status==200){ 
                      sessionStorage.setItem('TF_SESSION_TOKEN', JDATA.tfSessionToken.value);
					  sessionStorage.setItem('TF_APP_ROOT',root);	
					  sessionStorage.setItem('TF_URL_TOKEN',root+"/"+sessionStorage.getItem('TF_SESSION_TOKEN'));	
					  sessionStorage.setItem('TF_URL_ROOT', root+"/"+sessionStorage.getItem('TF_SESSION_TOKEN')+"/"+sessionStorage.getItem('TF_TRAIL_ID'));
                      auth = 'Y';
                    }else{
                      auth = 'N'; 	
                    } 
                               
			    } catch (e) {
			    	console.log(response);
			    	console.log('Exception '+e);
			    	auth = 'N'; 
			    }     
		                
			},
			error: function(response) {	
				console.log(response);  
				auth = 'N'; 
		    }
	    });
       return auth;
	}


	static reset(jsonObject){
		let str = window.location.href;
        let root = str.substr(0, str.indexOf("/forgot"));

        if (root.substr(root.length - 1)=="/"){
          root+="ve";
        }else{
          root+="/ve";
        }
		
		let JDATA = new Object();
		let cryptography  = new TfCryptography();
		sessionStorage.setItem('RANDOM_PUBLIC_KEY', cryptography.randomPublicKey(8));
        sessionStorage.setItem('TF_TRAIL_ID', cryptography.randomPublicKey(8));
        
        jsonObject.tfTrailId = sessionStorage.getItem('TF_TRAIL_ID');
        jsonObject.tfRandomPublicKey = sessionStorage.getItem('RANDOM_PUBLIC_KEY');

		let auth = 'N';
          $.ajax({
		    data: {"request-data": cryptography.encryptRSA(jsonObject)},
		    url: root+'/reset',    
		    type: 'POST',
		    async : false,
			 success:function(response){
			    try {
			        JDATA=JSON.parse(response);  
                    
                    if (JDATA.status==200){ 
                      sessionStorage.setItem('TF_SESSION_TOKEN', JDATA.tfSessionToken.value);
					  sessionStorage.setItem('TF_APP_ROOT',root);	
					  sessionStorage.setItem('TF_URL_TOKEN',root+"/"+sessionStorage.getItem('TF_SESSION_TOKEN'));	
					  sessionStorage.setItem('TF_URL_ROOT', root+"/"+sessionStorage.getItem('TF_SESSION_TOKEN')+"/"+sessionStorage.getItem('TF_TRAIL_ID'));
                      auth = 'Y';
                    }else{
                      auth = 'N'; 	
                    } 
                               
			    } catch (e) {
			    	console.log('Exception '+e);
			    	auth = 'N'; 
			    }     
		                
			},
			error: function(response) {	
				console.log(response);  
				auth = 'N'; 
		    }
	    });
       return auth;
	}

	static admin(jsonObject){
		let str = window.location.href;
        let root = str.replace("admin", "");

        if (root.substr(root.length - 1)=="/"){
          root+="ve";
        }else{
          root+="/ve";
        }
        console.log(root);
		let JDATA = new Object();
		let cryptography  = new TfCryptography();
		sessionStorage.setItem('RANDOM_PUBLIC_KEY', cryptography.randomPublicKey(8));
        sessionStorage.setItem('TF_TRAIL_ID', cryptography.randomPublicKey(8));
        
        jsonObject.tfTrailId = sessionStorage.getItem('TF_TRAIL_ID');
        jsonObject.tfRandomPublicKey = sessionStorage.getItem('RANDOM_PUBLIC_KEY');

		let auth = 'N';
          $.ajax({
		    data: {"request-data": cryptography.encryptRSA(jsonObject)},
		    url: root+"/login",    
		    type: 'POST',
		    async : false,
			 success:function(response){
			    try {
			        JDATA=JSON.parse(response);  
                    
                    if (JDATA.status==200){ 
                      sessionStorage.setItem('TF_SESSION_TOKEN', JDATA.tfSessionToken.value);
					  sessionStorage.setItem('TF_APP_ROOT',root);	
					  sessionStorage.setItem('TF_URL_TOKEN',root+"/"+sessionStorage.getItem('TF_SESSION_TOKEN'));	
					  sessionStorage.setItem('TF_URL_ROOT', root+"/"+sessionStorage.getItem('TF_SESSION_TOKEN')+"/"+sessionStorage.getItem('TF_TRAIL_ID'));
                      auth = 'Y';
                    }else{
                      auth = 'N'; 	
                    } 
                               
			    } catch (e) {
			    	console.log('Exception '+e);
			    	auth = 'N'; 
			    }     
		                
			},
			error: function(response) {	
				console.log(response);  
				auth = 'N'; 
		    }
	    });
       return auth;
	}

	static register(jsonObject){
		let str = window.location.href;
        let root = str.replace("register", "");

        if (root.substr(root.length - 1)=="/"){
          root+="ve";
        }else{
          root+="/ve";
        }
        console.log(root);
		let JDATA = new Object();
		let cryptography  = new TfCryptography();
		sessionStorage.setItem('RANDOM_PUBLIC_KEY', cryptography.randomPublicKey(8));
        sessionStorage.setItem('TF_TRAIL_ID', cryptography.randomPublicKey(8));
        
        jsonObject.tfTrailId = sessionStorage.getItem('TF_TRAIL_ID');
        jsonObject.tfRandomPublicKey = sessionStorage.getItem('RANDOM_PUBLIC_KEY');

		let status = 'N';
          $.ajax({
		    data: {"request-data": cryptography.encryptRSA(jsonObject)},
		    url: root+"/register",    
		    type: 'POST',
		    async : false,
			 success:function(response){
			    try {
			        JDATA=JSON.parse(response);  
                    console.log(JDATA.status);
                    if (JDATA.status==200){ 

                      if (JDATA.tfStatus.value=="Ok"){
                        sessionStorage.setItem('TF_SESSION_TOKEN', JDATA.tfSessionToken.value);
					    sessionStorage.setItem('TF_APP_ROOT',root);	
					    sessionStorage.setItem('TF_URL_TOKEN',root+"/"+sessionStorage.getItem('TF_SESSION_TOKEN'));	
					    sessionStorage.setItem('TF_URL_ROOT', root+"/"+sessionStorage.getItem('TF_SESSION_TOKEN')+"/"+sessionStorage.getItem('TF_TRAIL_ID'));
                      
                        status = 'Y';
                      }else if(JDATA.tfStatus.value=="Duplicate"){
                        status = 'D';
                      }else{
                        status = 'F';
                      }                      
                    }else{
                      status = 'N'; 	
                    } 
                               
			    } catch (e) {
			    	console.log('Exception '+e);
			    	status = 'N'; 
			    }     
		                
			},
			error: function(response) {	
				console.log(response);  
				status = 'N'; 
		    }
	    });
       return status;
	}
  
    static nav(){
    	let jsonObject = new Object();
		  let JDATA = new Object();
      let cryptography  = new TfCryptography();
        
      let rpk = sessionStorage.getItem('RANDOM_PUBLIC_KEY');

      if(rpk !== null && rpk !== '') {
        
        let photo = sessionStorage.getItem('TF_PHOTO_LOAD');

        if(photo !== null && photo !== '') {
        	console.log("photo");
          $('#main-menu-ul').html(sessionStorage.getItem('TF_MENU_LOAD'));
			    $('#nav-user-photo').attr("src",sessionStorage.getItem('TF_PHOTO_LOAD'));	
        }else{
        	console.log("no-photo");
          jsonObject.randomPublicKey = rpk;
		      jsonObject.tfTrailId = sessionStorage.getItem('TF_TRAIL_ID');            

	        $.ajax({
			      data: {"request-data": cryptography.encryptRSA(jsonObject)},
			      url: sessionStorage.getItem('TF_URL_ROOT')+"/nav",    
			      type: 'POST',
			      async : false,
				  success:function(response){
				    let JDATA=JSON.parse(response);
				    //$('#main-menu-ul').html(cryptography.decryptResponse(JDATA.tfMenu.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY')));
				    //$('#nav-user-photo').attr("src",cryptography.decryptResponse(JDATA.tfUserPhoto.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY')));	
				    $('#main-menu-ul').html(JDATA.tfMenu.value);
				    $('#nav-user-photo').attr("src",JDATA.tfUserPhoto.value);	
				    sessionStorage.setItem('TF_MENU_LOAD',JDATA.tfMenu.value);
				    sessionStorage.setItem('TF_PHOTO_LOAD',JDATA.tfUserPhoto.value);
					  },
					  error: function(response) {	
						  JDATA=JSON.parse(response.responseText);  
				    }});
        }	
        
        
      }else{
        //alert("NO MENU");
      }   

	    return JDATA;
	}	

	static default(){
       
        let str = window.location.href;
        let step = str.replace(sessionStorage.getItem('TF_URL_ROOT'), "");
		    step = step.replace("/", "");
		    $.ajax({
		    data: {"request-data": ""},
		    url: sessionStorage.getItem('TF_URL_ROOT')+"/default/"+step,   
		    type: 'POST',
		    async : false,
			success:function(response){	
				try{
				   let JDATA=JSON.parse(response);
			     let cryptography  = new TfCryptography();
	         if (JDATA.tfContent.encrypt=='true'){
	            $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY')));
					        $("#loading").hide();
	                $("#screen").show();
	                window.scrollTo(0,0);
				    }else{
				        $('#tf-container').html(JDATA.tfContent.value);
					        $("#loading").hide();
	                $("#screen").show();
	                window.scrollTo(0,0);					      
				    }

				    sessionStorage.setItem("TF_PREVIOUS_STEP",sessionStorage.getItem("TF_CURRENT_STEP"));
            sessionStorage.setItem("TF_CURRENT_STEP",step);
         } catch (e) {
			    	 $("#loading").hide();
             $("#screen").show();
             $('#tf-container').html(e.message);
			    	console.log(response);  
			    } 
			},
			error: function(response) {
				$("#loading").hide();
        $("#screen").show();
        $('#tf-container').html("<p>Error Inesperado (DEFAULT)</p>");
				console.log(response);  
		    }
	     });
    }

    static redirect(tfTaskId,tfController,tfAction,tfData){
    	 $.ajax({
		    data: {"request-data": tfData},
		    url: sessionStorage.getItem('TF_URL_ROOT')+"/redirect/"+tfTaskId+"/"+tfController+"/"+tfAction,    
		    type: 'POST',
		    async : false,
			success:function(response){	
			   $("#loading").show();
               $("#screen").hide(); 
			   try{   
			     let JDATA=JSON.parse(response);
		         let cryptography  = new TfCryptography();
                 if (JDATA.tfContent.encrypt=='true'){
                      $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY')));
				    }else{
				      $('#tf-container').html(JDATA.tfContent.value);	
				    }
                 window.scrollTo(0,0);
          }catch (e) {
			      $("#loading").hide();
            $("#screen").show();
			      $('#tf-container').html(e.message);
			      console.log(response);
			    }   
			}
	     });
    }
    

    static previous(step){

    	 $("#loading").show();
       $("#screen").hide();
    	 $.ajax({
		    data: {"request-data": ""},
		    url: sessionStorage.getItem('TF_URL_ROOT')+"/previous/"+step,    
		    type: 'POST',
		    async : true,
			success:function(response){	
			   //console.log(response);	
			   let JDATA=JSON.parse(response);
		     let cryptography  = new TfCryptography();

		     if (JDATA.tfContent.encrypt=='true'){
            $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY'))).promise().done(function(){
				        $("#loading").hide();
                $("#screen").show();
                window.scrollTo(0,0);
				    });
			    }else{
			        $('#tf-container').html(JDATA.tfContent.value).promise().done(function(){
				        $("#loading").hide();
                $("#screen").show();
                window.scrollTo(0,0);
				    });	
			    }
            sessionStorage.setItem("TF_PREVIOUS_STEP",sessionStorage.getItem("TF_CURRENT_STEP"));
            sessionStorage.setItem("TF_CURRENT_STEP",step);
            
			}
	  });

  } 
	static menu(element){
		$("#loading").show();
        $("#screen").hide(); 
		let root = sessionStorage.getItem('TF_URL_ROOT');
		$('#tf-container').addClass("spinner");
		$('.menu-link').removeClass("active");  
	    $('#'+element.id).addClass("active"); 
	    sessionStorage.setItem('profile', 'null');
	    sessionStorage.setItem('quarter', 'null');
	    sessionStorage.setItem('section', 'null');
	    window.TF_TASK_ID = element.dataset.tfTaskId;
		let url=root+"/view/"+element.dataset.tfTaskId+"/"+element.dataset.tfController+"/"+element.dataset.tfAction;
        let data;
        //let step = TfRequest.counter();
        //history.pushState({ step: step },null,root+"/"+step);

        $.ajax({
		    data: {"request-data": data},
		    url: url,    
		    type: 'POST',
		    async : false,
			success:function(response){	
				 try {
			    	$('#tf-container').removeClass("spinner");   
			        let JDATA=JSON.parse(response);
			        let cryptography  = new TfCryptography();
                    if (JDATA.tfContent.encrypt=='true'){
                      $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY')));
				    }else{
				      $('#tf-container').html(JDATA.tfContent.value);	
				    }
                    $("#loading").hide();
                    $("#screen").show();
                    window.scrollTo(0,0);

                    let step = JDATA.tfSessionPushState.value;
                    history.pushState({ step: step },null,root+"/"+step);

                    sessionStorage.setItem("TF_PREVIOUS_STEP",sessionStorage.getItem("TF_CURRENT_STEP"));
                    sessionStorage.setItem("TF_CURRENT_STEP",step);
                
			    } catch (e) {
			    	$("#loading").hide();
            $("#screen").show();
			      $('#tf-container').html(e.message);
			      console.log(response);
			    }     
			}
	    });
	} 
	
	static post(element){
		$("#loading").show();
    $("#screen").hide();
		let url=sessionStorage.getItem('TF_URL_ROOT')+"/view/"+element.dataset.tfTaskId+"/"+element.dataset.tfController+"/"+element.dataset.tfAction;
        let data = element.dataset.tfData;
        let form = element.dataset.tfForm;
        let go = true;
        let root = sessionStorage.getItem('TF_URL_ROOT');
        
        if (typeof data == 'undefined') {
          let jsonObject = new Object();
          if (typeof form != 'undefined') {
          	if ($(form).valid()){
             jsonObject = $(form).serializeJSON();   
            }else{
            	go = false;
            } 
          }
          let cryptography  = new TfCryptography();
         
	      data = cryptography.encryptJSON(jsonObject,sessionStorage.getItem('RANDOM_PUBLIC_KEY'));
        }
        
        if (go){
            $.ajax({
			    data: {"request-data": data},
			    url: url,    
			    type: 'POST',
			    async : true,
				success:function(response){	
				   try{ 
						   //tfSessionHistoryState
						    let JDATA=JSON.parse(response);

						    let cryptography  = new TfCryptography();
						    
						    if (JDATA.tfContent.encrypt=='true'){
		              $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY'))).promise().done(function(){
							        if (JDATA.tfSessionHistoryState.value > 0){
						            let step = JDATA.tfSessionPushState.value;
		                    history.pushState({ step: step },null,root+"/"+step);
		                    sessionStorage.setItem("TF_PREVIOUS_STEP",sessionStorage.getItem("TF_CURRENT_STEP"));
                        sessionStorage.setItem("TF_CURRENT_STEP",step);
						          }
		                  $("#loading").hide();
		                  $("#screen").show();
		                  window.scrollTo(0,0);
							    });
						    }else{
						        $('#tf-container').html(JDATA.tfContent.value).promise().done(function(){
							      if (JDATA.tfSessionHistoryState.value > 0){
						            let step = JDATA.tfSessionPushState.value;
		                    history.pushState({ step: step },null,root+"/"+step);
		                    sessionStorage.setItem("TF_PREVIOUS_STEP",sessionStorage.getItem("TF_CURRENT_STEP"));
                        sessionStorage.setItem("TF_CURRENT_STEP",step);
						          }
		                  $("#loading").hide();
		                  $("#screen").show();
		                  window.scrollTo(0,0);
							    });	
						    }

                    
                    
                   
                   }catch (e) {
			    	         $("#loading").hide();
                     $("#screen").show();
                     ('#tf-container').html(e.message);
			    	         console.log(response);
			       } 
				}
	        });
        }else{
        	$("#loading").hide();
            $("#screen").show();
        }
    }

   static fn(jsonObject){
	    let url=sessionStorage.getItem('TF_URL_ROOT')+"/fn/"+jsonObject.tfTaskId+"/"+jsonObject.tfController+"/"+jsonObject.tfFnName;
         
        let cryptography  = new TfCryptography();
	    let data = cryptography.encryptJSON(jsonObject.tfData,sessionStorage.getItem('RANDOM_PUBLIC_KEY'));
        var r;
        $.ajax({
		    data: {"request-data": data},
		    url: url,    
		    type: 'POST',
		    async : false,
			success:function(response){	
			   try{	
			   let JDATA=JSON.parse(response);
               r = cryptography.decryptResponse(JDATA.tfContent.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY'));   
               } catch (e) {

			    	console.log(e);
			    	console.log(sessionStorage.getItem('RANDOM_PUBLIC_KEY'));
			    	console.log(response);  
			    }     
			},
			error: function(response) {	
				$("#loading").hide();
            $("#screen").show();
			      $('#tf-container').html(e.message);
			      console.log(response); 
		    }
	    });

	    return r;
        
    }

   
    static home(){
		let step = TfRequest.counter();
        history.pushState({ step: step },null,sessionStorage.getItem('TF_URL_ROOT')+"/"+step);
        $.ajax({
		    data: {"request-data": ""},
		    url: sessionStorage.getItem('TF_URL_ROOT')+"/home",    
		    type: 'POST',
		    async : false,
			success:function(response){	
				try{
				   let JDATA=JSON.parse(response);
			       let cryptography  = new TfCryptography();
	               if (JDATA.tfContent.encrypt=='true'){
                      $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY')));
				    }else{
				      $('#tf-container').html(JDATA.tfContent.value);	
				    }
	               window.scrollTo(0,0);
               } catch (e) {

			    	console.log(e);
			    	console.log(sessionStorage.getItem('RANDOM_PUBLIC_KEY'));
			    	console.log(response);  
			    } 
			},
			error: function(response) {	
				$("#loading").hide();
            $("#screen").show();
			      $('#tf-container').html(e.message);
			      console.log(response); 
		    }
	     });
    }
    

    static refresh(){
	    let str = window.location.href; 
		let url=str.replace("#", "")+"/"+sessionStorage.getItem('TF_TRAIL_ID')+"/refresh";
        $.ajax({
		    data: {"request-data": ""},
		    url: url,    
		    type: 'POST',
		    async : false,
			success:function(response){	
			   let JDATA=JSON.parse(response);
		       let cryptography  = new TfCryptography();
               if (JDATA.tfContent.encrypt=='true'){
                      $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY')));
				    }else{
				      $('#tf-container').html(JDATA.tfContent.value);	
				    }
               window.scrollTo(0,0);
			}
	     });
    }

    

    static logout(){
        let login = sessionStorage.getItem('TF_APP_ROOT'); 
        let url=login.replace("#", "");
        url=url.replace("/ve", "");
        sessionStorage.clear();
	    window.location.href=url;
    }

    static tab(){
	   window.open(sessionStorage.getItem('TF_APP_ROOT')+'/'+sessionStorage.getItem('TF_SESSION_TOKEN'),'_blank');
    }

    
    static do(element,confirm=false){
		if (confirm){
		   jConfirm('Está seguro de que desea procesar esta información?','Continuar?',function(r){
		   	   if(r){
		   	   	
		   	   	TfRequest.post(element);
		   	   }
		   });
		}else{
          TfRequest.post(element);
		}
	}

    static api(url,jsonObject){
		let JDATA = new Object();
        let cryptography  = new TfCryptography();
         $.ajax({
		    data: {"request-data": cryptography.encryptJSON(jsonObject)},
		    url: url,    
		    type: 'POST',
		    async : false,
			 success:function(response){
			    try {
			    	 JDATA=JSON.parse(response);  
                
			        $.each(JDATA, function(i, item) {
			        	if (JDATA[i].encrypt){
	                       JDATA[i].value=cryptography.decryptResponse(JDATA[i].value,sessionStorage.getItem('RANDOM_PUBLIC_KEY'));
			        	}
	               
	                });
			    } catch (e) {
			    	console.log('Exception 69');
			    }     
		                
			},
			error: function(response) {	
				JDATA=JSON.parse(response.responseText);  
		    }
	    });

	    return JDATA;
	}
    
    
	static counter(op='INCREASE'){
	  let n = sessionStorage.getItem('TF_COUNTER');
	  if (n === null) {
		n = 1;
	  }else {
	  	if (op=='DECREASE'){
          n--;
	  	}else{
	  	  n++;	
	  	}
	  }
	 sessionStorage.setItem("TF_COUNTER", n);	 
     return n; 
	}	
	
}